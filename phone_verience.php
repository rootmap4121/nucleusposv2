<?php
include('class/auth.php');
$table="product_verience";
if(isset($_POST['create']))
{
	extract($_POST);
	$chk=$obj->exists_multiple($table,array("pid"=>$_POST['pid']));
	if($chk==0)
	{
		$obj->insert($table,array("store_id"=>$input_by,"pid"=>$pid,"quantity"=>$quantity,"date"=>date('Y-m-d'),"status"=>1));
		$obj->Success("Your Product Verience Report has been generated",$obj->filename()."?pid=".$pid."&ac=view");
	}
	else
	{
		$inid=$obj->SelectAllByVal2($table,"pid",$pid,"store_id",$input_by,"id");
		$obj->update($table,array("id"=>$inid,"pid"=>$_POST['pid'],"quantity"=>$quantity,"date"=>date('Y-m-d'),"status"=>1));
		$obj->Success("Your Product Verience Report has been Updated",$obj->filename()."?pid=".$pid."&ac=view");
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">

            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->


            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="icon-random"></i> Phone Verience Report </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>?pid=<?php echo $_GET['pid']; ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                
                                
                                
                                <!-- Content Start from here customized -->
                                
                         
                        <div class="table-overflow">
                                <table class="table table-striped" id="data-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Barcode</th>
                                            <th>Name</th>
                                            <th>Cost</th>
                                            <th>Retail</th>
                                            <th>Given Inventory</th>
                                            <!--<th>Quantity</th>-->
                                            <th>Status</th>
                                            <th>Generated</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
										if(!isset($_GET['lotno'])){
											$sql_product=$obj->SelectAllByID_Multiple("product_verience",array("store_id"=>$input_by,"status"=>2));
											$i=1;
											if(!empty($sql_product))
											foreach($sql_product as $product):
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"barcode"); ?></td>
												<td><label class="label label-success"> <?php echo $obj->SelectAllByVal("product","id",$product->pid,"name"); ?> </label></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"price_cost"); ?></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"price_retail"); ?></td>
												<td><?php 
												$sqlsalesproduct=$obj->SelectAllByID_Multiple("sales",array("pid"=>$product->pid));
												$sold=0;
												if(!empty($sqlsalesproduct))
												foreach($sqlsalesproduct as $soldproduct):
													$sold+=$soldproduct->quantity;
												endforeach;
												
												$instock=$product->quantity-$sold;
												
												if($instock<$product->quantity)
												{
													$mess="<button type='button' class='btn btn-info'>Over Inventory</button>";	
												}
												else
												{
													$mess="<button type='button' class='btn btn-warning'>Missing Inventory</button>";		
												}
												echo $product->quantity;
												?></td>
											 <!--<td><label class="label label-primary"> <?php //echo $product->quantity; ?> </label></td>-->
											 <td>
												<?php echo $mess; ?>
											 </td>
											 <td>
												<?php echo $product->date; ?>
											 </td>
											</tr>
											<?php 
											$i++;
											endforeach; 
										}else{
											$lotno=$_GET['lotno'];
											$sql_product=$obj->SelectAllByID_Multiple("product_verience",array("store_id"=>$input_by,"lotno"=>$lotno,"status"=>2));
											$i=1;
											if(!empty($sql_product))
											foreach($sql_product as $product):
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"barcode"); ?></td>
												<td><label class="label label-success"> <?php echo $obj->SelectAllByVal("product","id",$product->pid,"name"); ?> </label></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"price_cost"); ?></td>
												<td><?php echo $obj->SelectAllByVal("product","id",$product->pid,"price_retail"); ?></td>
												<td><?php 
												$sqlsalesproduct=$obj->SelectAllByID_Multiple("sales",array("pid"=>$product->pid));
												$sold=0;
												if(!empty($sqlsalesproduct))
												foreach($sqlsalesproduct as $soldproduct):
													$sold+=$soldproduct->quantity;
												endforeach;
												
												$instock=$product->quantity-$sold;
												
												if($instock<$product->quantity)
												{
													$mess="<button type='button' class='btn btn-info'>Over Inventory</button>";	
												}
												else
												{
													$mess="<button type='button' class='btn btn-warning'>Missing Inventory</button>";		
												}
												echo $product->quantity;
												?></td>
											 <!--<td><label class="label label-primary"> <?php //echo $product->quantity; ?> </label></td>-->
											 <td>
												<?php echo $mess; ?>
											 </td>
											 <td>
												<?php echo $product->date; ?>
											 </td>
											</tr>
											<?php 
											$i++;
											endforeach; 
	
										}
										?>
                                    </tbody>
                                </table>
                            </div>      
                                <!-- Content End from here customized -->
                                



                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
