<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
if(@$_GET['export']=="excel") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sql_parts_order = $report->SelectAllDate("parts_order",$from,$to,"1");
			$record = $report->SelectAllDate("parts_order",$from,$to,"2");
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sql_parts_order = $obj->SelectAll("parts_order");
			$record = $obj->totalrows("parts_order");
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sql_parts_order = $report->SelectAllDate_Store("parts_order",$from,$to,"1","input_by",$input_by);
			$record = $report->SelectAllDate_Store("parts_order",$from,$to,"2","input_by",$input_by);
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sql_parts_order = $obj->SelectAllByID("parts_order",array("input_by"=>$input_by));
			$record = $obj->exists_multiple("parts_order",array("input_by"=>$input_by));
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}

header('Content-type: application/excel');
$filename ="Parts_Order_Report_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Parts Order Report List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Parts Order Report List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
		<th>#</th>
		<th>ID</th>
		<th>Entered </th>
		<th>Ticket </th>
		<th>Customer </th>
		<th>Description</th>
		<th>Price</th>                                                            
		<th>Store</th>
		<th>Bought</th>
		<th>Tracking</th>
		<th>Arrived</th>
		</tr>
</thead>        
<tbody>";

			$i=1;
			if(!empty($sql_parts_order))
			foreach($sql_parts_order as $row): 
			$cids=$obj->SelectAllByVal("ticket","ticket_id",$row->ticket_id,"cid");
				
		$data.="<tr>
				<td>".$i."</td>
				<td>".$row->id."</td>
				<td>".$row->date."</td>
				<td>".$row->ticket_id."</td>
				<td>".$obj->SelectAllByVal("coustomer","id",$cids,"businessname")."</td>
				<td>".$row->description."</td>
				<td>".$row->cost."</td>
				<td>".$row->store."</td>
				<td>".$row->ordered."</td>
				<td>".$row->trackingnum."</td>
				<td>".$row->received."</td>
			</tr>";
			
			$i++; endforeach;
			
$data .="</tbody><tfoot><tr>
		<th>#</th>
		<th>ID</th>
		<th>Entered </th>
		<th>Ticket </th>
		<th>Customer </th>
		<th>Description</th>
		<th>Price</th>                                                            
		<th>Store</th>
		<th>Bought</th>
		<th>Tracking</th>
		<th>Arrived</th>
		</tr></tfoot></table>";
		
		
				
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf")
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sql_parts_order = $report->SelectAllDate("parts_order",$from,$to,"1");
			$record = $report->SelectAllDate("parts_order",$from,$to,"2");
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sql_parts_order = $obj->SelectAll("parts_order");
			$record = $obj->totalrows("parts_order");
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sql_parts_order = $report->SelectAllDate_Store("parts_order",$from,$to,"1","input_by",$input_by);
			$record = $report->SelectAllDate_Store("parts_order",$from,$to,"2","input_by",$input_by);
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sql_parts_order = $obj->SelectAllByID("parts_order",array("input_by"=>$input_by));
			$record = $obj->exists_multiple("parts_order",array("input_by"=>$input_by));
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Parts Order Report List Report
						</td>
					</tr>
				</table>
				
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td>Parts Order Report List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>

        <tr style='background:#09f; color:#fff;'>
		<th>#</th>
		<th>ID</th>
		<th>Entered </th>
		<th>Ticket </th>
		<th>Customer </th>
		<th>Description</th>
		<th>Price</th>                                                            
		<th>Store</th>
		<th>Bought</th>
		<th>Tracking</th>
		<th>Arrived</th>
		</tr>
</thead>        
<tbody>";
	
	
			$i=1;
			if(!empty($sql_parts_order))
			foreach($sql_parts_order as $row): 
			$cids=$obj->SelectAllByVal("ticket","ticket_id",$row->ticket_id,"cid");
				
			$html.="<tr>
				<td>".$i."</td>
				<td>".$row->id."</td>
				<td>".$row->date."</td>
				<td>".$row->ticket_id."</td>
				<td>".$obj->SelectAllByVal("coustomer","id",$cids,"businessname")."</td>
				<td>".$row->description."</td>
				<td>".$row->cost."</td>
				<td>".$row->store."</td>
				<td>".$row->ordered."</td>
				<td>".$row->trackingnum."</td>
				<td>".$row->received."</td>
			</tr>";
			
			$i++; endforeach;
			
	$html.="</tbody><tfoot><tr>
		<th>#</th>
		<th>ID</th>
		<th>Entered </th>
		<th>Ticket </th>
		<th>Customer </th>
		<th>Description</th>
		<th>Price</th>                                                            
		<th>Store</th>
		<th>Bought</th>
		<th>Tracking</th>
		<th>Arrived</th>
		</tr></tfoot></table>";
			
			
    $html.="</td></tr>";
    $html.="</tbody></table>";
    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0; 
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); 
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                        	
                            <?php 
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sql_parts_order = $report->SelectAllDate("parts_order",$from,$to,"1");
												$record = $report->SelectAllDate("parts_order",$from,$to,"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sql_parts_order = $obj->SelectAllByID("parts_order",array("date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("parts_order",array("date"=>date('Y-m-d')));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										if($input_status==5)
										{
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												if(isset($_GET['from']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_parts_order=$obj_report_chain->ReportQuery_Datewise_Or("parts_order",$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
													$record =$obj_report_chain->ReportQuery_Datewise_Or("parts_order",$array_ch,"input_by",$_GET['from'],$_GET['to'],"2");
													$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
												}
												else
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_parts_order=$obj_report_chain->SelectAllByID_Multiple2_Or("parts_order",array("date"=>date('Y-m-d')),$array_ch,"input_by","1");
													$record =$obj_report_chain->SelectAllByID_Multiple2_Or("parts_order",array("date"=>date('Y-m-d')),$array_ch,"input_by","2");
													$record_label="Total record Found ( ".$record." ).";
												}
												//echo "Work";
											}
											else
											{
												//echo "Not Work";
												$sqlinvoice="";
												$record =0;
												$record_label="Total record Found ( ".$record." ).";
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sql_parts_order = $report->SelectAllDate_Store("parts_order",$from,$to,"1","input_by",$input_by);
												$record = $report->SelectAllDate_Store("parts_order",$from,$to,"2","input_by",$input_by);
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sql_parts_order = $obj->SelectAllByID_Multiple("parts_order",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("parts_order",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										?>
                            <h5><i class="font-money"></i> Parts Order List | <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->


<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->

                                        <!-- General form elements -->
                                        <div class="row-fluid block">


                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>ID</th>
                                                            <th>Entered </th>
                                                            <th>Ticket </th>
                                                            <th>Customer </th>
                                                            <th>Description</th>
                                                            <th>Price</th>                                                            <th>Store</th>
                                                            <th>Bought</th>
                                                            <th>Tracking</th>
                                                            <th>Arrived</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
														$i=1;
                                                        if(!empty($sql_parts_order))
                                                        foreach($sql_parts_order as $row): 
														$cids=$obj->SelectAllByVal("ticket","ticket_id",$row->ticket_id,"cid");
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td> <?php echo $row->id; ?> </td>
                                                            <td> <?php echo $row->date; ?> </td>
                                                            
                                                            <td><a href="view_tickets.php?ticket_id=<?php echo $row->ticket_id; ?>"><?php echo $row->ticket_id; ?></a></td>
                                                            <td><a href="customer.php?edit=<?php echo $cids; ?>">
                                                            <?php echo $obj->SelectAllByVal("coustomer","id",$cids,"businessname"); ?>
                                                            </a></td>
                                                            <td><label class="label label-success"> <?php echo $row->description; ?>  </label></td>
                                                            <td> <?php echo $row->cost; ?> </td>
                                                            <td><?php echo $row->store; ?></td>
                                                            <td><?php echo $row->ordered; ?></td>
                                                            <td><a target="_blank" href="http://www.google.com/search?&amp;q=<?php echo $row->trackingnum; ?>"><?php echo $row->trackingnum; ?></a></td>
                                                            <td><?php echo $row->received; ?></td>
                                                        </tr>
                                                        <?php 
                                                        $i++;
                                                        endforeach; 
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

                            <?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>

                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
