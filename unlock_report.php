<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
if(@$_GET['export']=="excel") 
{
if($input_status==1)
{
	if(isset($_GET['from']))
	{
		$from=$_GET['from'];
		$to=$_GET['to'];
$sql_parts_order = $report->SelectAllDate("unlock_request",$from,$to,"1");
		$record = $report->SelectAllDate("unlock_request",$from,$to,"2");
$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
	}
	else
	{
		$sql_parts_order = $obj->SelectAll("unlock_request");
		$record = $obj->totalrows("unlock_request");
		$record_label="Total Record Found ( ".$record." )"; 
	}
}
else
{
	if(isset($_GET['from']))
	{
		$from=$_GET['from'];
		$to=$_GET['to'];
$sql_parts_order = $report->SelectAllDate_Store("unlock_request",$from,$to,"1","uid",$input_by);
$record = $report->SelectAllDate_Store("unlock_request",$from,$to,"2","uid",$input_by);
$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
	}
	else
	{
$sql_parts_order = $obj->SelectAllByID("unlock_request",array("uid"=>$input_by));
$record = $obj->exists_multiple("unlock_request",array("uid"=>$input_by));
		$record_label="Total Record Found ( ".$record." )"; 
	}
}

header('Content-type: application/excel');
$filename ="Unlock_Request_Order_Report_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Unlock Request Order Report List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Unlock Request Order Report List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Our Cost</th>
			<th>Retail Cost</th>
			<th>Profit</th>
			<th>Service</th>
			<th>Created</th>
			<th>Status</th>
			<th>Submit</th>
		</tr>
</thead>        
<tbody>";

	$i=1;
	$our_cost=0;
	$retail_cost=0;
	$profit=0;
	$tqq=0;
	if(!empty($sql_parts_order))
	foreach($sql_parts_order as $ticket): 
	
$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
if($curcheck!=0)		
$tqq+=1;						
		$a=$ticket->our_cost;
		$b=$ticket->retail_cost;
		$c=$b-$a;
	$our_cost+=$a;
	$retail_cost+=$b;
	$profit+=$c;
				
			$data.="<tr>
				<td>".$i."</td>
				<td>".$ticket->unlock_id."</td>
				<td>".$ticket->our_cost."</td>
				<td>".$ticket->retail_cost."</td>
				<td>".$c."</td>
				<td>".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name")."</td>
				<td>".$ticket->date."</td>
				<td>".$obj->ticket_status($ticket->status)."</td>
				<td>".$obj->duration($ticket->date,date('Y-m-d'))."</td>
			</tr>";
			
			$i++; endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Our Cost</th>
			<th>Retail Cost</th>
			<th>Profit</th>
			<th>Service</th>
			<th>Created</th>
			<th>Status</th>
			<th>Submit</th>
		</tr></tfoot></table>";
		
		
				
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf")
{
if($input_status==1)
{
	if(isset($_GET['from']))
	{
		$from=$_GET['from'];
		$to=$_GET['to'];
$sql_parts_order = $report->SelectAllDate("unlock_request",$from,$to,"1");
		$record = $report->SelectAllDate("unlock_request",$from,$to,"2");
$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
	}
	else
	{
		$sql_parts_order = $obj->SelectAll("unlock_request");
		$record = $obj->totalrows("unlock_request");
		$record_label="Total Record Found ( ".$record." )"; 
	}
}
else
{
	if(isset($_GET['from']))
	{
		$from=$_GET['from'];
		$to=$_GET['to'];
$sql_parts_order = $report->SelectAllDate_Store("unlock_request",$from,$to,"1","uid",$input_by);
$record = $report->SelectAllDate_Store("unlock_request",$from,$to,"2","uid",$input_by);
$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
	}
	else
	{
$sql_parts_order = $obj->SelectAllByID("unlock_request",array("uid"=>$input_by));
$record = $obj->exists_multiple("unlock_request",array("uid"=>$input_by));
		$record_label="Total Record Found ( ".$record." )"; 
	}
}
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Unlock Request Order Report List Report
						</td>
					</tr>
				</table>
				
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td>Unlock Request Order Report List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>

        <tr style='background:#09f; color:#fff;'>
		<th>#</th>
		<th>Unlock ID</th>
		<th>Our Cost</th>
		<th>Retail Cost</th>
		<th>Profit</th>
		<th>Service</th>
		<th>Created</th>
		<th>Status</th>
		<th>Submit</th>
		</tr>
</thead>        
<tbody>";
	

	$i=1;
	$our_cost=0;
	$retail_cost=0;
	$profit=0;
	$tqq=0;
	if(!empty($sql_parts_order))
	foreach($sql_parts_order as $ticket): 
	
$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
if($curcheck!=0)		
$tqq+=1;						
		$a=$ticket->our_cost;
		$b=$ticket->retail_cost;
		$c=$b-$a;
	$our_cost+=$a;
	$retail_cost+=$b;
	$profit+=$c;
				
			$html.="<tr>
				<td>".$i."</td>
				<td>".$ticket->unlock_id."</td>
				<td>".$ticket->our_cost."</td>
				<td>".$ticket->retail_cost."</td>
				<td>".$c."</td>
				<td>".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name")."</td>
				<td>".$ticket->date."</td>
				<td>".$obj->ticket_status($ticket->status)."</td>
				<td>".$obj->duration($ticket->date,date('Y-m-d'))."</td>
			</tr>";
			
			$i++; endforeach;
			
	$html.="</tbody><tfoot><tr>
		<th>#</th>
		<th>Unlock ID</th>
		<th>Our Cost</th>
		<th>Retail Cost</th>
		<th>Profit</th>
		<th>Service</th>
		<th>Created</th>
		<th>Status</th>
		<th>Submit</th>
		</tr></tfoot></table>";
			
			
    $html.="</td></tr>";
    $html.="</tbody></table>";
    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0; 
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); 
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                                                        <?php 
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sql_parts_order = $report->SelectAllDate("unlock_request",$from,$to,"1");
												$record = $report->SelectAllDate("unlock_request",$from,$to,"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sql_parts_order = $obj->SelectAll("unlock_request");
												$record = $obj->totalrows("unlock_request");
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										elseif($input_status==5)
										{
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												include('class/report_chain_admin.php');	
												$obj_report_chain = new chain_report();
												
												if(isset($_GET['from']))
												{
													$from=$_GET['from'];
													$to=$_GET['to'];
													$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("unlock_request",$array_ch,"uid","1");
													$record=$obj_report_chain->SelectAllByID_Multiple_Or("unlock_request",$array_ch,"uid","2");
													$record_label="| Report Generate Between ".$from." - ".$to;
												}
												else
												{
													$sqlticket =$obj_report_chain->SelectAllByID_Multiple2_Or("unlock_request",array("date"=>date('Y-m-d')),$array_ch,"uid","1");
													$record =$obj_report_chain->SelectAllByID_Multiple2_Or("unlock_request",array("date"=>date('Y-m-d')),$array_ch,"uid","2");
													$record_label="Total Record ( ".$record." )"; 
												}
												
												
											}
											else
											{
												//echo "Not Work";
												$sqlticket ="";
												$record =0;
												$record_label="Total Record ( ".$record." )"; 
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sql_parts_order = $report->SelectAllDate_Store("unlock_request",$from,$to,"1","uid",$input_by);
												$record = $report->SelectAllDate_Store("unlock_request",$from,$to,"2","uid",$input_by);
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sql_parts_order = $obj->SelectAllByID("unlock_request",array("uid"=>$input_by));
												$record = $obj->exists_multiple("unlock_request",array("uid"=>$input_by));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										?>
                            <h5><i class="font-money"></i> Unlock Request Report List | <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
						<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Unlock ID</th>
                                                            <th>Our Cost</th>
                                                            <th>Retail Cost</th>
                                                            <th>Profit</th>
                                                            <th>Service</th>
                                                            <th>Created</th>
                                                            <th>Status</th>
                                                            <th>Submit</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php														
                                                        $i=1;
                                                        $our_cost=0;
														$retail_cost=0;
														$profit=0;
														$tqq=0;
														if(!empty($sql_parts_order))
                                                        foreach($sql_parts_order as $ticket): 
														
				$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
				$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
				$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
				if($curcheck!=0)
				{				
				$tqq+=1;						
															$a=$ticket->our_cost;
															$b=$ticket->retail_cost;
															$c=$b-$a;
														$our_cost+=$a;
														$retail_cost+=$b;
														$profit+=$c;
															
														?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><a class="label label-success" href="view_unlock.php?unlock_id=<?php echo $ticket->unlock_id; ?>"><i class="icon-tags"></i> <?php echo $ticket->unlock_id; ?></a></td>
                                                                <!--<td><i class="icon-user"></i> <?php //echo $obj->SelectAllByVal("coustomer","id",$ticket->cid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"lastname"); ?></td>-->
                                                                <td>$<?php echo $ticket->our_cost; ?></td>
                                                                <td>$<?php echo $ticket->retail_cost; ?></td>
                                                                <td>$<?php echo $c; ?></td>
                                                                <td><?php echo $obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name"); ?></td>
                                                                <td><i class="icon-calendar"></i> <?php echo $ticket->date; ?></td>
                
                                                                <td><?php echo $obj->ticket_status($ticket->status); ?></td>
                                                                <td><label class="label label-info"><i class="icon-calendar"></i> <?php echo $obj->duration($ticket->date,date('Y-m-d')); ?></label></td>
                
                                                            </tr>
                                                        <?php $i++; 
														
				}
														endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-overflow">
                                                <table style="width:300px; float:left; margin-top:10px;" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Unlock Short Report</th>
                                                        </tr>
                                                        <tr>
                                                            <th>1</th>
                                                            <th>Quantity</th>
                                                            <th><?php echo $tqq; ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>2</th>
                                                            <th>Our Total Cost</th>
                                                            <th>$<?php echo $our_cost; ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>3</th>
                                                            <th>Retail Total Cost</th>
                                                            <th>$<?php echo $retail_cost; ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>4</th>
                                                            <th>Profit Total</th>
                                                            <th>$<?php echo $profit; ?></th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>
                                            </div>



                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

							<?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>


                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
