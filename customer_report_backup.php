<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i>Customer Report Info</h5>
                            <ul class="icons">
                                <li>
                                    <a data-toggle="modal" href="#myModal1" class="hovertip" title="Search Customer Report">
                                        <i class="icon-calendar"></i>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        
                                        <?php
						if(isset($_GET['from']))
						{
							if(!empty($_GET['to']))
							{
								$to=$_GET['to'];	
							}
							else
							{
								$to=date('Y-m-d');	
							}
								?>
								<h4>Customer Report Generated For Date : <?php echo $_GET['from']; ?> To <?php echo $to; ?></h4>
                                <?php
						}
						else
						{
						?>
                        <!-- Dialog content -->
                            <div class="modal-body">
                                <form class="form-horizontal" action="<?php echo $obj->filename(); ?>" method="get">
                                    <div class="row-fluid">
                                        
                                        <div class="control-group">
                                            <label class="control-label"><strong>Date Search:</strong></label>
                                            <div class="controls">
                                                <ul class="dates-range">
                                                    <li><input type="text" id="fromDate" name="from" placeholder="From" /></li>
                                                    <li class="sep">-</li>
                                                    <li><input type="text" id="toDate" name="to" placeholder="To" /></li>
                                                    <li class="sep">&nbsp;</li>
                                                    <li><button class="btn btn-primary" type="submit">Search Report</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </form>
                            </div>
                        <!-- /dialog content -->
                        <?php } ?>
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name/Business</th>
                                                            <th>Sales</th>
                                                            <th>Invoice</th>
                                                            <th>Estimate</th>
                                                            <th>Ticket</th>
                                                            <th>Parts Order</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    
										<?php 
										
										
										
										
										
										
										if(isset($_GET['from']))
										{
											if(!empty($_GET['to']))
											{
												$to=$_GET['to'];	
											}
											else
											{
												$to=date('Y-m-d');	
											}
											
											$from=$_GET['from'];
											
											if($input_status==1)
											{
													$sql_coustomer=$obj->SelectAll_ddate($table,"date",$_GET['from'],$_GET['to']);
												
											}
											else
											{
												
													$sql_coustomer=$obj_report->ReportQuery_Datewise($table,array("input_by"=>$input_by),$_GET['from'],$_GET['to'],"1");
												
											}
											$i=1;
											if(!empty($sql_coustomer))
											foreach($sql_coustomer as $customer): ?>
											<tr>
												<td><?php echo $i; ?></td>
										<td><?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?></td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",3,$from,$to); ?> Sales</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",1,$from,$to); ?> Invoice</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",2,$from,$to); ?> Estimate</td>
										<td><?php echo $report->count_ticket_report_date($customer->id,"ticket",$from,$to); ?> Ticket</td>
										<td><?php echo $report->count_parts_report_date($customer->id,"parts_order",$from,$to); ?> Parts</td>
										<td><a href="customer_all_report.php?cid=<?php echo $customer->id; ?>" class="btn btn-success"><i class="icon-list"></i> View All Report</a></td>
											</tr>
											<?php $i++; endforeach;	
										}
										elseif(isset($_GET['all']))
										{
											if($input_status==1)
											{
													$sql_coustomer=$obj->SelectAll($table);
											}
											else
											{
													$sql_coustomer=$obj->SelectAllByID($table,array("input_by"=>$input_by));
												
											}
											$i=1;
											if(!empty($sql_coustomer))
											foreach($sql_coustomer as $customer): ?>
											<tr>
												<td><?php echo $i; ?></td>
										<td><?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?></td>
										<td><?php echo $report->count_invoice_report($customer->id,"invoice",3); ?> Sales</td>
										<td><?php echo $report->count_invoice_report($customer->id,"invoice",1); ?> Invoice</td>
										<td><?php echo $report->count_invoice_report($customer->id,"invoice",2); ?> Estimate</td>
										<td><?php echo $report->count_ticket_report($customer->id,"ticket"); ?> Ticket</td>
										<td><?php echo $report->count_parts_report($customer->id,"parts_order"); ?> Parts</td>
										<td><a href="customer_all_report.php?cid=<?php echo $customer->id; ?>" class="btn btn-success"><i class="icon-list"></i> View All Report</a></td>
											</tr>
											<?php $i++; endforeach;
										}
										else
										{
											$to=date('Y-m-d');	
											$from=date('Y-m-d');
											if($input_status==1)
											{
	
													$sql_coustomer=$obj->SelectAllByID($table,array("date"=>date('Y-m-d')));
											}
											else
											{
													$sql_coustomer=$obj->SelectAllByID($table,array("input_by"=>$input_by));
											}
											$i=1;
											if(!empty($sql_coustomer))
											foreach($sql_coustomer as $customer): ?>
											<tr>
												<td><?php echo $i; ?></td>
										<td><?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?></td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",3,$from,$to); ?> Sales</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",1,$from,$to); ?> Invoice</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",2,$from,$to); ?> Estimate</td>
										<td><?php echo $report->count_ticket_report_date($customer->id,"ticket",$from,$to); ?> Ticket</td>
										<td><?php echo $report->count_parts_report_date($customer->id,"parts_order",$from,$to); ?> Parts</td>
										<td><a href="customer_all_report.php?cid=<?php echo $customer->id; ?>" class="btn btn-success"><i class="icon-list"></i> View All Report</a></td>
											</tr>
											<?php $i++; endforeach;	
										}
										?>
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
