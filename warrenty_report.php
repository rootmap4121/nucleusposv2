<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
if (isset($_GET['del'])) 
{
    $obj->deletesing("id",$_GET['del'],"sales");
}
if(@$_GET['export']=="excel") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlinvoice = $report->SelectAllDate("warrenty",$from,$to,"1");
			$record = $report->SelectAllDate("warrenty",$from,$to,"2");
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAll("warrenty");
			$record = $obj->totalrows("warrenty");
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlinvoice = $report->SelectAllDate_Store("warrenty",$from,$to,"1","uid",$input_by);
			$record = $report->SelectAllDate_Store("warrenty",$from,$to,"2","uid",$input_by);
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAllByID("warrenty",array("uid"=>$input_by));
			$record = $obj->exists_multiple("warrenty",array("uid"=>$input_by));
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}


header('Content-type: application/excel');
$filename ="Warrenty_Report_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Warrenty Report List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Warrenty Report List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
		<th>#</th>
		<th>Warranty-ID</th>
		<th>Service</th>
		<th>Warrenty </th>
		<th>W.Left </th>
		<th>Date</th>
		<th>Retail Cost</th>
		<th>Reason for Warranty</th>
		</tr>
</thead>        
<tbody>";

			$i = 1;
			$a=0;
			$bb=0;
			if (!empty($sqlinvoice))
			foreach ($sqlinvoice as $invoice):
				$a+=$i;
				$daysgone2=$obj->daysgone($invoice->date,date('Y-m-d')); 
				//echo $wd." Days";
				$have2=$invoice->warrenty-$daysgone2;
				
				if($invoice->type=="ticket")
				{
				  $ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost");
				  $bb+=$ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost");
				}
				
				if($invoice->type=="checkin")
				{
				    $ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost");
					$bb+=$ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost");
				}
				
			$data.="<tr>
				<td>".$i."</td>
				<td>".$invoice->warrenty_id."</td>
				<td>".$invoice->type."</td>
				<td>".$invoice->warrenty." Days</td>
				<td>".$have2." Days</td>
				<td>".$invoice->date."</td>
				<td>".$ourcost."</td>
				<td>".$invoice->note."</td>
				<td>".$ticket->date."</td>
			</tr>";
			
			$i++; endforeach;
			
$data .="</tbody><tfoot><tr>
		<th>#</th>
		<th>Warranty-ID</th>
		<th>Service</th>
		<th>Warrenty </th>
		<th>W.Left </th>
		<th>Date</th>
		<th>Retail Cost</th>
		<th>Reason for Warranty</th>
		</tr></tfoot></table>";
		
		$data.="<table border='0' width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong> ".$record."</strong></td>
						</tr>
						<tr>
							<td>2. Our Total Cost = <strong> $".number_format($bb,2)."</strong></td>
						</tr>
					</tbody>
				</table>";
		
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlinvoice = $report->SelectAllDate("warrenty",$from,$to,"1");
			$record = $report->SelectAllDate("warrenty",$from,$to,"2");
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAll("warrenty");
			$record = $obj->totalrows("warrenty");
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlinvoice = $report->SelectAllDate_Store("warrenty",$from,$to,"1","uid",$input_by);
			$record = $report->SelectAllDate_Store("warrenty",$from,$to,"2","uid",$input_by);
			$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAllByID("warrenty",array("uid"=>$input_by));
			$record = $obj->exists_multiple("warrenty",array("uid"=>$input_by));
			$record_label="Total Record Found ( ".$record." )"; 
		}
	}
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Warrenty Report List Report
						</td>
					</tr>
				</table>
				
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td>Warrenty Report List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>

        <tr style='background:#09f; color:#fff;'>
		<th>#</th>
		<th>Warranty-ID</th>
		<th>Service</th>
		<th>Warrenty </th>
		<th>W.Left </th>
		<th>Date</th>
		<th>Retail Cost</th>
		<th>Reason for Warranty</th>
		</tr>
</thead>        
<tbody>";
	
	
			$i = 1;
			$a=0;
			$bb=0;
			if (!empty($sqlinvoice))
			foreach ($sqlinvoice as $invoice):
				$a+=$i;
				$daysgone2=$obj->daysgone($invoice->date,date('Y-m-d')); 
				//echo $wd." Days";
				$have2=$invoice->warrenty-$daysgone2;
				
				if($invoice->type=="ticket")
				{
				  $ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost");
				  $bb+=$ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost");
				}
				
				if($invoice->type=="checkin")
				{
				    $ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost");
					$bb+=$ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost");
				}
				
			$html.="<tr>
				<td>".$i."</td>
				<td>".$invoice->warrenty_id."</td>
				<td>".$invoice->type."</td>
				<td>".$invoice->warrenty." Days</td>
				<td>".$have2." Days</td>
				<td>".$invoice->date."</td>
				<td>".$ourcost."</td>
				<td>".$invoice->note."</td>
				<td>".$ticket->date."</td>
			</tr>";
			
			$i++; endforeach;
			
	$html.="</tbody><tfoot><tr>
		<th>#</th>
		<th>Warranty-ID</th>
		<th>Service</th>
		<th>Warrenty </th>
		<th>W.Left </th>
		<th>Date</th>
		<th>Retail Cost</th>
		<th>Reason for Warranty</th>
		</tr></tfoot></table>";
		
		$html.="<table border='0'  width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong> ".$record."</strong></td>
						</tr>
						<tr>
							<td>2. Our Total Cost = <strong> $".number_format($bb,2)."</strong></td>
						</tr>
					</tbody>
				</table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";
    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0; 
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); 
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header">
                        <!-- Page header -->
                        				<?php 
										echo $obj->ShowMsg();
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sqlinvoice = $report->SelectAllDate("warrenty",$from,$to,"1");
												$record = $report->SelectAllDate("warrenty",$from,$to,"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAllByID("warrenty",array("date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("warrenty",array("date"=>date('Y-m-d')));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										elseif($input_status==5)
										{
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												if(isset($_GET['from']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sqlinvoice=$obj_report_chain->ReportQuery_Datewise_Or("warrenty",$array_ch,"uid",$_GET['from'],$_GET['to'],"1");
													$record =$obj_report_chain->ReportQuery_Datewise_Or("warrenty",$array_ch,"uid",$_GET['from'],$_GET['to'],"2");
													$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
												}
												else
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple2_Or("warrenty",array("date"=>date('Y-m-d')),$array_ch,"uid","1");
													$record =$obj_report_chain->SelectAllByID_Multiple2_Or("warrenty",array("date"=>date('Y-m-d')),$array_ch,"uid","2");
													$record_label="Total record Found ( ".$record." ).";
												}
												//echo "Work";
											}
											else
											{
												//echo "Not Work";
												$sqlinvoice="";
												$record =0;
												$record_label="Total record Found ( ".$record." ).";
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sqlinvoice = $report->SelectAllDate_Store("warrenty",$from,$to,"1","uid",$input_by);
												$record = $report->SelectAllDate_Store("warrenty",$from,$to,"2","uid",$input_by);
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAllByID_Multiple("warrenty",array("uid"=>$input_by,"date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("warrenty",array("uid"=>$input_by,"date"=>date('Y-m-d')));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										?>
                            <h5><i class="font-money"></i> Warranty Report | <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        
<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Warranty-ID</th>
                                                            <th>Service</th>
                                                            <th>Warrenty </th>
                                                            <th>W.Left </th>
        													<th>Date</th>
                                                            <th>Retail Cost</th>
                                                            <th>Reason for Warranty</th>
                                                            <th width="80">Print</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
															$i = 1;
															$a=0;
															$bb=0;
															if (!empty($sqlinvoice))
                                                            foreach ($sqlinvoice as $invoice):
                                                                $a+=$i;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $i; ?></td>
                                                                        <td>
                                                                        
                                                                        <?php
																		if($invoice->type=="ticket")
																		{
																		?>
                                                                        <a href="view_ticket.php?ticket_id=<?php echo $invoice->warrenty_id; ?>"  target="_blank" onclick="javascript:return confirm('Are you absolutely sure to View This <?php echo $invoice->type; ?> ?')"  class="label label-important"> <?php echo $invoice->warrenty_id; ?></a>
                                                                        <?php }elseif($invoice->type=="checkin"){ ?>
                                                                        <a href="view_checkin.php?ticket_id=<?php echo $invoice->warrenty_id; ?>"  target="_blank" onclick="javascript:return confirm('Are you absolutely sure to View This <?php echo $invoice->type; ?> ?')"  class="label label-important"> <?php echo $invoice->warrenty_id; ?></a>
                                                                        <?php } ?>
                                                                        
                                                                        </td>
                                                                        <td><label class="label label-success"> <?php echo $invoice->type; ?> </label></td>
                                                                        <td><label class="label label-warning"> <?php echo $invoice->warrenty; ?> Days </label></td>
                                                                        <td><label class="label label-warning"> <?php 
															$daysgone2=$obj->daysgone($invoice->date,date('Y-m-d')); 
															//echo $wd." Days";
															$have2=$invoice->warrenty-$daysgone2;
															echo $have2." Days";
																		 ?></label></td>
        
                                                                       
                                                                        <td><?php echo $invoice->date; ?></td>
                                                                        <td>$
																		<?php 
                                                                        if($invoice->type=="ticket")
                                                                        {
                                                                          echo number_format($ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost"),2);
																		  $bb+=$ourcost=$obj->SelectAllByVal("ticket_list","ticket_id",$invoice->warrenty_id,"our_cost");
                                                                        }
                                                                        
                                                                        if($invoice->type=="checkin")
                                                                        {
                                                                           echo number_format($ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost"),2);
																		    $bb+=$ourcost=$obj->SelectAllByVal("checkin_list","checkin_id",$invoice->warrenty_id,"retail_cost");
                                                                        }
                                                                        ?>
                                                                        </td>
                                                                        <td><?php echo $invoice->note; ?></td>
                                                                        <td><a href="warrenty_print.php?warrenty_id=<?php echo $invoice->warrenty_id; ?>&print_invoice=yes"  target="_blank" onclick="javascript:return confirm('Are you absolutely sure to Print This Invoice ?')"  class="label label-important"><i class="icon-print"></i></a></td>                                      
                                                                    </tr>
                                                                    <?php 
																	$i++;
                                                                 endforeach;
                                                        ?>
                                                    </tbody>
                                                    </table>
                                            </div>
                                        <!-- Table condensed -->
                                        <div class="block well span4" style="margin-left:0;">
                                            <div class="navbar">
                                                <div class="navbar-inner">
                                                    <h5> Report</h5>
                                                </div>
                                            </div>
                                            <div class="table-overflow">
                                <table class="table table-condensed">
                                    <tbody>
                                    	<tr>
                                            <td>1. Total Quantity = <strong> <?php echo $record; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>2. Our Total Cost = <strong> $<?php echo number_format($bb,2); ?></strong></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                            </div>
                                        </div>
                                        <!-- /table condensed -->


                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

                            <?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>

                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
