<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
if(@$_GET['export']=="excel") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
		$sqlinvoice = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"1");
			$record = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"2");
			$record_label="Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAllByID_Multiple("invoice",array("doc_type"=>3));
			$record = $obj->exists_multiple("invoice",array("doc_type"=>3));
			$record_label=""; 
		}
	}
	elseif($input_status==5)
	{
		
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		{
			$array_ch = array();
			foreach($sqlchain_store_ids as $ch):
				array_push($array_ch,$ch->store_id);
			endforeach;
			
			    include('class/report_chain_admin.php');	
				$obj_report_chain = new chain_report();
				$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3),$array_ch,"input_by","1");
				$record=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3),$array_ch,"input_by","2");
				$record_label="Report Generate Between ".date('Y-m-d');
			
			//echo "Work";
		}
		else
		{
			//echo "Not Work";
			$sqlinvoice="";
			$record_label="";
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
$sqlinvoice = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"1");
$record = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"2");
			$record_label="Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by));
			$record = $obj->exists_multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by));
			$record_label=""; 
		}
	}

header('Content-type: application/excel');
$filename ="Sales_Report_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sales Report List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Sales Report List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Invoice-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Tender</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr>
</thead>        
<tbody>";

						$a=0; $a1=0; $a2=0;
			$i = 1;
			if (!empty($sqlinvoice))
				foreach ($sqlinvoice as $invoice):
					?>
					<?php
					$sqlitem = $obj->SelectAllByID_Multiple("sales", array("sales_id" => $invoice->invoice_id));
					$item_q = 0;
					$total = 0;
					if (!empty($sqlitem))
						foreach ($sqlitem as $item):
							$rr = $item->quantity * $item->single_cost;
							
					if($obj->exists_multiple("pos_tax",array("invoice_id"=>$invoice->invoice_id,"status"=>2))==0)
							{
							$taxchk=1;
							
							}
							else
							{
							$taxchk=0;
							}
							if ($taxchk == 0) {
								$tax = 0;
							} else {
								$tax = ($rr * $tax_per_product) / 100;
							}

							$tot = $rr + $tax;
							$total+=$tot;
							$item_q+=$item->quantity;
						endforeach;
					if ($total != 0) {
						$a+=1;
						
			$chkpayment_id=$obj->SelectAllByVal("invoice_payment","invoice_id",$invoice->invoice_id,"payment_type");
			$a1+=$item_q;
			$a2+=$total; 			
			$data.="<tr>
				<td>".$i."</td>
				<td>".$invoice->invoice_id."</td>
				<td>".$obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname")."</td>
				<td>".$obj->invoice_paid_status($invoice->status)."</td>
				<td>".$obj->SelectAllByVal("payment_method","id",$chkpayment_id,"meth_name")."</td>
				<td>".$invoice->date."</td>
				<td>".$item_q."</td>
				<td>".number_format($total, 2)."</td>
			</tr>";
			
			$i++; } endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Invoice-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Tender</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr></tfoot></table>";
		
		
		
		
		$data.="<table border='0' width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong> ".$a."</strong></td>
						</tr>
						<tr>
							<td>2. Total Quantity Sold  = <strong> ".$a1."</strong></td>
						</tr>
						<tr>
							<td>4. Total Sales = <strong> $".number_format($a2,2)."</strong></td>
						</tr>
					</tbody>
				</table>";
		
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
		$sqlinvoice = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"1");
			$record = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"2");
			$record_label="Report Generate Between ".$from." - ".$to;
		}
		else
		{
	$sqlinvoice = $obj->SelectAllByID_Multiple("invoice",array("doc_type"=>3));
	$record = $obj->exists_multiple("invoice",array("doc_type"=>3));
			$record_label=""; 
		}
	}
	elseif($input_status==5)
	{
		
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		{
			$array_ch = array();
			foreach($sqlchain_store_ids as $ch):
				array_push($array_ch,$ch->store_id);
			endforeach;
			
			    include('class/report_chain_admin.php');	
				$obj_report_chain = new chain_report();
				$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3),$array_ch,"input_by","1");
				$record=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3),$array_ch,"input_by","2");
				$record_label="Report Generate Between ".date('Y-m-d');
			
			//echo "Work";
		}
		else
		{
			//echo "Not Work";
			$sqlinvoice="";
			$record_label="";
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
$sqlinvoice = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"1");
$record = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"2");
			$record_label="Report Generate Between ".$from." - ".$to;
		}
		else
		{
$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by));
$record = $obj->exists_multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by));
			$record_label=""; 
		}
	}
	    
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Sales Report List Report
						</td>
					</tr>
				</table>
				

				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Sales Report List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Invoice-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Tender</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr>
</thead>        
<tbody>";
	
	
			$a=0; $a1=0; $a2=0;
			$i = 1;
			if (!empty($sqlinvoice))
				foreach ($sqlinvoice as $invoice):
					?>
					<?php
					$sqlitem = $obj->SelectAllByID_Multiple("sales", array("sales_id" => $invoice->invoice_id));
					$item_q = 0;
					$total = 0;
					if (!empty($sqlitem))
						foreach ($sqlitem as $item):
							$rr = $item->quantity * $item->single_cost;
							
					if($obj->exists_multiple("pos_tax",array("invoice_id"=>$invoice->invoice_id,"status"=>2))==0)
							{
							$taxchk=1;
							
							}
							else
							{
							$taxchk=0;
							}
							if ($taxchk == 0) {
								$tax = 0;
							} else {
								$tax = ($rr * $tax_per_product) / 100;
							}

							$tot = $rr + $tax;
							$total+=$tot;
							$item_q+=$item->quantity;
						endforeach;
					if ($total != 0) {
						$a+=1;
						
			$chkpayment_id=$obj->SelectAllByVal("invoice_payment","invoice_id",$invoice->invoice_id,"payment_type");
			$a1+=$item_q;
			$a2+=$total; 			
			$html.="<tr>
				<td>".$i."</td>
				<td>".$invoice->invoice_id."</td>
				<td>".$obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname")."</td>
				<td>".$obj->invoice_paid_status($invoice->status)."</td>
				<td>".$obj->SelectAllByVal("payment_method","id",$chkpayment_id,"meth_name")."</td>
				<td>".$invoice->date."</td>
				<td>".$item_q."</td>
				<td>".number_format($total, 2)."</td>
			</tr>";
			
			$i++; } endforeach;
			
	$html.="</tbody><tfoot><tr>
			<th>#</th>
			<th>Invoice-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Tender</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr></tfoot></table>";
		
		$html.="<table border='0'  width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong> ".$a."</strong></td>
						</tr>
						<tr>
							<td>2. Total Quantity Sold  = <strong> ".$a1."</strong></td>
						</tr>
						<tr>
							<td>4. Total Sales = <strong> $".number_format($a2,2)."</strong></td>
						</tr>
					</tbody>
				</table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                        				<?php 
										echo $obj->ShowMsg();
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
											$sqlinvoice = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"1");
												$record = $report->SelectAllDateCond("invoice","doc_type","3",$from,$to,"2");
												$record_label="Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAllByID_Multiple("invoice",array("doc_type"=>3,"date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("invoice",array("doc_type"=>3,"date"=>date('Y-m-d')));
												$record_label=""; 
											}
										}
										elseif($input_status==5)
										{
											
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												if(isset($_GET['from']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_coustomer=$obj_report_chain->ReportQuery_Datewise_Or_array("invoice",array("doc_type"=>3),$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
													$record = $obj_report_chain->ReportQuery_Datewise_Or_array("invoice",array("doc_type"=>3),$array_ch,"input_by",$_GET['from'],$_GET['to'],"2");
													$record_label="Report Generate Between ".$_GET['from']." - ".$_GET['to'];
												}
												else
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3,"date"=>date('Y-m-d')),$array_ch,"input_by","1");
													$record=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>3,"date"=>date('Y-m-d')),$array_ch,"input_by","2");
													$record_label="Report Generate Between ".date('Y-m-d');
												}
												//echo "Work";
											}
											else
											{
												//echo "Not Work";
												$sql_coustomer="";
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sqlinvoice = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"1");
												$record = $report->SelectAllDateCond_store2("invoice","doc_type","3","invoice_creator",$input_by,$from,$to,"2");
												$record_label="Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by,"date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("invoice", array("doc_type"=>3,"invoice_creator"=>$input_by,"date"=>date('Y-m-d')));
												$record_label=""; 
											}
										}
										?>
                            <h5><i class="font-home"></i> Sales Report <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        
<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Invoice-ID</th>
                                                            <th>Customer</th>
                                                            <th>Status</th>
                                                            <th>Tender</th>
                                                            <th>Date</th>
                                                            <th>Item</th>
                                                            <th>Total</th>
        
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
														$a=0; $a1=0; $a2=0;
                                                        $i = 1;
                                                        if (!empty($sqlinvoice))
                                                            foreach ($sqlinvoice as $invoice):
                                                                ?>
                                                                <?php
                                                                $sqlitem = $obj->SelectAllByID_Multiple("sales", array("sales_id" => $invoice->invoice_id));
                                                                $item_q = 0;
                                                                $total = 0;
                                                                if (!empty($sqlitem))
                                                                    foreach ($sqlitem as $item):
                                                                        $rr = $item->quantity * $item->single_cost;
                                                                        
								if($obj->exists_multiple("pos_tax",array("invoice_id"=>$invoice->invoice_id,"status"=>2))==0)
								{
									$taxchk=1;
									
								}
								else
								{
									$taxchk=0;
								}
                                                                        if ($taxchk == 0) {
                                                                            $tax = 0;
                                                                        } else {
                                                                            $tax = ($rr * $tax_per_product) / 100;
                                                                        }
        
                                                                        $tot = $rr + $tax;
                                                                        $total+=$tot;
                                                                        $item_q+=$item->quantity;
                                                                    endforeach;
                                                                if ($total != 0) {
																	$a+=1;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $i; ?></td>
                                                                        <td><a href="view_sales.php?invoice=<?php echo $invoice->invoice_id; ?>" class="label label-important"><i class="font-money"></i> <?php echo $invoice->invoice_id; ?></a></td>
                                                                        <td><?php echo $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname"); ?></td>
                                                                        <td><label class="label label-success"> <?php echo $obj->invoice_paid_status($invoice->status); ?> </label></td>
                                                                        <td><label class="label label-warning"> 
				<?php 
	$chkpayment_id=$obj->SelectAllByVal("invoice_payment","invoice_id",$invoice->invoice_id,"payment_type");
	echo $obj->SelectAllByVal("payment_method","id",$chkpayment_id,"meth_name"); ?> 
                                                                        </label></td>
        
                                                                        <td><label class="label label-primary"><i class="icon-calendar"></i> <?php echo $invoice->date; ?></label></td>
                                                                        <td><?php echo $item_q;
																		$a1+=$item_q;
																		 ?></td>
                                                                        <td>$<?php echo number_format($total, 2);
																		$a2+=$total;
																		 ?></td>                                            
                                                                        <td>
        
                                                                            
                                                                            <?php if($input_status==1) { ?>
                                                                            <a target="_blank" href="pos.php?action=pdf&invoice=<?php echo $invoice->invoice_id; ?>" class="hovertip" title="Print" onclick="javascript:return confirm('Are you absolutely sure to Print This Sales ?')"><i class="icon-print"></i></a>
                                                                            
                                                                                <a href="<?php echo $obj->filename(); ?>?del=<?php echo $invoice->id; ?>" class="hovertip" title="Delete" onclick="javascript:return confirm('Are you absolutely sure to delete This Sales ?')"><i class="icon-remove"></i></a>
                                                                            <?php } ?> 
                                                                        </td>
                                                                    </tr>
                                                                    <?php $i++;
                                                                } endforeach;
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
<div class="block well span4" style="margin-left:0; margin-top:20px;">
                                            <div class="navbar">
                                                <div class="navbar-inner">
                                                    <h5> Sales Short Report</h5>
                                                </div>
                                            </div>
                                            <div class="table-overflow">
                                <table class="table table-condensed">
                                    <tbody>
                                    	<tr>
                                            <td>1. Total Quantity = <strong> <?php echo $a; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>2. Total Quantity Sold  = <strong> <?php 
											
											echo $a1; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>4. Total Sales = <strong> $<?php echo number_format($a2,2); ?></strong></td>
                                        </tr>
                                        <?php
										?>
                                        
                                    </tbody>
                                </table>
                                            </div>
                                        </div>
                                        <!-- /table condensed -->


                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

							<?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>

                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
