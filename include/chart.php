<?php 
function makedate($str)
{
	$ddf=date('Y-m-d');
	$datenews=date_create($ddf);
	date_modify($datenews,"-".$str." days");
	return $datenews=date_format($datenews,"Y-m-d");
}

function randomColor($r) { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
	shuffle($possibilities);
	$color = "#";
	for($i=1;$i<=$r;$i++){
		$color .= $possibilities[rand(0,14)];
	}
	return $color;
} 
?>
<div class="row-fluid block">  

                                        <div class="semi-block" style="margin-top:-20px;">
                                            <div class="body">
                                            <!--bar chart start-->
                                            <div class="span6 well-white ">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Profit Report <?php  ?> </h3>
<canvas id="canvas" height="450" width="600"></canvas>

<script>
//var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

var barChartData = {
    labels : ["<?php echo date('Y-m-d'); ?>",
	<?php 
	for($b=1; $b<=15; $b++):
		if($b==15){ ?>"<?php echo makedate($b); ?>"<?php	}
		else{ ?>"<?php echo makedate($b); ?>",<?php }
	endfor; ?>
	],
    datasets : [
        {
            fillColor : "rgba(134,179,0,1)",
            strokeColor : "rgba(134,179,0,0)",
            highlightFill: "rgba(134,179,0,1)",
            highlightStroke: "rgba(134,179,0,0)",
            data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->profit;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=15; $a++):
			 	if($a==15)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->profit;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->profit;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
        }
    ]

}

</script><div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Bar chart end-->
                                                <!--Dought chart start-->
                                            <div class="span6 well-white ">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Employee Wise Sales </h3>
		<div id="canvas-holder" style="width:75%; margin-left:auto; margin-right:auto;">
			<canvas id="chart-areas" width="100%" height="100%"/>
		</div>


	<script>

		var doughnutData = [
				<?php  
				if($input_status==1){
				$sqlcashier=$obj->SelectAll("cashier_list");
				}else{
				$sqlcashier=$obj->SelectAllByID_Multiple("cashier_list",array("store_id"=>$input_by));	
				}
				$countccc=count($sqlcashier);
				$fp=1;
				if(!empty($sqlcashier))
				foreach($sqlcashier as $cashier):
				$sqlinvoiced = $obj->SelectAllByID_Multiple("sales_list",array("cashier_id"=>$cashier->id));
					
					if(!empty($sqlinvoiced))
					{
						$cp=0;
						foreach($sqlinvoiced as $ins):
							$cp+=$ins->profit;
						endforeach; 
					}
					else
					{
						$cp+=0;
					}
					
					if($countccc==$fp){
						?>
						{
							value:<?php echo $cp; ?>,
							color:"#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
							highlight: "#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
							label: "<?php echo $cashier->name; ?>"
						}
						<?php
					}
					else
					{
						?>
						{
							value:<?php echo $cp; ?>,
							color:"#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
							highlight: "#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
							label: "<?php echo $cashier->name; ?>"
						},
						<?php
					}
				$fp++;	
				endforeach;
				?>];

/*			window.onload = function()
			{
				
			};
*/


	</script>

<div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Dought chart end-->
                                                
                                                
                                                
                                                <div class="clearfix"></div>
                                                
                                                <!--Line chart start-->
                                            <div class="span6 well-white" style="margin-left:0px; margin-top:10px;">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Checkin &amp; Ticket Report <?php  ?> </h3>
<canvas id="canvas_line" height="450" width="600"></canvas>

<script>
		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
		var lineChartData = {
			labels : ["<?php echo date('Y-m-d'); ?>",<?php for($b=1; $b<=15; $b++):
							if($b==15){ ?>"<?php echo makedate($b); ?>"<?php	}
							else{ ?>"<?php echo makedate($b); ?>",<?php }
						endfor; ?>
						],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=1;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=15; $a++):
			 	if($a==15)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=1;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("ticket_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=1;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
				},
				{
					label: "My Second dataset",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=1;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=15; $a++):
			 	if($a==15)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=1;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("checkin_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=1;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
				}
			]

		}


</script><div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Line chart end-->
                                                
                                                  <!--Pie chart start-->
                                            <div class="span6 well-white" style="margin-top:10px;">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Cash, Credit, buyback Report <?php  ?> </h3>
															<div id="canvas-holder" style="margin-left:auto; margin-right:auto; width:75%;">
                                                                <canvas id="chart-area_pie" width="100%" height="100%"/>
                                                            </div>
                                                            <?php 
															include('class/chart.php');
															$charts =new chart();
															?>
		<script>
		var pieData = [
					<?php 
					for($g=0; $g<=66; $g++):
						if($input_status==1){
							$cash=$charts->buyback_check("3",makedate($g));
							$credit=$charts->buyback_check("4",makedate($g));
						}else{
							$cash=$charts->buyback_check_store($input_by,"3",makedate($g));
							$credit=$charts->buyback_check_store($input_by,"4",makedate($g));	
						}
						$total=$cash+$credit;
					if($g==66){
					?>
					{
						value:<?php echo $total; ?>,
						color:"#<?php echo dechex(rand(0x010000, 0xFFF0FF)); ?>",
						highlight: "#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
						label: "<?php echo makedate($g); ?> - Cash = <?php echo $cash; ?>, Credit Card = <?php echo $credit; ?>"
					}
					<?php  
					}
					else
					{
					?>
					{
						value:<?php echo $total; ?>,
						color:"#<?php echo dechex(rand(0x0F0000, 0xFFF1FF)); ?>",
						highlight: "#<?php echo dechex(rand(0x00A000, 0xFFF0FF)); ?>",
						label: "<?php echo makedate($g); ?> - Cash = <?php echo $cash; ?>, Credit Card = <?php echo $credit; ?>"
					},
					<?php  	
					}
					endfor;
					?>
					];
</script>
<div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Pie chart end-->
                                                
                                                
                                                  <!--Pie chart start-->
                                            <div class="span6 well-white" style="margin-top:10px; margin-left:0px;">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Payout Datewise Report <?php  ?> </h3>
															<div id="canvas-holder" style="margin-left:auto; margin-right:auto; width:75%;">
                                                                <canvas id="chart-area_pie2" width="100%" height="100%"/>
                                                            </div>
		<script>
		var pieData2 = [
					<?php 
					for($h=0; $h<=66; $h++):
						if($input_status==1){
							$cashplus=$charts->payout_check_plus(makedate($h));
							$cashmin=$charts->payout_check_min(makedate($h));
						}else{
							$cashplus=$charts->payout_check_plus_store_id($input_by,makedate($h));
							$cashmin=$charts->payout_check_min_store_id($input_by,makedate($h));	
						}
						$total=$cashplus+$cashmin;
					if($h==66){
					?>
					{
						value:<?php echo $total; ?>,
						color:"#<?php echo dechex(rand(0x010000, 0xFFF0FF)); ?>",
						highlight: "#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
						label: "<?php echo makedate($h); ?> : (+) = <?php echo $cashplus; ?>, (-) = <?php echo $cashmin; ?>"
					}
					<?php  
					}
					else
					{
					?>
					{
						value:<?php echo $total; ?>,
						color:"#<?php echo dechex(rand(0x010000, 0xFFF0FF)); ?>",
						highlight: "#<?php echo dechex(rand(0x000000, 0xFFFFFF)); ?>",
						label: "<?php echo makedate($h); ?> : (+) = <?php echo $cashplus; ?>, (-) = <?php echo $cashmin; ?>"
					},
					<?php  	
					}
					endfor;
					?>
					];
</script>
<div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Pie chart end-->
                                                <!--bar chart start-->
                                            <div class="span6 well-white" style="margin-top:10px;">
                                            
                                                    <div style="width:100%">
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Sales Report <div class="clearfix"></div><span style="font-size:18px;"><span style="background:rgba(134,179,0,1); width:20px; height:20px; display:inline-block;"></span> Sales Amount   <span style="background:rgba(151,187,205,0.5); width:20px; height:20px; display:inline-block;"></span> Sales Quantity  </span></h3>
<canvas id="canvas_sales" height="430" width="600"></canvas>

<script>
//var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

var barChartData_sales = {
    labels : ["<?php echo date('Y-m-d'); ?>",
	<?php 
	for($b=1; $b<=15; $b++):
		if($b==15){ ?>"<?php echo makedate($b); ?>"<?php	}
		else{ ?>"<?php echo makedate($b); ?>",<?php }
	endfor; ?>
	],
    datasets : [
        {
            fillColor : "rgba(134,179,0,1)",
            strokeColor : "rgba(134,179,0,0)",
            highlightFill: "rgba(134,179,0,1)",
            highlightStroke: "rgba(134,179,0,0)",
            data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->totalcost;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=15; $a++):
			 	if($a==15)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->totalcost;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->totalcost;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
        },
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->quantity;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=15; $a++):
			 	if($a==15)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->quantity;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("sales_list",array("input_by"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->quantity;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
    ]

}

</script><div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Bar chart end-->
                                                <div class="clearfix"></div>
                                                <!--bar chart start-->
                                            <div class="span12 well-white" style="margin-top:10px; margin-left:0;">
                                            
                                                    <div style="width:100%">
                                                    
                                                    <h3 style="padding-left:30px; border-bottom:3px #666 solid;">Store Closing Report </h3>
                                                    <div class="clearfix"></div>
                                                    <img style="position:absolute; right:0;" src="images/chart_store_close.png">
<canvas id="canvas_store_close" height="430" width="600"></canvas>

<script>
//var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

var barChartData_store_close = {
    labels : ["<?php echo date('Y-m-d'); ?>",
	<?php 
	for($b=1; $b<=5; $b++):
		if($b==5){ ?>"<?php echo makedate($b); ?>"<?php	}
		else{ ?>"<?php echo makedate($b); ?>",<?php }
	endfor; ?>
	],
    datasets : [
        {
            fillColor : "rgba(134,179,0,1)",
            strokeColor : "rgba(134,179,0,0)",
            highlightFill: "rgba(134,179,0,1)",
            highlightStroke: "rgba(134,179,0,0)",
            data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->total_collection_cash_credit_card;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->total_collection_cash_credit_card;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->total_collection_cash_credit_card;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
        },{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->total_collection_cash_credit_card;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->cash_collected_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->cash_collected_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			},{
				fillColor : "rgba(255,197,120,1)",
				strokeColor : "rgba(255,197,120,1)",
				highlightFill : "rgba(255,197,120,1)",
				highlightStroke : "rgba(255,197,120,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->credit_card_collected_plus;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->credit_card_collected_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->credit_card_collected_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(73,155,234,1)",
				strokeColor : "rgba(73,155,234,1)",
				highlightFill : "rgba(73,155,234,1)",
				highlightStroke : "rgba(73,155,234,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->opening_cash_plus;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->opening_cash_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->opening_cash_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(229,112,231,1)",
				strokeColor : "rgba(229,112,231,1)",
				highlightFill : "rgba(229,112,231,1)",
				highlightStroke : "rgba(229,112,231,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->opening_credit_card_plus;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->opening_credit_card_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->opening_credit_card_plus;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(169,3,41,1)",
				strokeColor : "rgba(169,3,41,1)",
				highlightFill : "rgba(169,3,41,1)",
				highlightStroke : "rgba(169,3,41,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->payout_plus_min;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->payout_plus_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->payout_plus_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(255,255,136,1)",
				strokeColor : "rgba(255,255,136,1)",
				highlightFill : "rgba(255,255,136,1)",
				highlightStroke : "rgba(255,255,136,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->buyback_min;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->buyback_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->buyback_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(69,72,77,1)",
				strokeColor : "rgba(69,72,77,1)",
				highlightFill : "rgba(69,72,77,1)",
				highlightStroke : "rgba(69,72,77,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->tax_min;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->tax_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->tax_min;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(180,227,145,1)",
				strokeColor : "rgba(180,227,145,1)",
				highlightFill : "rgba(180,227,145,1)",
				highlightStroke : "rgba(180,227,145,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->current_cash;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_cash;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_cash;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(255,26,0,1)",
				strokeColor : "rgba(255,26,0,1)",
				highlightFill : "rgba(255,26,0,1)",
				highlightStroke : "rgba(255,26,0,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->current_credit_card;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_credit_card;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_credit_card;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
			,{
				fillColor : "rgba(98,125,77,1)",
				strokeColor : "rgba(98,125,77,1)",
				highlightFill : "rgba(98,125,77,1)",
				highlightStroke : "rgba(98,125,77,1)",
				data : [<?php 
			if($input_status==1)
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>date('Y-m-d')));
			}
			else
			{
				$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>date('Y-m-d')));
			}
			if(!empty($sqlinvoice))
			{
				$pp=0;	
			foreach($sqlinvoice as $in):
				$pp+=$in->current_total;
			endforeach;
			echo $pp;
			}
			else
			{
				echo $pp=0;
			}
			 ?>,<?php 
			 for($a=1; $a<=5; $a++):
			 	if($a==5)
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_total;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
				}
				else
				{
					if($input_status==1)
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("date"=>makedate($a)));
					}
					else
					{
						$sqlinvoice = $obj->SelectAllByID_Multiple("close_store_detail",array("store_id"=>$input_by,"date"=>makedate($a)));
					}
					if(!empty($sqlinvoice))
					{
						$pp=0;	
					foreach($sqlinvoice as $in):
						$pp+=$in->current_total;
					endforeach;
					echo $pp;
					}
					else
					{
						echo $pp=0;
					}
					echo ",";	
				}
			 endfor;
			  ?>]
			}
    ]

}

</script><div class="clearfix"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                            	</div>
                                                <!--Bar chart end-->
                                                
                                                
                                                
                                                
                                                
                                                
<script>
window.onload = function()
{
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx).Bar(barChartData, { responsive : true });
	var ctxx = document.getElementById("chart-areas").getContext("2d");
	window.myDoughnut = new Chart(ctxx).Doughnut(doughnutData, {responsive : true});
	var ctx_line = document.getElementById("canvas_line").getContext("2d");
	window.myLine = new Chart(ctx_line).Line(lineChartData, { responsive: true });
	var ctx_pie = document.getElementById("chart-area_pie").getContext("2d");
	window.myPie = new Chart(ctx_pie).Pie(pieData, { responsive : true });
	var ctx_pie2 = document.getElementById("chart-area_pie2").getContext("2d");
	window.myPie = new Chart(ctx_pie2).Pie(pieData2, { responsive : true });
	var ctx_sales = document.getElementById("canvas_sales").getContext("2d");
    window.myBar = new Chart(ctx_sales).Bar(barChartData_sales, { responsive : true });
	var ctx_store_close = document.getElementById("canvas_store_close").getContext("2d");
    window.myBar = new Chart(ctx_store_close).Bar(barChartData_store_close, { responsive : true });
}
</script>                                                
                                            </div>
                                        </div>

                                        
                                </div>
                                <?php 
								
								?>
                                <div class="separator-doubled"></div> 