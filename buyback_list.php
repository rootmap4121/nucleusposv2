<?php 
include('class/auth.php'); 
if(isset($_GET['del']))
{
	$obj->deletesing("id",$_GET['del'],"buyback");	
}
if(@$_GET['export']=="excel") 
{
$record_label="BuyBack List Report"; 
header('Content-type: application/excel');
$filename ="BuyBack_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>BuyBack List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>BuyBack List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>BuyBack ID</th>
			<th>Customer</th>
			<th>Model</th>
			<th>Carrier</th>
			<th>IMEI</th>
			<th>Price</th>
			<th>Payment Method</th>
			<th>Date</th>
		</tr>
</thead>        
<tbody>";


		if($input_status==1)
		{
			$sqlticket=$obj->SelectAll("buyback");
		}
		elseif($input_status==5)
		{
			
			$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
			if(!empty($sqlchain_store_ids))
			{
				$array_ch = array();
				foreach($sqlchain_store_ids as $ch):
					array_push($array_ch,$ch->store_id);
				endforeach;
				
				include('class/report_chain_admin.php');	
					$obj_report_chain = new chain_report();
					$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("buyback",$array_ch,"input_by","1");
				
			}
			else
			{
				//echo "Not Work";
				$sqlticket="";
			}
		}
		else
		{
			$sqlticket=$obj->SelectAllByID("buyback",array("input_by"=>$input_by));
		}
		$i=1;
		$a1=0; $a2=0;
		if(!empty($sqlticket))
		foreach($sqlticket as $ticket): 
		$a1+=1;
		$a2+=$ticket->price;
		
			$data.="<tr>
				<td>".$i."</td>
				<td>".$ticket->buyback_id."</td>
				<td>".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"lastname")."</td>
				<td>".$ticket->model."</td>
				<td>".$ticket->carrier." Days</td>
				<td>".$ticket->imei."</td>
				<td>".$ticket->price."</td>
				<td>".$obj->SelectAllByVal("payment_method","id",$ticket->payment_method,"meth_name")."</td>
				<td>".$obj->duration($ticket->date,date('Y-m-d'))."</td>
			</tr>";
			$i++;
			endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>BuyBack ID</th>
			<th>Customer</th>
			<th>Model</th>
			<th>Carrier</th>
			<th>IMEI</th>
			<th>Price</th>
			<th>Payment Method</th>
			<th>Date</th>
		</tr></tfoot></table>";

$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	$record_label="Warrenty List Report"; 
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						BuyBack List Report
						</td>
					</tr>
				</table>
			
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> BuyBack List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>BuyBack ID</th>
			<th>Customer</th>
			<th>Model</th>
			<th>Carrier</th>
			<th>IMEI</th>
			<th>Price</th>
			<th>Payment Method</th>
			<th>Date</th>
		</tr>
</thead>        
<tbody>";

		if($input_status==1)
		{
			$sqlticket=$obj->SelectAll("buyback");
		}
		elseif($input_status==5)
		{
			
			$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
			if(!empty($sqlchain_store_ids))
			{
				$array_ch = array();
				foreach($sqlchain_store_ids as $ch):
					array_push($array_ch,$ch->store_id);
				endforeach;
				
				include('class/report_chain_admin.php');	
					$obj_report_chain = new chain_report();
					$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("buyback",$array_ch,"input_by","1");
				
			}
			else
			{
				//echo "Not Work";
				$sqlticket="";
			}
		}
		else
		{
			$sqlticket=$obj->SelectAllByID("buyback",array("input_by"=>$input_by));
		}
		$i=1;
		$a1=0; $a2=0;
		if(!empty($sqlticket))
		foreach($sqlticket as $ticket): 
		$a1+=1;
		$a2+=$ticket->price;
		
			$html.="<tr>
				<td>".$i."</td>
				<td>".$ticket->buyback_id."</td>
				<td>".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"lastname")."</td>
				<td>".$ticket->model."</td>
				<td>".$ticket->carrier." Days</td>
				<td>".$ticket->imei."</td>
				<td>".$ticket->price."</td>
				<td>".$obj->SelectAllByVal("payment_method","id",$ticket->payment_method,"meth_name")."</td>
				<td>".$obj->duration($ticket->date,date('Y-m-d'))."</td>
			</tr>";
			$i++;
			endforeach;
			
	$html.="</tbody><tfoot><tr>
			<th>#</th>
			<th>BuyBack ID</th>
			<th>Customer</th>
			<th>Model</th>
			<th>Carrier</th>
			<th>IMEI</th>
			<th>Price</th>
			<th>Payment Method</th>
			<th>Date</th>
		</tr></tfoot></table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">

            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->


            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i>
                            <span style="border-right:2px #333 solid; padding-right:10px;"> BuyBack Info </span>
                            <span><a data-toggle="modal" href="#myModal"> Generate BuyBack Report</a></span>
                            </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
                            
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">
                                
                                <!-- Dialog content -->
                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h5 id="myModalLabel">Generate BuyBack Report <span id="mss"></span></h5>
                                </div>
                                <div class="modal-body">

                                    <div class="row-fluid">
											<form class="form-horizontal" method="get" action="">
                                            <div class="control-group">
                                                <label class="control-label"><strong>Date Search:</strong></label>
                                                <div class="controls">
                                                    <ul class="dates-range">
                                                        <li><input type="text" id="fromDate" name="from" placeholder="From" /></li>
                                                        <li class="sep">-</li>
                                                        <li><input type="text" id="toDate" name="to" placeholder="To" /></li>
                                                        <li class="sep">&nbsp;</li>
                                                        <li><button class="btn btn-primary" type="submit">Search Report</button></li>
                                                    </ul>
                                                </div>
                                            </div>
											</form>
                                            
											
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <form class="form-horizontal" method="get" action="">
                                    <button class="btn btn-primary" name="all" type="submit">Show All BuyBack</button>
                                    </form>
                                </div>
                        </div>
                        <!-- /dialog content -->
                                
                                <!-- Content Start from here customized -->
                                
                                
                                <!-- Default datatable -->
                            <div class="table-overflow">
                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>BuyBack ID</th>
                                                            <th>Customer</th>
                                                            <th>Model</th>
                                                            <th>Carrier</th>
                                                            <th>IMEI</th>
                                                            <th>Price</th>
                                                            <th>Payment Method</th>
                                                            <th>Date</th>
                                                            <th>Process</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
														if($input_status==1)
														{
															if(isset($_GET['from']))
															{
																$sql_coustomer=$obj->SelectAll_ddate("buyback","date",$_GET['from'],$_GET['to']);
															}
															elseif(isset($_GET['all']))
															{
																$sql_coustomer=$obj->SelectAll("buyback");
															}
															else
															{
																$sql_coustomer=$obj->SelectAllByID("buyback",array("date"=>date('Y-m-d')));
															}
														}
														elseif($input_status==5)
														{
															
															$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
															if(!empty($sqlchain_store_ids))
															{
																$array_ch = array();
																foreach($sqlchain_store_ids as $ch):
																	array_push($array_ch,$ch->store_id);
																endforeach;
																
																if(isset($_GET['from']))
																{
																	include('class/report_chain_admin.php');	
																	$obj_report_chain = new chain_report();
																	$sql_coustomer=$obj_report_chain->ReportQuery_Datewise_Or("buyback",$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
																}
																elseif(isset($_GET['all']))
																{
																	include('class/report_chain_admin.php');	
																	$obj_report_chain = new chain_report();
																	$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple_Or("buyback",$array_ch,"input_by","1");
																}
																else
																{
																	include('class/report_chain_admin.php');	
																	$obj_report_chain = new chain_report();
																	$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple2_Or("buyback",array("date"=>date('Y-m-d')),$array_ch,"input_by","1");
																}
																//echo "Work";
															}
															else
															{
																//echo "Not Work";
																$sql_coustomer="";
															}
														}
														else
														{
															if(isset($_GET['from']))
															{
																include('class/report_customer.php');	
																$obj_report = new report();
																$sql_coustomer=$obj_report->ReportQuery_Datewise("buyback",array("input_by"=>$input_by),$_GET['from'],$_GET['to'],"1");
															}
															elseif(isset($_GET['all']))
															{
																$sql_coustomer=$obj->SelectAllByID("buyback",array("input_by"=>$input_by));
															}
															else
															{
																$sql_coustomer=$obj->SelectAllByID_Multiple("buyback",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
															}
														}
														
														
														
														
														
														/*if($input_status==1)
														{
															$sqlticket=$obj->SelectAll("buyback");
														}
														else
														{
															$sqlticket=$obj->SelectAllByID("buyback",array("input_by"=>$input_by));
														}*/
                                                        $i=1;
														$a1=0; $a2=0;
                                                        if(!empty($sql_coustomer))
                                                        foreach($sql_coustomer as $ticket): 
														$a1+=1;
														$a2+=$ticket->price;
														?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><a class="label label-success" href="view_buyback.php?buyback_id=<?php echo $ticket->buyback_id; ?>"><i class="icon-tags"></i> <?php echo $ticket->buyback_id; ?></a></td>
                                                                <td><i class="icon-user"></i> <?php echo $obj->SelectAllByVal("coustomer","id",$ticket->cid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"lastname"); ?></td>
                                                                <td><?php echo $ticket->model; ?></td>
                                                                <td><?php echo $ticket->carrier; ?></td>
                                                                <td><?php echo $ticket->imei; ?></td>
                                                                <td><?php echo $ticket->price; ?></td>
                                                                 <td><?php echo $obj->SelectAllByVal("payment_method","id",$ticket->payment_method,"meth_name"); ?></td>
                                                                <td><label class="label label-info"><i class="icon-calendar"></i> <?php echo $obj->duration($ticket->date,date('Y-m-d')); ?></label></td>
                                                                <td>
                                                                <?php if($input_status==1 || $input_status==2){ ?>
                                                                <?php 
																
																$chkproduct=$obj->exists_multiple("buyback_record",array("barcode"=>$ticket->buyback_id));
																if($chkproduct==0)
																{ 
																	?>
																	<a href="view_buyback.php?process=<?php echo $ticket->buyback_id; ?>" target="_blank" title="Parts | Sale" onclick="javascript:return confirm('Are you absolutely sure to perform This action?')"><i class="icon-refresh"></i></a>
																	<?php 
																}
																elseif($chkproduct==1)
																{ 
																	$psst=$obj->SelectAllByVal("buyback_record","barcode",$ticket->buyback_id,"status");
																	if($psst==1){ ?> <label class="label label-warning">Parts</label>  <?php }
																	elseif($psst==2){ ?> <label class="label label-danger">Sale</label> <?php }
																}
																} ?>
                                                                </td>
                                                                <td>
                                                                <?php if($input_status==1 || $input_status==2 || $input_status==5){ ?>
                                                                
                                                                <a href="view_buyback.php?print_invoice=<?php echo $ticket->buyback_id; ?>" target="_blank" title="Print" onclick="javascript:return confirm('Are you absolutely sure to Print This?')"><i class="icon-print"></i></a>
                                                                <a href="<?php echo $obj->filename(); ?>?del=<?php echo $ticket->id; ?>" title="Delete" onclick="javascript:return confirm('Are you absolutely sure to delete This?')"><i class="icon-trash"></i></a>
                                                                <?php }
																else
																{
																	?>
                                                                    <a href="view_buyback.php?print_invoice=<?php echo $ticket->buyback_id; ?>" target="_blank" title="Print" onclick="javascript:return confirm('Are you absolutely sure to Print This?')"><i class="icon-print"></i></a>
                                                                    <?php
																}
																 ?>
                                                                </td>
                
                                                            </tr>
                                                        <?php $i++; endforeach; ?>
                                                    </tbody>
                                                </table>
                            </div>
                        <!-- /default datatable -->
                                
                                
                                <!-- Content End from here customized -->
                                



                                <div class="separator-doubled"></div> 

							<a href="<?php echo $obj->filename(); ?>?export=excel"><img src="pos_image/file_excel.png"></a>
                            <a href="<?php echo $obj->filename(); ?>?export=pdf"><img src="pos_image/file_pdf.png"></a> 


                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
