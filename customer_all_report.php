<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
$cid=$_GET['cid'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i>Customer Report Info</h5>
                            <ul class="icons">
                                <li>
                                    <a data-toggle="modal" href="#myModal1" class="hovertip" title="Search Customer Report">
                                        <i class="icon-calendar"></i>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /page header -->
						
                        <!-- Dialog content -->
                            
                        <!-- /dialog content -->
                        
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
											
                                            <!-- General form elements -->
                                            <div class="row-fluid span6">
                                            	<div class="clear"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Business Name: </strong> 
                                                    
                                                     <span class="span8">  
                                                    <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"businessname"); ?> </span>
                                                    </label>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Name: </strong><span class="span8">
                                                     <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"firstname"); ?> </span>
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Email: </strong><span class="span8">
                                                     <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"email"); ?></span> 
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Invoice Email: </strong><span class="span8">
                                                     <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"invoice_email"); ?> </span> 
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Address: </strong><span class="span8">
                                                     <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"address1"); ?>  </span>
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Phone: </strong><span class="span8">
                                                     <?php echo $obj->SelectAllByVal("coustomer","id",$cid,"phone"); ?> </span> 
                                                     </label>
                                                </div>
                                            </div>
                                            <!-- /general form elements -->
                                            
                                            <!-- General form elements -->
                                            <div class="row-fluid span5">
                                            <div class="clear"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Sales : </strong> 
                                                    
                                                     <span class="span8">  
                                                    	<?php echo $report->count_invoice_report($cid,"invoice",3); ?> Report
                                                     </span>
                                                    </label>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Invoice : </strong>
                                                    <span class="span8">  
                                                    	<?php echo $report->count_invoice_report($cid,"invoice",3); ?> Report
                                                     </span>
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Estimate : </strong>
                                                    <span class="span8">  
                                                    	<?php echo $report->count_invoice_report($cid,"invoice",2); ?> Report
                                                     </span> 
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Ticket : </strong><span class="span8">
                                                     <?php echo $report->count_ticket_report($cid,"ticket"); ?> Report </span>
                                                     </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="span12">
                                                    <label class="span12"> <strong class="span4">Parts order : </strong><span class="span8">
                                                     <?php echo $report->count_parts_report($cid,"parts_order"); ?> Report </span> 
                                                     </label>
                                                </div>
                                            <!-- /general form elements -->

                                        	</div>
											


                                        </div>

                                <!-- Content End from here customized -->
                                <!-- Sales form elements -->
                                        <div class="row-fluid  span12 well" style="margin-top: 5px;">     
                                    			<div class="navbar">
                                                    <div class="navbar-inner">
                                                        <h5><i class="font-money"></i> Sales  </h5>
                                                    </div>
                                                </div>
                                    	<!-- Selects, dropdowns -->
                                    	<div class="span12" style="padding:0px; margin:0px;">
                                        	<div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Sales-ID</th>
                                                            <th>Product Name</th>
                                                            <th>Status</th>
                                                            
                                                            <th>Payment Method</th>
                                                            <th>Date</th>
                                                            <th>Item</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $sqlinvoice=$obj->SelectAllByID_Multiple("invoice",array("doc_type"=>3,"cid"=>$cid));
														$sales_total=0;
                                                        $i=1;
                                                        if(!empty($sqlinvoice))
                                                        foreach($sqlinvoice as $invoice): ?>
                                                        
                                                        <?php 
														$sqlitem=$obj->SelectAllByID_Multiple("sales",array("sales_id"=>$invoice->invoice_id));
														$countitem=count($sqlitem);
														$item_q=0;
														$total=0;
														if($countitem!=0)
														{
														foreach($sqlitem as $item):
														?>
                                                        
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><a href="view_invoice.php?invoice=<?php echo $invoice->invoice_id; ?>" class="label label-important"><i class="font-money"></i> <?php echo $invoice->invoice_id; ?></a></td>
                                                            <td><?php echo $obj->SelectAllByVal("product","id",$item->pid,"name"); ?></td>
                                                            <td><label class="label label-success"> <?php echo $obj->invoice_paid_status($invoice->status); ?> </label></td>
                                                            <td><label class="label label-warning"> <?php echo $obj->SelectAllByVal("payment_method","id",$item->payment_method,"meth_name"); ?> </label></td>
                                                            
                                                            <td><label class="label label-primary"><i class="icon-calendar"></i> <?php echo $item->date; ?></label></td>
                                                            <td>
                                                            <?php 
															echo $item->quantity;
															$ittt=$item->totalcost;
															?>
                                                            </td>
                                                            <td>$<?php echo $ittt; 
															$sales_total+=$ittt;
															?></td>                                            
                                                        </tr>
                                                        <?php
														$i++; 
														endforeach;
														 
														}
														endforeach; ?>
                                                    </tbody>
                                                    <tfoot>
                                                    	<tr>
                                                        	<td colspan="7" align="right"><strong>Total = </strong></td>
                                                            <td>$<?php echo number_format($sales_total,2); ?></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                    </div>
                                        <!-- /Sales form elements -->
                                
                                
                                
										 <!-- Invoice form elements -->
                                        <div class="row-fluid  span12 well" style="margin-top: 5px;">     
                                    			<div class="navbar">
                                                    <div class="navbar-inner">
                                                        <h5><i class="font-money"></i> Invoices  </h5>
                                                    </div>
                                                </div>
                                    	<!-- Selects, dropdowns -->
                                    	<div class="span12" style="padding:0px; margin:0px;">
                                        	<div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Invoice-ID</th>
                                                            <th>Customer</th>
                                                            <th>Status</th>
                                                            
                                                            <th>Took Payment</th>
                                                            <th>Date</th>
                                                            <th>Item</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $sqlinvoices=$obj->SelectAllByID_Multiple("invoice",array("doc_type"=>3,"cid"=>$cid));
                                                        $i=1;
                                                        if(!empty($sqlinvoices))
                                                        foreach($sqlinvoices as $invoice): ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><a href="view_invoice.php?invoice=<?php echo $invoice->invoice_id; ?>" class="label label-important"><i class="font-money"></i> <?php echo $invoice->invoice_id; ?></a></td>
                                                            <td><?php echo $obj->SelectAllByVal("coustomer","id",$invoice->cid,"businessname")."-".$obj->SelectAllByVal("coustomer","id",$invoice->cid,"firstname")."".$obj->SelectAllByVal("coustomer","id",$invoice->cid,"lastname"); ?></td>
                                                            <td><label class="label label-success"> <?php echo $obj->invoice_paid_status($invoice->status); ?> </label></td>
                                                            <td><label class="label label-warning"> <?php echo $obj->invoice_took_payment($invoice->status); ?> </label></td>
                                                            
                                                            <td><label class="label label-primary"><i class="icon-calendar"></i> <?php echo $invoice->date; ?></label></td>
                                                            <td>
                                                            <?php 
                                                            $sqlitem=$obj->SelectAllByID_Multiple("sales",array("sales_id"=>$invoice->invoice_id));
                                                            $item_q=0;
                                                            $total=0;
                                                            if(!empty($sqlitem))
                                                            foreach($sqlitem as $item):
                                                                $rr=$item->totalcost;
                                                                
                                                                $tot=$rr;
                                                                $total+=$tot;
                                                                $item_q+=$item->quantity;
                                                            endforeach;
                                                            
                                                            echo $item_q;
                                                            ?>
                                                            </td>
                                                            <td>$<?php echo $total; ?></td>                                            
                                                        </tr>
                                                        <?php $i++; endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                    </div>
                                        <!-- /invoice form elements -->
                                        
                                        
                                        
                                        
                                        <!-- Estimate form elements -->
                                        <div class="row-fluid  span12 well" style="margin-top: 5px;">     
                                    			<div class="navbar">
                                                    <div class="navbar-inner">
                                                        <h5><i class="font-money"></i> Estimate  </h5>
                                                    </div>
                                                </div>
                                    	<!-- Selects, dropdowns -->
                                    	<div class="span12" style="padding:0px; margin:0px;">
                                        	<div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Estimate-ID</th>
                                                            <th>Customer</th>
                                                            <th>Status</th>
                                                            
                                                            <th>Took Payment</th>
                                                            <th>Date</th>
                                                            <th>Item</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $sqlinvoice=$obj->SelectAllByID_Multiple("invoice",array("doc_type"=>2,"cid"=>$cid));
                                                        $i=1;
                                                        if(!empty($sqlinvoice))
                                                        foreach($sqlinvoice as $invoice): ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><a href="view_invoice.php?invoice=<?php echo $invoice->invoice_id; ?>" class="label label-important"><i class="font-money"></i> <?php echo $invoice->invoice_id; ?></a></td>
                                                            <td><?php echo $obj->SelectAllByVal("coustomer","id",$invoice->cid,"businessname")."-".$obj->SelectAllByVal("coustomer","id",$invoice->cid,"firstname")."".$obj->SelectAllByVal("coustomer","id",$invoice->cid,"lastname"); ?></td>
                                                            <td><label class="label label-success"> <?php echo $obj->invoice_paid_status($invoice->status); ?> </label></td>
                                                            <td><label class="label label-warning"> <?php echo $obj->invoice_took_payment($invoice->status); ?> </label></td>
                                                            
                                                            <td><label class="label label-primary"><i class="icon-calendar"></i> <?php echo $invoice->date; ?></label></td>
                                                            <td>
                                                            <?php 
                                                            $sqlitem=$obj->SelectAllByID_Multiple("invoice_detail",array("invoice_id"=>$invoice->invoice_id));
                                                            $item_q=0;
                                                            $total=0;
                                                            if(!empty($sqlitem))
                                                            foreach($sqlitem as $item):
                                                                $rr=$item->quantity*$item->single_cost;
                                                                if($item->tax!=0)
                                                                {
                                                                    $tax=0;
                                                                }
                                                                else
                                                                {
                                                                    $tax=($rr*$tax_per_product)/100;
                                                                }
                                                                
                                                                $tot=$rr+$tax;
                                                                $total+=$tot;
                                                                $item_q+=$item->quantity;
                                                            endforeach;
                                                            
                                                            echo $item_q;
                                                            ?>
                                                            </td>
                                                            <td>$<?php echo $total; ?></td>                                            
                                                        </tr>
                                                        <?php $i++; endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                    </div>
                                        <!-- /Estimate form elements -->
                                        
                                        
                                        
                                        
                                        <!-- Ticket form elements -->
                                        <div class="row-fluid  span12 well" style="margin-top: 5px;">     
                                    			<div class="navbar">
                                                    <div class="navbar-inner">
                                                        <h5><i class="font-money"></i> Ticket  </h5>
                                                    </div>
                                                </div>
                                    	<!-- Selects, dropdowns -->
                                    	<div class="span12" style="padding:0px; margin:0px;">
                                        	<div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Ticket ID</th>
                                                            <th>Customer</th>
                                                            <th>Subject</th>
                                                            <th>Created</th>
                                                            
                                                            <th>Status</th>
                                                            <th>Problem type</th>
                                                            <th>Last Updated</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sqlticket=$obj->SelectAllByID_Multiple("ticket",array("cid"=>$cid)); 
                                                        $i=1;
														
                                                        if(!empty($sqlticket))
                                                        foreach($sqlticket as $ticket): ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><a class="label label-success" href="view_tickets.php?ticket_id=<?php echo $ticket->ticket_id; ?>"><i class="icon-tags"></i> <?php echo $ticket->ticket_id; ?></a></td>
                                                                <td><i class="icon-user"></i> <?php echo $obj->SelectAllByVal("coustomer","id",$ticket->cid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->cid,"lastname"); ?></td>
                                                                <td><?php echo $ticket->title; ?></td>
                                                                <td><i class="icon-calendar"></i> <?php echo $ticket->date; ?></td>
                
                                                                <td><?php echo $obj->ticket_status($ticket->status); ?></td>
                                                                 <td><label class="label label-warning"><i class="icon-tint"></i> <?php echo $obj->SelectAllByVal("problem_type","id",$ticket->problem_type,"name"); ?></label></td>
                                                                <td><label class="label label-info"><i class="icon-calendar"></i> <?php echo $obj->duration($ticket->date,date('Y-m-d')); ?></label></td>
                
                                                            </tr>
                                                        <?php $i++; endforeach; ?>
                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                    </div>
                                        <!-- /Ticket form elements -->
                                        
                                        
                                        
                                        <!-- Parts Order form elements -->
                                        <div class="row-fluid  span12 well" style="margin-top: 5px;">     
                                    			<div class="navbar">
                                                    <div class="navbar-inner">
                                                        <h5><i class="font-money"></i> Parts Order  </h5>
                                                    </div>
                                                </div>
                                    	<!-- Selects, dropdowns -->
                                    	<div class="span12" style="padding:0px; margin:0px;">
                                        	<div class="table-overflow">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>ID</th>
                                                            <th>Entered </th>
                                                            <th>Ticket </th>
                                                            <th>Customer </th>
                                                            <th>Description</th>
                                                            <th>Price</th>                                            
                                                            <th>Store</th>
                                                            <th>Bought</th>
                                                            <th>Tracking</th>
                                                            <th>Arrived</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql_parts_order=$obj->SelectAll("parts_order");
                                                        $i=1;
                                                        if(!empty($sql_parts_order))
                                                        foreach($sql_parts_order as $row): 
														$cids=$obj->SelectAllByVal("ticket","ticket_id",$row->ticket_id,"cid");
														if($cid==$cids)
														{
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td> <?php echo $row->id; ?> </td>
                                                            <td> <?php echo $row->date; ?> </td>
                                                            
                                                            <td><a href="view_tickets.php?ticket_id=<?php echo $row->ticket_id; ?>"><?php echo $row->ticket_id; ?></a></td>
                                                            <td><a href="customer.php?edit=<?php echo $cids; ?>">
                                                            <?php echo $obj->SelectAllByVal("customer_list","id",$cids,"fullname"); ?>
                                                            </a></td>
                                                            <td><label class="label label-success"> <?php echo $row->description; ?>  </label></td>
                                                            <td> <?php echo $row->cost; ?> </td>
                                                            <td><?php echo $row->store; ?></td>
                                                            <td><?php echo $row->ordered; ?></td>
                                                            <td><a target="_blank" href="http://www.google.com/search?&amp;q=<?php echo $row->trackingnum; ?>"><?php echo $row->trackingnum; ?></a></td>
                                                            <td><?php echo $row->received; ?></td>
                                                        </tr>
                                                        <?php 
														}
                                                        $i++;
                                                        endforeach; 
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /selects, dropdowns -->
                                    </div>
                                        <!-- /Parts Order form elements -->



                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
