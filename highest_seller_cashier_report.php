<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";

	if($input_status==5)
	{
		include('class/report_chain_admin.php');	
		$obj_report_chain = new chain_report();
		$array_ch = array();
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		foreach($sqlchain_store_ids as $ch):
			array_push($array_ch,$ch->store_id);
		endforeach;	
	}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i> Highest Seller Report </h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        

                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                    <th>#</th>
                                                    <th>Store</th>
                                                    <th>Cashier Name</th>
													<th>Sold Quantity</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
														if($input_status==1)
														{
														$sqlhighestsales=$report->SelectAllOrder("highest_sales","sales","desc");
														}
														elseif($input_status==5)
														{
															
														$sqlhighestsales=$obj_report_chain->SelectAllByID_Multiple_Or("highest_sales",$array_ch,"input_by","1");
														}
														else
														{
														$sqlhighestsales=$report->SelectAllOrderCond1("highest_sales","sales","desc","input_by",$input_by);	
														}
														$i=1;
														if(!empty($sqlhighestsales))
														foreach($sqlhighestsales as $highestsales):
														?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $highestsales->input_by; ?></td>
                                                            <td><?php echo $highestsales->cashier; ?></td>
                                                            <td><strong><?php echo $highestsales->sales; ?></strong> Item Sold</td>
                                                        </tr>
                                                        <?php 
														$i++;
														endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        

                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
