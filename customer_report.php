<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php 
				echo $obj->ShowMsg();
				
				if($input_status==1)
				{
						$sqlticket=$obj->SelectAll($table);
						$record = $obj->totalrows($table);
						$record_label=""; 
						
						if(isset($_GET['from']))
						{
							$status_data=1;
							$from=$_GET['from'];
							$to=$_GET['to'];	
						}
						else
						{
							$status_data=0;
						}
						
				}
				elseif($input_status==5)
				{
					
					$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
					if(!empty($sqlchain_store_ids))
					{
						$array_ch = array();
						foreach($sqlchain_store_ids as $ch):
							array_push($array_ch,$ch->store_id);
						endforeach;
						
						include('class/report_chain_admin.php');	
						$obj_report_chain = new chain_report();
						$sqlticket=$obj_report_chain->SelectAllByID_Multiple2_Or($table,array("status"=>1),$array_ch,"input_by","1");
						$record =$obj_report_chain->SelectAllByID_Multiple2_Or($table,array("status"=>1),$array_ch,"input_by","2");;
						$record_label="";
						//echo "Work";
						if(isset($_GET['from']))
						{
							$status_data=1;
							$from=$_GET['from'];
							$to=$_GET['to'];	
						}
						else
						{
							$status_data=0;
						}
					}
					else
					{
						//echo "Not Work";
						$sqlticket="";
						$record =0;
						$record_label="";
					}
				}
				else
				{
					$sqlticket = $obj->SelectAllByID_Multiple($table,array("input_by"=>$input_by));
					$record = $obj->exists_multiple($table,array("input_by"=>$input_by));
					$record_label=""; 
					if(isset($_GET['from']))
					{
						$status_data=1;
						$from=$_GET['from'];
						$to=$_GET['to'];	
					}
					else
					{
						$status_data=0;
					}
				}
				?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-user"></i>Customer Report Info  <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                            <ul class="icons">
                                <li>
                                    <a data-toggle="modal" href="#myModal1" class="hovertip" title="Search Customer Report">
                                        <i class="icon-calendar"></i>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /page header -->
						
                        <div class="body">
<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        
                                       
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name/Business</th>
                                                            <th>Sales</th>
                                                            <th>Invoice</th>
                                                            <th>Estimate</th>
                                                            <th>Ticket</th>
                                                            <th>Parts Order</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    
										<?php 
										$i=1;
										if(!empty($sqlticket))
										foreach($sqlticket as $customer): 
										if($i<3)
										{
											
											if($status_data==1)
											{
											?>
											<tr>
												<td><?php echo $i; ?></td>
										<td><?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?></td>
                                        
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",3,$from,$to); ?> Sales</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",3,$from,$to); ?> Invoice</td>
										<td><?php echo $report->count_invoice_report_date($customer->id,"invoice",2,$from,$to); ?> Estimate</td>
										<td><?php echo $report->count_ticket_report_date($customer->id,"ticket",$from,$to); ?> Ticket</td>
										<td><?php echo $report->count_parts_report_date($customer->id,"parts_order",$from,$to); ?> Parts</td>
										<td><a href="customer_all_report.php?cid=<?php echo $customer->id; ?>" class="btn btn-success"><i class="icon-list"></i> View All Report</a></td>
											</tr>
											<?php 
											}
											else
											{
										?>
											<tr>
												<td><?php echo $i; ?></td>
										<td><?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?></td>
                                        
										<td><?php echo $report->count_invoice_report_single_date_sales($customer->id,"invoice",3,date('Y-m-d')); ?> Sales</td>
										<td><?php echo $report->count_invoice_report_single_date($customer->id,"invoice",3,date('Y-m-d')); ?> Invoice</td>
										<td><?php echo $report->count_invoice_report_single_date($customer->id,"invoice",2,date('Y-m-d')); ?> Estimate</td>
										<td><?php echo $report->count_ticket_report_single_date($customer->id,"ticket",date('Y-m-d')); ?> Ticket</td>
										<td><?php echo $report->count_parts_report_single_date($customer->id,"parts_order",date('Y-m-d')); ?> Parts</td>
										<td><a href="customer_all_report.php?cid=<?php echo $customer->id; ?>" class="btn btn-success"><i class="icon-list"></i> View All Report</a></td>
											</tr>
											<?php 
											}
										}
											$i++; endforeach;	?>
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
