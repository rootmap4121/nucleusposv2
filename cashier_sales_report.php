<?php 
include('class/auth.php');
include('class/report_customer.php');
$report=new report();  
$table="coustomer";

	if($input_status==5)
	{
		include('class/report_chain_admin.php');	
		$obj_report_chain = new chain_report();
		$array_ch = array();
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		foreach($sqlchain_store_ids as $ch):
			array_push($array_ch,$ch->store_id);
		endforeach;	
	}

if(@$_GET['export']=="excel") 
{
			if($input_status==1)
			{
				if(isset($_GET['from']))
				{
					$from=$_GET['from'];
					$to=$_GET['to'];
					$cashier_id=$_GET['cashier_id'];
					$sqlinvoice = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"1");
					$record = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"2");
					$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
				}
				else
				{
					$sqlinvoice = $obj->SelectAll("sales_list");
					$record = $obj->totalrows("sales_list");
					$record_label="Total Record Found ( ".$record." )"; 
				}
			}
			else
			{
				if(isset($_GET['from']))
				{
					$from=$_GET['from'];
					$to=$_GET['to'];
					$cashier_id=$_GET['cashier_id'];
$sqlinvoice = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"1");
$record = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"2");
					$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
				}
				else
				{
					$sqlinvoice = $obj->SelectAllByID("sales_list",array("input_by"=>$input_by));
					$record = $obj->exists_multiple("sales_list",array("input_by"=>$input_by));
					$record_label="Total Record Found ( ".$record." )"; 
				}
			}
header('Content-type: application/excel');
$filename ="Cashier_Sales_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Cashier Sales List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Cashier Sales List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Sales-ID</th>
			<th>Cashier</th>
			<th>Product Name</th>
			<th>Quantity</th>
			<th>Retail</th>
			<th>Our Cost</th>
			<th>Total Retail</th>  
			<th>Our Total Cost</th>  
			<th>Profit</th>      
			<th>Date</th>
		</tr>
</thead>        
<tbody>";

			$i=1;
			$aa=0; $bb=0;  $cc=0;  $dd=0;
			if(!empty($sqlinvoice))
			foreach($sqlinvoice as $invoice):
			$dd+=$invoice->quantity;
			$aa+=$invoice->totalcost;
			$cc+=$invoice->profit;
			$bb+=$invoice->our_totalcost;
			$data.="<tr>
				<td>".$i."</td>
				<td>".$invoice->sales_id."</td>
				<td>".$invoice->cashier."</td>
				<td>".$invoice->product."</td>
				<td>".$invoice->quantity."</td>
				<td>".$invoice->single_cost."</td>
				<td>".$invoice->our_cost."</td>
				<td>".$invoice->totalcost."</td>
				<td>".$invoice->our_totalcost."</td>
				<td>".$invoice->profit."</td>
				<td>".$invoice->date."</td>
			</tr>";
			
			$i++; endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Sales-ID</th>
			<th>Cashier</th>
			<th>Product Name</th>
			<th>Quantity</th>
			<th>Retail</th>
			<th>Our Cost</th>
			<th>Total Retail</th>  
			<th>Our Total Cost</th>  
			<th>Profit</th>      
			<th>Date</th>
		</tr></tfoot></table>";
		
		
		
		
		$data.="<table border='0' width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong>".$dd."</strong></td>
						</tr>
						<tr>
							<td>2. Our Total Cost = <strong> $".number_format($bb,2)."</strong></td>
						</tr>
						<tr>
							<td>3. Total Retail Cost = <strong> $".number_format($aa,2)."</strong></td>
						</tr>
						<tr>
							<td>4. Total Profit = <strong> $".number_format($cc,2)."</strong></td>
						</tr>
					</tbody>
				</table>";
		
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
			if($input_status==1)
			{
				if(isset($_GET['from']))
				{
					$from=$_GET['from'];
					$to=$_GET['to'];
					$cashier_id=$_GET['cashier_id'];
					$sqlinvoice = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"1");
					$record = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"2");
					$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
				}
				else
				{
					$sqlinvoice = $obj->SelectAll("sales_list");
					$record = $obj->totalrows("sales_list");
					$record_label="Total Record Found ( ".$record." )"; 
				}
			}
			else
			{
				if(isset($_GET['from']))
				{
					$from=$_GET['from'];
					$to=$_GET['to'];
					$cashier_id=$_GET['cashier_id'];
$sqlinvoice = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"1");
$record = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"2");
					$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
				}
				else
				{
					$sqlinvoice = $obj->SelectAllByID("sales_list",array("input_by"=>$input_by));
					$record = $obj->exists_multiple("sales_list",array("input_by"=>$input_by));
					$record_label="Total Record Found ( ".$record." )"; 
				}
			}
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Cashier Sales List Report
						</td>
					</tr>
				</table>
				
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Cashier Sales List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Sales-ID</th>
			<th>Cashier</th>
			<th>Product Name</th>
			<th>Quantity</th>
			<th>Retail</th>
			<th>Our Cost</th>
			<th>Total Retail</th>  
			<th>Our Total Cost</th>  
			<th>Profit</th>      
			<th>Date</th>
		</tr>
</thead>        
<tbody>";
	
	
			$i=1;
			$aa=0; $bb=0;  $cc=0;  $dd=0;
			if(!empty($sqlinvoice))
			foreach($sqlinvoice as $invoice):
			$dd+=$invoice->quantity;
			$aa+=$invoice->totalcost;
			$cc+=$invoice->profit;
			$bb+=$invoice->our_totalcost;
	
	$html.="<tr>
				<td>".$i."</td>
				<td>".$invoice->sales_id."</td>
				<td>".$invoice->cashier."</td>
				<td>".$invoice->product."</td>
				<td>".$invoice->quantity."</td>
				<td>".$invoice->single_cost."</td>
				<td>".$invoice->our_cost."</td>
				<td>".$invoice->totalcost."</td>
				<td>".$invoice->our_totalcost."</td>
				<td>".$invoice->profit."</td>
				<td>".$invoice->date."</td>
			</tr>";
			
			$i++; endforeach;
			
	$html.="</tbody><tfoot><tr>
			<th>#</th>
			<th>Sales-ID</th>
			<th>Cashier</th>
			<th>Product Name</th>
			<th>Quantity</th>
			<th>Retail</th>
			<th>Our Cost</th>
			<th>Total Retail</th>  
			<th>Our Total Cost</th>  
			<th>Profit</th>      
			<th>Date</th>
		</tr></tfoot></table>";
		
		$html.="<table border='0'  width='250' style='width:250px;'>
					<tbody>
						<tr>
							<td>1. Total Quantity = <strong>".$dd."</strong></td>
						</tr>
						<tr>
							<td>2. Our Total Cost = <strong> $".number_format($bb,2)."</strong></td>
						</tr>
						<tr>
							<td>3. Total Retail Cost = <strong> $".number_format($aa,2)."</strong></td>
						</tr>
						<tr>
							<td>4. Total Profit = <strong> $".number_format($cc,2)."</strong></td>
						</tr>
					</tbody>
				</table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                        				<?php 
										echo $obj->ShowMsg();
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$cashier_id=$_GET['cashier_id'];
												$sqlinvoice = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"1");
												$record = $report->SelectAllDateCond("sales_list","cashier_id",$cashier_id,$from,$to,"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAll("sales_list");
												$record = $obj->totalrows("sales_list");
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										elseif($input_status==5)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$cashier_id=$_GET['cashier_id'];
												
												$sqlinvoice =$obj_report_chain->ReportQuery_Datewise_Or_array("sales_list",array("cashier_id"=>$cashier_id),$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
												$record =$obj_report_chain->ReportQuery_Datewise_Or_array("sales_list",array("cashier_id"=>$cashier_id),$array_ch,"input_by",$_GET['from'],$_GET['to'],"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice =$obj_report_chain->SelectAllByID_Multiple_Or("sales_list",$array_ch,"input_by","1");
												$record =$obj_report_chain->SelectAllByID_Multiple_Or("sales_list",$array_ch,"input_by","2");
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$cashier_id=$_GET['cashier_id'];
$sqlinvoice = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"1");
$record = $report->SelectAllDateCond_store2("sales_list","cashier_id",$cashier_id,"input_by",$input_by,$from,$to,"2");
												$record_label="Total record Found ( ".$record." ). | Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlinvoice = $obj->SelectAllByID("sales_list",array("input_by"=>$input_by));
												$record = $obj->exists_multiple("sales_list",array("input_by"=>$input_by));
												$record_label="Total Record Found ( ".$record." )"; 
											}
										}
										?>
                            <h5><i class="font-money"></i> Cashier Sales Report | <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                        
<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label">Cashier :</label>
                        <div class="controls">
                            <select name="cashier_id">
                            	<?php 
								$sqlcashier=$obj->SelectAll("cashier_list");
								if(!empty($sqlcashier))
								foreach($sqlcashier as $cashier):
								?>
                                <option value="<?php echo $cashier->id; ?>"><?php echo $cashier->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
                                        
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Sales-ID</th>
                                                            <th>Cashier</th>
                                                            <th>Product Name</th>
                                                            <th>Quantity</th>
                                                            <th>Retail</th>
                                                            <th>Our Cost</th>
                                                            <th>Total Retail</th>  
                                                            <th>Our Total Cost</th>  
                                                            <th>Profit</th>      
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $i=1;
														$aa=0; $bb=0;  $cc=0;  $dd=0;
                                                        if(!empty($sqlinvoice))
                                                        foreach($sqlinvoice as $invoice):
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><a href="view_sales.php?invoice=<?php echo $invoice->sales_id; ?>"><?php echo $invoice->sales_id; ?></a></td>
                                                            <td><?php echo $invoice->cashier; ?></td>
                                                            <td><?php echo $invoice->product; ?></td>
                                                            <td><label class="label label-success"><?php 
															$dd+=$invoice->quantity;
															echo $invoice->quantity; ?></label></td>
                                                            <td><label class="label label-success"> $<?php echo $invoice->single_cost; ?> </label></td>
                                                            <td><label class="label label-warning"> $<?php echo $invoice->our_cost; ?> </label></td>

                                                            <td><label class="label label-success"> $<?php 
															$aa+=$invoice->totalcost;
															echo $invoice->totalcost; ?></label></td>
                                                            <td><label class="label label-warning"> $<?php 
															$bb+=$invoice->our_totalcost;
															echo $invoice->our_totalcost; ?></label></td>
                                                            <td><label class="label label-danger"> $<?php 
															$cc+=$invoice->profit;
															echo $invoice->profit; ?></label></td>
                                                            <td><?php echo $invoice->date; ?></td>
                                                        </tr>
                                                        <?php 
														$i++; 
														endforeach; 
														?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <!-- Table condensed -->
                                        <div class="block well span4" style="margin-left:0;">
                                            <div class="navbar">
                                                <div class="navbar-inner">
                                                    <h5> Profit Report</h5>
                                                </div>
                                            </div>
                                            <div class="table-overflow">
                                <table class="table table-condensed">
                                    <tbody>
                                    	<tr>
                                            <td>1. Total Quantity = <strong> <?php echo $dd; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>2. Our Total Cost = <strong> $<?php echo number_format($bb,2); ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>3. Total Retail Cost = <strong> $<?php echo number_format($aa,2); ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>4. Total Profit = <strong> $<?php echo number_format($cc,2); ?></strong></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                            </div>
                                        </div>
                                        <!-- /table condensed -->


                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

							<?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
								$cashier_id=$_GET['cashier_id'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>&amp;cashier_id=<?php echo $_GET['cashier_id']; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>&amp;cashier_id=<?php echo $_GET['cashier_id']; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>

                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
