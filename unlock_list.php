<?php 
include('class/auth.php'); 
if(isset($_GET['del']))
{
	$obj->deletesing("id",$_GET['del'],"unlock_request");	
}

function checkin_paid($st) {
    if ($st == 0) {
        return "Not Paid";
    } else {
        return "Paid";
    }
}

if(@$_GET['export']=="excel") 
{


$record_label="Unlock List Report"; 
header('Content-type: application/excel');
$filename ="Unlock_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Unlock List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Unlock List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Sevice </th>
			<th>Created</th>
			<th>Status</th>
			<th>Send To POS</th>
			<th>Paid</th>
		</tr>
</thead>        
<tbody>";


			if($input_status==1)
			{
				$sqlticket=$obj->SelectAll("unlock_request"); 
			}
			elseif($input_status==5)
			{
				
				$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
				if(!empty($sqlchain_store_ids))
				{
					$array_ch = array();
					foreach($sqlchain_store_ids as $ch):
						array_push($array_ch,$ch->store_id);
					endforeach;
					
					include('class/report_chain_admin.php');	
						$obj_report_chain = new chain_report();
						$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("unlock_request",$array_ch,"uid","1");
					
					//echo "Work";
				}
				else
				{
					//echo "Not Work";
					$sqlticket="";
				}
			}
			else
			{
				$sqlticket=$obj->SelectAllByID("unlock_request",array("uid"=>$input_by)); 	
			}
			$i=1;
			if(!empty($sqlticket))
			foreach($sqlticket as $ticket):
			
			$data.="<tr>
				<td>".$i."</td>
				<td>".$ticket->unlock_id."</td>
				<td>".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name")."</td>
				<td>".$ticket->date."</td>
				<td>".$obj->ticket_status($ticket->status)."</td>";
			
			$product_name=$ticket->service_id.":".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name");
			$chkx=$obj->exists_multiple("product", array("name" =>$product_name));
			if($chkx!=0){ 
			$pid=$obj->SelectAllByVal("product","name",$product_name,"id");
			
			$price=$ticket->retail_cost;
			
			}
			else{ $pid=0; $price=0; }
			$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
			$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
			$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
			if($curcheck==0)
			{	
			 	$data.="<td>".$price." Send To POS</td>";
			}
			else
			{
				$data.="<td>".$price." Paid</td>";	
			}
			
			$data.="<td>".checkin_paid($curcheck)."</td></tr>";
			$i++;
			endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Sevice </th>
			<th>Created</th>
			<th>Status</th>
			<th>Send To POS</th>
			<th>Paid</th>
		</tr></tfoot></table>";

$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	$record_label="Unlock List Report"; 
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Unlock List Report
						</td>
					</tr>
				</table>
				

				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Unlock List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Sevice </th>
			<th>Created</th>
			<th>Status</th>
			<th>Send To POS</th>
			<th>Paid</th>
		</tr>
</thead>        
<tbody>";

			if($input_status==1)
			{
				$sqlticket=$obj->SelectAll("unlock_request"); 
			}
			elseif($input_status==5)
			{
				
				$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
				if(!empty($sqlchain_store_ids))
				{
					$array_ch = array();
					foreach($sqlchain_store_ids as $ch):
						array_push($array_ch,$ch->store_id);
					endforeach;
					
					include('class/report_chain_admin.php');	
						$obj_report_chain = new chain_report();
						$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("unlock_request",$array_ch,"uid","1");
					
					//echo "Work";
				}
				else
				{
					//echo "Not Work";
					$sqlticket="";
				}
			}
			else
			{
				$sqlticket=$obj->SelectAllByID("unlock_request",array("uid"=>$input_by)); 	
			}
			$i=1;
			if(!empty($sqlticket))
			foreach($sqlticket as $ticket):
			
			$html.="<tr>
				<td>".$i."</td>
				<td>".$ticket->unlock_id."</td>
				<td>".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name")."</td>
				<td>".$ticket->date."</td>
				<td>".$obj->ticket_status($ticket->status)."</td>";
			
			$product_name=$ticket->service_id.":".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name");
			$chkx=$obj->exists_multiple("product", array("name" =>$product_name));
			if($chkx!=0){ 
			$pid=$obj->SelectAllByVal("product","name",$product_name,"id");
			
			$price=$ticket->retail_cost;
			
			}
			else{ $pid=0; $price=0; }
			$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
			$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
			$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
			if($curcheck==0)
			{	
			 	$html.="<td>".$price." Send To POS</td>";
			}
			else
			{
				$html.="<td>".$price." Paid</td>";	
			}
			
			$html.="<td>".checkin_paid($curcheck)."</td></tr>";
			$i++;
			endforeach;
			
	$html.="</tbody><tfoot><tr>
			<th>#</th>
			<th>Unlock ID</th>
			<th>Sevice </th>
			<th>Created</th>
			<th>Status</th>
			<th>Send To POS</th>
			<th>Paid</th>
		</tr></tfoot></table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">

            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->


            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i>
                            <span style="border-right:2px #333 solid; padding-right:10px;">Unlock Request Info</span>
                            <span><a data-toggle="modal" href="#myModal"> Generate Unlock Request Report</a></span>
                            </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
                            
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">
                                
                                <!-- Dialog content -->
                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h5 id="myModalLabel">Generate Unlock Request Report <span id="mss"></span></h5>
                                </div>
                                <div class="modal-body">

                                    <div class="row-fluid">
											<form class="form-horizontal" method="get" action="">
                                            <div class="control-group">
                                                <label class="control-label"><strong>Date Search:</strong></label>
                                                <div class="controls">
                                                    <ul class="dates-range">
                                                        <li><input type="text" id="fromDate" name="from" placeholder="From" /></li>
                                                        <li class="sep">-</li>
                                                        <li><input type="text" id="toDate" name="to" placeholder="To" /></li>
                                                        <li class="sep">&nbsp;</li>
                                                        <li><button class="btn btn-primary" type="submit">Search Report</button></li>
                                                    </ul>
                                                </div>
                                            </div>
											</form>
                                            
											
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <form class="form-horizontal" method="get" action="">
                                    <button class="btn btn-primary" name="all" type="submit">Show All Unlock Request</button>
                                    </form>
                                </div>
                        </div>
                        <!-- /dialog content -->
                                
                                <!-- Content Start from here customized -->
                                
                                
                                <!-- Default datatable -->
                            <div class="table-overflow">
                                <table class="table table-striped" id="data-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Unlock ID</th>
                                            <th>Sevice </th>
                                            <th>Created</th>
                                            <th>Status</th>
                                            <th width="150">Send To POS</th>
                                            <th>Paid</th>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$sql_coustomer=$obj->SelectAll_ddate("unlock_request","date",$_GET['from'],$_GET['to']);
											}
											elseif(isset($_GET['all']))
											{
												$sql_coustomer=$obj->SelectAll("unlock_request");
											}
											else
											{
												$sql_coustomer=$obj->SelectAllByID("unlock_request",array("date"=>date('Y-m-d')));
											}
										}
										elseif($input_status==5)
										{
											
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												if(isset($_GET['from']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_coustomer=$obj_report_chain->ReportQuery_Datewise_Or("unlock_request",$array_ch,"uid",$_GET['from'],$_GET['to'],"1");
												}
												elseif(isset($_GET['all']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple_Or("unlock_request",$array_ch,"uid","1");
												}
												else
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple2_Or("unlock_request",array("date"=>date('Y-m-d')),$array_ch,"uid","1");
												}
												//echo "Work";
											}
											else
											{
												//echo "Not Work";
												$sql_coustomer="";
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												include('class/report_customer.php');	
												$obj_report = new report();
												$sql_coustomer=$obj_report->ReportQuery_Datewise("unlock_request",array("uid"=>$input_by),$_GET['from'],$_GET['to'],"1");
											}
											elseif(isset($_GET['all']))
											{
												$sql_coustomer=$obj->SelectAllByID("unlock_request",array("uid"=>$input_by));
											}
											else
											{
												$sql_coustomer=$obj->SelectAllByID_Multiple("unlock_request",array("uid"=>$input_by,"date"=>date('Y-m-d')));
											}
										}
										
										
										/*if($input_status==1)
										{
											$sqlticket=$obj->SelectAll("unlock_request"); 
										}
										else
										{
											$sqlticket=$obj->SelectAllByID("unlock_request",array("uid"=>$input_by)); 	
										}*/
										$i=1;
										if(!empty($sql_coustomer))
										foreach($sql_coustomer as $ticket): ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><a class="label label-success" href="view_unlock.php?unlock_id=<?php echo $ticket->unlock_id; ?>"><i class="icon-tags"></i> <?php echo $ticket->unlock_id; ?></a></td>
                                                <!--<td><i class="icon-user"></i> <?php //echo $obj->SelectAllByVal("coustomer","id",$ticket->uid,"firstname")." ".$obj->SelectAllByVal("coustomer","id",$ticket->uid,"lastname"); ?></td>-->
                                                <td><?php echo $obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name"); ?></td>
                                                <td><i class="icon-calendar"></i> <?php echo $ticket->date; ?></td>

                                                <td><?php echo $obj->ticket_status($ticket->status); ?></td>
                                                
                                                <?php
												$product_name=$ticket->service_id.":".$obj->SelectAllByVal("unlock_service","id",$ticket->service_id,"name");
								$chkx=$obj->exists_multiple("product", array("name" =>$product_name));
									if($chkx!=0){ 
										$pid=$obj->SelectAllByVal("product","name",$product_name,"id");
										
										$price=$ticket->retail_cost;
										
									 }
									else{ $pid=0; $price=0; }
									$chkcheckin=$obj->exists_multiple("invoice",array("doc_type"=>3,"unlock_id"=>$ticket->unlock_id));
				$getsales_id=$obj->SelectAllByVal("invoice","unlock_id",$ticket->unlock_id,"invoice_id");
				$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
												?>
                                                <td>
                                                <?php if($curcheck==0){ ?>
                                                <a href="pos.php?newsales=1&amp;pid=<?php echo $pid; ?>&amp;price=<?php echo $price; ?>&AMP;cid=<?php echo $ticket->cid; ?>&amp;unlock_id=<?php echo $ticket->unlock_id; ?>" class="btn btn-success"><i class="font-money"></i> <?php echo number_format($price,2); ?> To POS</a>
                                                <?php }else{ ?>
                                                <span href="pos.php?newsales=1&amp;pid=<?php echo $pid; ?>&amp;price=<?php echo $price; ?>&AMP;cid=<?php echo $ticket->cid; ?>&amp;unlock_id=<?php echo $ticket->unlock_id; ?>" class="label label-success"><i class="font-money"></i> <?php echo number_format($price,2); ?> Paid</span>
                                                <?php } ?>
                                                </td>
                                            <td>
											<?php 
				
				echo checkin_paid($curcheck);	
											?>
                                            </td>
                                            <td>
                                                <?php if($input_status==1 || $input_status==2 || $input_status==2){ ?>
                                                <a href="<?php echo $obj->filename(); ?>?del=<?php echo $ticket->id; ?>" class="btn btn-danger hovertip" title="Delete"  onclick="javascript:return confirm('Are you absolutely sure to delete This Ticket ?')"><i class="icon-remove"></i></a>
                                                 <a href="view_unlock.php?print_invoice=<?php echo $ticket->unlock_id; ?>" class="btn btn-info hovertip" title="Print"  onclick="javascript:return confirm('Are you absolutely sure to Print This Requested Invoice ?')"><i class="icon-print"></i></a>
                                                
                                                
                                            <?php } ?> 
                                            
                                            </td>

                                            </tr>
                                        <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <!-- /default datatable -->
                                
                                
                                <!-- Content End from here customized -->
                                



                                <div class="separator-doubled"></div> 

<a href="<?php echo $obj->filename(); ?>?export=excel"><img src="pos_image/file_excel.png"></a>
                            <a href="<?php echo $obj->filename(); ?>?export=pdf"><img src="pos_image/file_pdf.png"></a> 


                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
