<?php
include('class/auth.php');
if (isset($_GET['del'])) {
    $obj->deletesing("id", $_GET['del'], "invoice");
}

if (isset($_GET['action'])) {

    $cart = $_GET['invoice'];
    $cid = $obj->SelectAllByVal("invoice", "invoice_id", $cart, "cid");
    $creator = $obj->SelectAllByVal("invoice", "invoice_id", $cart, "invoice_creator");
	$salrep_id = $obj->SelectAllByVal("invoice", "invoice_id", $cart, "access_id");
    $pt = $obj->SelectAllByVal("invoice", "invoice_id", $cart, "payment_type");
    $ckid = $obj->SelectAllByVal("invoice", "invoice_id", $cart, "checkin_id");
    $tax_statuss = $obj->SelectAllByVal("pos_tax", "invoice_id", $cart, "status");
    if($tax_statuss==0){ $taxs=0; }else{ $taxs=$tax_per_product; }
    include("pdf/MPDF57/mpdf.php");
    $html.="<table id='sample-table-2' class='table table-hover' border='1'><tbody>";

	$report_cpmpany_name=$obj->SelectAllByVal("setting_report","store_id",$input_by,"name");
	$report_cpmpany_address=$obj->SelectAllByVal("setting_report","store_id",$input_by,"address");
	$report_cpmpany_phone=$obj->SelectAllByVal("setting_report","store_id",$input_by,"phone");
	$report_cpmpany_email=$obj->SelectAllByVal("setting_report","store_id",$input_by,"email");
	$report_cpmpany_fotter=$obj->SelectAllByVal("setting_report","store_id",$input_by,"fotter");

	function limit_words($string, $word_limit){
		$words = explode(" ",$string);
		return implode(" ",array_splice($words,0,$word_limit));
	}
	
	$addressfline=limit_words($report_cpmpany_address,3);
	$lengthaddress=strlen($addressfline);
	$lastaddress=substr($report_cpmpany_address,$lengthaddress,30000);


    $html .="<tr>
			<td style='height:40px; background:rgba(0,51,153,1);'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:30px;'>".$report_cpmpany_name."</td><td width='13%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:30px;'><span style='float:left; text-align:left;'>Estimate Detail</span></td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:960px; height:40px; font-size:12px; border:0px;'>
					<tr>
						<td width='69%'>
						".$addressfline."<br>
						".$lastaddress."
						</td>
						<td width='31%'>
						DIRECT ALL INQUIRIES TO:<br />
						".$report_cpmpany_name."<br />
						".$report_cpmpany_phone."<br />
						".$report_cpmpany_email."<br />
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:30px;' valign='top'>
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Estimate To : </td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:960px; height:40px; border:0px;'>
					<tr>
						<td width='69%'>
						Name : " . $obj->SelectAllByVal("coustomer", "id", $cid, "firstname") . "<br />
						Phone : " . $obj->SelectAllByVal("coustomer", "id", $cid, "phone") . "<br />
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:960px; height:40px; border:0px;'>
					<tr>
						<td width='69%'>
						Phone Repair Center <br />
						We Repair | We Buy | We Sell <br />
						</td>
						<td width='31%'>
						Estimate DATE  : " . $obj->SelectAllByVal("invoice", "invoice_id", $cart, "invoice_date") . "<br />
						Estimate NO. : " . $cart . "<br />
						Estimate REP : " . $obj->SelectAllByVal("store","id",$salrep_id,"name") . "<br />
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='79%'>
						Estimate Tax Rate:  " . $taxs . "%
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  
		  <tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
    $html.="<thead><tr>
						<td>S/L</td>
						<td>Product</td>
						<td>Description</td>
						
						<td>Quantity</td>
						<td>Unit Cost</td>
						<td>Tax</td>
						<td>Extended</td>
					</tr></thead>";
    $sqlsaleslist = $obj->SelectAllByID("invoice_detail", array("invoice_id" => $cart));
    $sss = 1;
    $subtotal = 0;
    $tax = 0;
    if (!empty($sqlsaleslist))
        foreach($sqlsaleslist as $saleslist):
            $caltax = ($saleslist->single_cost * $tax_per_product) / 100;
            $procost = $saleslist->quantity * $saleslist->single_cost;
            $subtotal+=$procost;
            $tax_status =$saleslist->tax;
            if ($tax_status== 0 || $tax_status =="") {
                $tax+=0;
                $taxst = "No";
                $taxstn = "1";
                $extended = $procost;
            } else {
                $tax+=$caltax * $saleslist->quantity;
                $taxst = "Yes";
                $taxstn = "0";
                $extended = $procost + $caltax;
            }
            $html.="<thead><tr>
						<td>" . $sss . "</td>
						<td>" . $obj->SelectAllByVal("product", "id", $saleslist->pid, "name") . "</td>
						<td>" . $obj->SelectAllByVal("product", "id", $saleslist->pid, "description") . "</td>
						
						<td>" . $saleslist->quantity . "</td>
						<td><button type='button' class='btn'>$" . $saleslist->single_cost . "</button></td>
						<td>$" . $tax . "</td>
						<td>
							<button type='button' class='btn'>$" . $extended . "</button>
						</td>
					</tr></thead>";

            $sss++;
        endforeach;
    if ($pt == 0) {
        $pp = "Not Paid";
    } else {
        $pp = $obj->SelectAllByVal("payment_method", "id", $pt, "meth_name");
    }
    $total = $subtotal + $tax;
    if ($paid != 0) {
        $dd = number_format($total, 2);
    } else {
        $dd = "$0.00";
    }

    if ($paid != 0) {
        $ff = "$0.00";
    } else {

        $ff = number_format($total, 2);
    }

    $paid = 0;
    $sqlpp = $obj->SelectAllByID_Multiple("invoice_payment", array("invoice_id" => $cart));
    if (!empty($sqlpp)) {
        foreach ($sqlpp as $pps):
            $paid+=$pps->amount;
        endforeach;
    }
    else {
        $paid+=0;
    }

    $due = $total - $paid;
    $html.="</table></td></tr>";
    if ($ckid != 0) {

        $html.="<tr><td><table style='width:960px;'>
					<thead>
						<tr>
							<td width='350' valign='top'>";
		  if($obj->exists_multiple("checkin_request_ticket",array("checkin_id"=>$ckid))==0)
		  {
		  		$html.="<table style='width:300px;border:1px; margin-left:-4px; font-size:12px; background:#ccc;'>
					<thead>
						<tr>
							<th>IMEI of Device being repair : </th>
							<th>".$obj->SelectAllByVal("ticket","ticket_id",$ckid,"imei")."</th>
						</tr>
						<tr>
							<th>Carrier :  </th>
							<th>".$obj->SelectAllByVal("ticket","ticket_id",$ckid,"carrier")."</th>
						</tr>
						<tr>
							<th>Color :  </th>
							<th>".$obj->SelectAllByVal("ticket","ticket_id",$ckid,"type_color")."</th>
						</tr>
						<tr>
							<th>Problem :  </th>
							<th>".$obj->SelectAllByVal("problem_type","id",$obj->SelectAllByVal("ticket","ticket_id",$ckid,"problem_type"),"name")."</th>
						</tr>
					</thead>
				</table>";
		  }
		  else
		  {
			  $html.="<table style='width:300px;border:1px; margin-left:-4px; font-size:12px; background:#ccc;'>
					<thead>
						<tr>
							<th>IMEI of Device being repair: </th>
							<th>" . $obj->SelectAllByVal("checkin_request_ticket", "checkin_id", $ckid, "imei") . "</th>
						</tr>
						<tr>
							<th>Carrier:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list", "checkin_id", $ckid, "network") . "</th>
						</tr>
						<tr>
							<th>Color:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list", "checkin_id", $ckid, "color") . "</th>
						</tr>
						<tr>
							<th>Problem:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list", "checkin_id", $ckid, "problem") . "</th>
						</tr>
					</thead>
				</table>";
		  }
				$html.="</td>
				<td>
					<table style='width:250px;border:1px; font-size:12px; background:#ccc;'>
						<thead>
							<tr>
								<th>Payment Type: </th>
								<th>" . $pp . "</th>
							</tr>
							<tr>
								<th>Sub - Total: </th>
								<th>" . number_format($subtotal, 2) . "</th>
							</tr>
							<tr>
								<th>Tax: </th>
								<th>" . number_format($tax, 2) . "</th>
							</tr>
							<tr>
								<th>Total: </th>
								<th>" . number_format($total, 2) . "</th>
							</tr>
							<tr>
								<th>Payments: </th>
								<th>" . $paid . "</th>
							</tr>
							<tr>
								<th>Balance Due: </th>
								<th>" . $due . "</th>
							</tr>
						</thead>
					</table>
				</td>
				</tr>
				</thead>
				</table>
		  </td>
		  </tr>
		  <tr>
		  <td>
				
		  </td>
		  </tr>";
    } else {

        $html.="<tr><td><table style='width:250px;border:1px; font-size:12px; background:#ccc;'><thead>

							<tr>
								<th>Sub - Total: </th>
								<th>" . number_format($subtotal, 2) . "</th>
							</tr>
							<tr>
								<th>Tax: </th>
								<th>" . number_format($tax, 2) . "</th>
							</tr>
							<tr>
								<th>Total: </th>
								<th>" . number_format($total, 2) . "</th>
							</tr>
							<tr>
								<th>Payments: </th>
								<th>" . $paid . "</th>
							</tr>
							<tr>
								<th>Balance Due: </th>
								<th>" . $due . "</th>
							</tr>
						</thead></table></td></tr>";
    }

    if ($obj->exists_multiple("invoice_payment", array("invoice_id" => $cart)) != 0) {
        $html.="<tr><td>
				<br />
				<h3> Transaction Detail </h3>
				<table style='width:100%;border:1px; font-size:12px; background:#ccc;'>";
        $html.="<thead><tr>
						<td> S/L </td>
						<td>Payment Method</td>
						<td>Amount</td>
						<td>Date</td>
					</tr></thead>";
        $sqlsaleslist = $obj->SelectAllByID("invoice_payment", array("invoice_id" => $cart));
        $sss = 1;
        if (!empty($sqlsaleslist))
            foreach ($sqlsaleslist as $saleslist):
                $html.="<thead><tr>
						<td>" . $sss . "</td>
						<td>" . $obj->SelectAllByVal("payment_method", "id", $saleslist->payment_type, "meth_name") . "</td>
						<td>$" . $saleslist->amount . "</td>
						<td>" . $saleslist->date . "</td></tr></thead>";

                $sss++;
            endforeach;
        $html.="</table></td></tr>";
    }

    $html.="<tr>
			<td align='center' style='font-size:8px;'>".$report_cpmpany_fotter."</td>
		  </tr>
		  <tr>
			<td align='center'>Thank You For Your Business</td>
		  </tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}


if(@$_GET['export']=="excel") 
{


$record_label="Estimate List Report"; 
header('Content-type: application/excel');
$filename ="Estimate_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Estimate List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Estimate List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Estimate-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Took Payment</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr>
</thead>        
<tbody>";


		if($input_status==1)
		{
$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type" =>2));
		}
		elseif($input_status==5)
		{
			
			$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
			if(!empty($sqlchain_store_ids))
			{
				$array_ch = array();
				foreach($sqlchain_store_ids as $ch):
					array_push($array_ch,$ch->store_id);
				endforeach;
				
					include('class/report_chain_admin.php');	
					$obj_report_chain = new chain_report();
					$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple_Or_array("invoice",array("doc_type" =>2),$array_ch,"input_by","1");
				
			}
			else
			{
				//echo "Not Work";
				$sqlinvoice="";
			}
		}
		else
		{
$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type" =>2,"input_by"=>$input_by));													
		}
		$i = 1;
		if (!empty($sqlinvoice))
			foreach ($sqlinvoice as $invoice):
			$sqlitem = $obj->SelectAllByID_Multiple("invoice_detail", array("invoice_id" => $invoice->invoice_id));
                                                        $item_q = 0;
                                                        $total = 0;
                                                        if (!empty($sqlitem))
                                                            foreach ($sqlitem as $item):
                                                                $rr = $item->quantity * $item->single_cost;
                                                                if ($item->tax != 0) {
                                                                    $tax = 0;
                                                                } else {
                                                                    $tax = ($rr * $tax_per_product) / 100;
                                                                }

                                                                $tot = $rr + $tax;
                                                                $total+=$tot;
                                                                $item_q+=$item->quantity;
                                                            endforeach;
                                                        if ($total != 0) {
			$data.="<tr>
				<td>".$i."</td>
				<td>".$invoice->invoice_id."</td>
				<td>".$obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname")."</td>
				<td>".$obj->invoice_paid_status($invoice->status)."</td>
				<td>".$obj->invoice_took_payment($invoice->status)."</td>
				<td>".$invoice->date."</td>
				<td>".$item_q."</td>
				<td>".number_format($total, 2)."</td>
			</tr>";
			$i++;
			}
			endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Estimate-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Took Payment</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr></tfoot></table>";

$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	$record_label="Parts List Report"; 
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Estimate List Report
						</td>
					</tr>
				</table>
				
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Estimate List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Estimate-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Took Payment</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr>
</thead>        
<tbody>";

		if($input_status==1)
		{
$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type" =>2));
		}
		elseif($input_status==5)
		{
			
			$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
			if(!empty($sqlchain_store_ids))
			{
				$array_ch = array();
				foreach($sqlchain_store_ids as $ch):
					array_push($array_ch,$ch->store_id);
				endforeach;
				
					include('class/report_chain_admin.php');	
					$obj_report_chain = new chain_report();
					$sqlinvoice=$obj_report_chain->SelectAllByID_Multiple_Or_array("invoice",array("doc_type" =>2),$array_ch,"input_by","1");
				
			}
			else
			{
				//echo "Not Work";
				$sqlinvoice="";
			}
		}
		else
		{
$sqlinvoice = $obj->SelectAllByID_Multiple("invoice", array("doc_type" =>2,"input_by"=>$input_by));													
		}
		$i = 1;
		if (!empty($sqlinvoice))
			foreach ($sqlinvoice as $invoice):
			$sqlitem = $obj->SelectAllByID_Multiple("invoice_detail", array("invoice_id" => $invoice->invoice_id));
                                                        $item_q = 0;
                                                        $total = 0;
                                                        if (!empty($sqlitem))
                                                            foreach ($sqlitem as $item):
                                                                $rr = $item->quantity * $item->single_cost;
                                                                if ($item->tax != 0) {
                                                                    $tax = 0;
                                                                } else {
                                                                    $tax = ($rr * $tax_per_product) / 100;
                                                                }

                                                                $tot = $rr + $tax;
                                                                $total+=$tot;
                                                                $item_q+=$item->quantity;
                                                            endforeach;
                                                        if ($total != 0) {
			$html.="<tr>
				<td>".$i."</td>
				<td>".$invoice->invoice_id."</td>
				<td>".$obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname")."</td>
				<td>".$obj->invoice_paid_status($invoice->status)."</td>
				<td>".$obj->invoice_took_payment($invoice->status)."</td>
				<td>".$invoice->date."</td>
				<td>".$item_q."</td>
				<td>".number_format($total, 2)."</td>
			</tr>";
			$i++;
			}
			endforeach;
			
	$html.="</tbody><tfoot><tr>
			<th>#</th>
			<th>Estimate-ID</th>
			<th>Customer</th>
			<th>Status</th>
			<th>Took Payment</th>
			<th>Date</th>
			<th>Item</th>
			<th>Total</th>
		</tr></tfoot></table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">

            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->


            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i> <span style="border-right:2px #333 solid; padding-right:10px;">Estimate Info</span>
                            <span><a data-toggle="modal" href="#myModal"> Generate Estimate Report</a></span>
                            </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- Dialog content -->
                        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h5 id="myModalLabel">Generate Estimate Report <span id="mss"></span></h5>
                                </div>
                                <div class="modal-body">

                                    <div class="row-fluid">
											<form class="form-horizontal" method="get" action="">
                                            <div class="control-group">
                                                <label class="control-label"><strong>Date Search:</strong></label>
                                                <div class="controls">
                                                    <ul class="dates-range">
                                                        <li><input type="text" id="fromDate" name="from" placeholder="From" /></li>
                                                        <li class="sep">-</li>
                                                        <li><input type="text" id="toDate" name="to" placeholder="To" /></li>
                                                        <li class="sep">&nbsp;</li>
                                                        <li><button class="btn btn-primary" type="submit">Search Report</button></li>
                                                    </ul>
                                                </div>
                                            </div>
											</form>
                                            
											
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <form class="form-horizontal" method="get" action="">
                                    <button class="btn btn-primary" name="all" type="submit">Show All Estimate</button>
                                    </form>
                                </div>
                        </div>
                        <!-- /dialog content -->
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

<!--                                <div class="separator-doubled"></div> -->



                                <!-- Content Start from here customized -->


                                <!-- Default datatable -->
                                <div class="block">
                                    <div class="table-overflow">
                                        <table class="table table-striped" id="data-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Estimate-ID</th>
                                                    <th>Customer</th>
                                                    <th>Date</th>
                                                    <th>Item</th>
                                                    
                                                    <th>Sub Total</th>
                                                    
                                                    <th>Tax</th>
                                                    
                                                    <th>Total</th>

                                                    <th width="40">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
												
												if($input_status==1)
												{
													if(isset($_GET['from']))
													{
														include('class/report_customer.php');	
														$obj_report = new report();
														$sql_coustomer=$obj_report->ReportQuery_Datewise("invoice",array("doc_type" =>2),$_GET['from'],$_GET['to'],"1");
													}
													elseif(isset($_GET['all']))
													{
														$sql_coustomer=$obj->SelectAllByID_Multiple("invoice",array("doc_type" =>2));
													}
													else
													{
														$sql_coustomer=$obj->SelectAllByID_Multiple("invoice",array("doc_type" =>2,"date"=>date('Y-m-d')));
													}
												}
												elseif($input_status==5)
												{
													
													$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
													if(!empty($sqlchain_store_ids))
													{
														$array_ch = array();
														foreach($sqlchain_store_ids as $ch):
															array_push($array_ch,$ch->store_id);
														endforeach;
														
														if(isset($_GET['from']))
														{
															include('class/report_chain_admin.php');	
															$obj_report_chain = new chain_report();
															$sql_coustomer=$obj_report_chain->ReportQuery_Datewise_Or_array("invoice",array("doc_type"=>2),$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
														}
														elseif(isset($_GET['all']))
														{
															include('class/report_chain_admin.php');	
															$obj_report_chain = new chain_report();
															$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple_Or_array("invoice",array("doc_type"=>2),$array_ch,"input_by","1");
														}
														else
														{
															include('class/report_chain_admin.php');	
															$obj_report_chain = new chain_report();
															$sql_coustomer=$obj_report_chain->SelectAllByID_Multiple2_Or("invoice",array("doc_type"=>2,"date"=>date('Y-m-d')),$array_ch,"input_by","1");
														}
														//echo "Work";
													}
													else
													{
														//echo "Not Work";
														$sql_coustomer="";
													}
												}
												else
												{
													if(isset($_GET['from']))
													{
														include('class/report_customer.php');	
														$obj_report = new report();
														$sql_coustomer=$obj_report->ReportQuery_Datewise("invoice",array("doc_type" =>2,"input_by"=>$input_by),$_GET['from'],$_GET['to'],"1");
													}
													elseif(isset($_GET['all']))
													{
														$sql_coustomer=$obj->SelectAllByID_Multiple("invoice",array("doc_type" =>2,"input_by"=>$input_by));
													}
													else
													{
														$sql_coustomer=$obj->SelectAllByID_Multiple("invoice",array("doc_type" =>2,"input_by"=>$input_by,"date"=>date('Y-m-d')));
													}
												}
                                                
                                                $i = 1;
                                                if (!empty($sql_coustomer))
                                                    foreach ($sql_coustomer as $invoice):
                                                        ?>
                                                        <?php
                                                        $sqlitem = $obj->SelectAllByID_Multiple("invoice_detail", array("invoice_id" =>$invoice->invoice_id));
                                                        $item_q = 0;
                                                        $total = 0;
														$taxtt=0;
														$item_tt=0;
                                                        if (!empty($sqlitem))
                                                            foreach ($sqlitem as $item):
                                                                $rr = $item->quantity * $item->single_cost;
                                                                if ($item->tax==0 || $item->tax=="") {
                                                                    $tax = 0;
                                                                } else {
                                                                    $tax = ($rr * $tax_per_product) / 100;
                                                                }
																$taxtt+=$tax;
																$item_tt+=$rr;
                                                                $tot = $rr + $tax;
                                                                $total+=$tot;
                                                                $item_q+=$item->quantity;
                                                            endforeach;
															
                                                        if ($total != 0) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><a href="view_estimate.php?estimate=<?php echo $invoice->invoice_id; ?>" class="label label-important"><i class="font-money"></i> <?php echo $invoice->invoice_id; ?></a></td>
                                                                <td><?php echo $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "businessname") . "-" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "firstname") . "" . $obj->SelectAllByVal("coustomer", "id", $invoice->cid, "lastname"); ?></td>
                                                                
                                                                <td><label class="label label-primary"><i class="icon-calendar"></i> <?php echo $invoice->date; ?></label></td>
                                                                <td><?php echo $item_q; ?></td>
                                                                <td>$<?php echo number_format($item_tt,2); ?></td>
                                                                <td>$<?php echo number_format($taxtt,2); ?></td>
                                                                <td>$<?php echo number_format($total, 2); ?></td>                                            
                                                                <td>
																<?php if($input_status==1 || $input_status==2 || $input_status==5) { ?>
                                                                    <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf&invoice=<?php echo $invoice->invoice_id; ?>" class="hovertip" title="Print" onclick="javascript:return confirm('Are you absolutely sure to Print This Sales ?')"><i class="icon-print"></i></a>
                                                                    
                                                                        <a href="<?php echo $obj->filename(); ?>?del=<?php echo $invoice->id; ?>" class="hovertip" title="Delete" onclick="javascript:return confirm('Are you absolutely sure to delete This Sales ?')"><i class="icon-trash"></i></a>
                                                                    <?php }else{ ?>
                                                                    <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf&invoice=<?php echo $invoice->invoice_id; ?>" class="hovertip" title="Print" onclick="javascript:return confirm('Are you absolutely sure to Print This Sales ?')"><i class="icon-print"></i></a>
                                                                    <?php } ?>  
                                                                </td>
                                                            </tr>
                                                            <?php $i++;
                                                        } endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /default datatable -->


                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

								<a href="<?php echo $obj->filename(); ?>?export=excel"><img src="pos_image/file_excel.png"></a>
                            <a href="<?php echo $obj->filename(); ?>?export=pdf"><img src="pos_image/file_pdf.png"></a> 


                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');  ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
