<?php
include('class/auth.php');
$table="parts_order";
if(isset($_POST['create']))
{
	extract($_POST);
	if(!empty($quantity) && !empty($cost) && !empty($retail_customer))
	{
		if($obj->insert($table,array("ticket_id"=>$ticket_id, "description"=>$description, "part_url"=>$parts_url, "quantity"=>$quantity, 
		"cost"=>$cost, "retail_customer"=>$retail_customer,"store"=>$input_by, "texable"=>$taxable, "shipping"=>$shipping, "trackingnum"=>$trackingnum, 
		"notes"=>$notes, "ordered"=>$ordered, "received"=>$received,"input_by"=>$input_by,"access_id"=>$access_id, "date"=>date('Y-m-d'), "status"=>1))==1)
		{
			$obj->Success("Part Order Successfully Placed","parts_list.php");
		}
		else
		{
			$obj->Error("Something is wrong, Try again.", $obj->filename());
		}
	}
	else
	{
		$obj->Error("Failed, Fill up required field", $obj->filename());
	}
}
elseif(isset($_POST['update']))
{
	extract($_POST);
	if(!empty($quantity) && !empty($cost) && !empty($retail_customer) && !empty($taxable) && !empty($ordered) && !empty($received))
	{
		if($obj->update($table,array("id"=>$edit,"ticket_id"=>$ticket_id, "description"=>$description, "part_url"=>$parts_url, "quantity"=>$quantity, 
		"cost"=>$cost, "retail_customer"=>$retail_customer, "texable"=>$taxable, "shipping"=>$shipping, "trackingnum"=>$trackingnum, 
		"notes"=>$notes, "ordered"=>$ordered, "received"=>$received,"store"=>$input_by,"input_by"=>$input_by,"access_id"=>$access_id,"status"=>1))==1)
		{
			$obj->Success("Successfully Updated", $obj->filename()."?edit=".$edit);
		}
		else
		{
			$obj->Error("Something is wrong, Try again.", $obj->filename()."?edit=".$edit);
		}
	}
	else
	{
		$obj->Error("Failed, Fill up required field", $obj->filename()."?edit=".$edit);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
<?php echo $obj->bodyhead(); ?>
	<script>
	function Ticket_Cost(tid) {
	  if (tid=="") { document.getElementById('cost').innerHTML=""; return; }
	  if (window.XMLHttpRequest) { xmlhttp=new XMLHttpRequest(); } else { xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); }
	  xmlhttp.onreadystatechange=function()
	  { 
		if (xmlhttp.readyState==4 && xmlhttp.status==200) 
		{ 
			//load retail cost
			  xmlhttps=new XMLHttpRequest();
			  xmlhttps.onreadystatechange=function()
			  { 
				if (xmlhttps.readyState==4 && xmlhttps.status==200) 
				{ 
					document.getElementById('retail_cost').value=xmlhttps.responseText; 
				}
			  }
			  st=2;
			  xmlhttps.open("GET","ajax/ticket_cost.php?tid="+tid+"&st="+st,true);
			  xmlhttps.send();
			//load retail cost
			
			//load retail cost
			  xmlhttps3=new XMLHttpRequest();
			  xmlhttps3.onreadystatechange=function()
			  { 
				if (xmlhttps3.readyState==4 && xmlhttps3.status==200) 
				{ 
					document.getElementById('ps').value=xmlhttps3.responseText; 
				}
			  }
			  st=3;
			  xmlhttps3.open("GET","ajax/ticket_cost.php?tid="+tid+"&st="+st,true);
			  xmlhttps3.send();
			//load retail cost
			document.getElementById('cost').value=xmlhttp.responseText; 
		}
	  }
	  st=1;
	  xmlhttp.open("GET","ajax/ticket_cost.php?tid="+tid+"&st="+st,true);
	  xmlhttp.send();
	}

	</script>
    </head>

    <body>
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><?php if(@$_GET['edit']){ ?><i class="icon-edit"></i> Update Parts Order <?php }else{ ?> <i class="font-plus-sign"></i> Add Parts Order<?php } ?> </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
                            <!-- /middle navigation standard -->

                            <!-- Content container -->




                                <!-- Content Start from here customized -->
                                
								<?php if(@$_GET['edit']){ ?>
                                <form class="form-horizontal" method="post" name="invoice" action="">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">     
                                           <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px;">
                                                <div class="control-group">
                                                <input type="hidden" name="edit" value="<?php echo $_GET['edit']; ?>">
                                                    <label class="control-label">* Ticket :</label>
                                                    <div class="controls">
                                                        <select name="ticket_id" data-placeholder="Please Select..." class="select-search" tabindex="2" style="width:100%;">
                                                            <option value=""></option> 
                                                            <?php
															 if($input_status==1)
															 {
                                                             	$data=$obj->SelectAll("ticket");
															 }
															 else
															 {
																$data=$obj->SelectAllByID_Multiple("ticket",array("input_by"=>$input_by)); 
															 }
															 if(!empty($data))
                                                             foreach($data as $row):
															 $cid=$obj->SelectAllByVal("ticket","ticket",$row->ticket_id,"cid");
															 $fname=$obj->SelectAllByVal("coustomer","id",$row->cid,"firstname");
															 $lname=$obj->SelectAllByVal("coustomer","id",$row->cid,"lastname");
															 $tid=$obj->SelectAllByVal($table,"id",$_GET['edit'],"ticket_id");
                                                            ?>
                                                            <option <?php if($tid==$row->ticket_id){ ?> selected <?php } ?> value="<?php echo $row->ticket_id; ?>"><?php echo $fname." ".$lname." - ".$row->ticket_id; ?></option> 
                                                            <?php endforeach; ?>
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> Description</label>
                                                    <div class="controls"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"description"); ?>" class="span12" type="text" name="description" /></div>
                                                </div>
                                                
                                                
												
                                                
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Part URL </label>
                                                    <div class="controls"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"part_url"); ?>" class="span12" type="text" name="parts_url" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Quantity</label>
                                                    <div class="span4"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"quantity"); ?>" class="span12" type="number" name="quantity" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Our Cost </label>
                                                    <div class="span4"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"cost"); ?>" class="span12" type="number" name="cost" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Retail For Your Customer</label>
                                                    <div class="span4">
                                                        <input type="number" value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"retail_customer"); ?>" id="retail_cost" name="retail_customer" class="span12" /></span>
                                                    </div>
                                                </div>
                                                
                                                <!--<div class="control-group">
                                                    <label class="span5">* Destination Store</label>
                                                    <div class="span4">
                                                        <select name="store" data-placeholder="Please Select..." class="select-search" tabindex="1" style="width:100%;">
                                                            <option value=""></option> 
                                                            <?php			
															 /*if($input_status==1)
															 {												 										$store_x=$obj->SelectAllByVal($table,"id",$_GET['edit'],"store");
                                                             	$data=$obj->SelectAllByID("store",array("status"=>2));
															 }
															 else
															 {
																 $data=$obj->SelectAllByID("store",array("store_id"=>$input_by));
															 }
                                                             foreach($data as $rows):*/
                                                            ?>
                                                            <option <?php //if($store_x==$rows->store_id){ ?> selected <?php //} ?> value="<?php //echo $rows->store_id; ?>"><?php //echo $rows->name; ?></option> 
                                                            <?php //endforeach; ?>
                                                       </select><?php //echo $store_x; ?>
                                                    </div>
                                                </div>-->
                                                
                                                
                                                
                                                
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px; float:right;">
                                                
                                                <div class="control-group">
                                                    <label class="control-label"></label>
                                                    <div class="controls">
                                                    <label class="checkbox"><div id="uniform-undefined" class="checker">
                                                            <span class="checked"><input style="opacity: 0;" value="1" name="taxable" class="style" checked="" type="checkbox"></span>
                                                        </div>* Taxable
                                                    </label>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Shipping </label>
                                                    <div class="controls"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"shipping"); ?>" class="span12" type="text" name="shipping" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Tracking No. </label>
                                                    <div class="controls">
                                                        <input class="span12" value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"trackingnum"); ?>" type="text" name="trackingnum" />
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> Notes </label>
                                                    <div class="controls"><input value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"notes"); ?>" class="span12" type="text" name="notes" /></div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">* Ordered </label>
                                                    <div class="controls"><input readonly value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"ordered"); ?>" type="text"  class="datepicker" name="ordered" /></div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">* Received </label>
                                                    <div class="controls"><input readonly value="<?php echo $obj->SelectAllByVal($table,"id",$_GET['edit'],"received"); ?>"  class="datepicker"  type="text" name="received" /></div>
                                                </div>

                                                
                                                <div class="control-group">
                                                    <label class="control-label">&nbsp;</label>
                                                    <div class="controls"><button type="submit" name="update" class="btn btn-info"><i class="icon-check"></i> Update Parts Order </button> <button type="reset" name="reset" class="btn btn-warning"><i class="icon-ban-circle"></i> Clear Form </button></div>
                                                </div>
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>
								<?php }else{ ?>
                                <form class="form-horizontal" method="post" name="invoice" action="">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">     
                                           <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px;">
                                                <div class="control-group">
                                                    <label class="control-label">* Ticket :</label>
                                                    <div class="controls">
                                                        <select name="ticket_id" onChange="Ticket_Cost(this.value)" data-placeholder="Please Select..." class="select-search" tabindex="2">
                                                            <option value=""></option> 
                                                            <?php
                                                             if($input_status==1)
															 {
                                                             	$data=$obj->SelectAll("ticket");
															 }
															 else
															 {
																$data=$obj->SelectAllByID_Multiple("ticket",array("input_by"=>$input_by)); 
															 }
															 if(!empty($data))
                                                             foreach($data as $row):
															 if(isset($_GET['fromticket'])){
                                                            ?>
                                                            <option <?php if($_GET['fromticket']==$row->ticket_id){ ?> selected <?php } ?> value="<?php echo $row->ticket_id; ?>"><?php echo $row->ticket_id; ?> : <?php echo $row->date; ?></option> 
                                                            <?php 
															 }
															 else
															 {
															?>
                                                            <option value="<?php echo $row->ticket_id; ?>"><?php echo $row->ticket_id; ?> : <?php echo $row->date; ?></option> 
                                                            <?php	 
															 }
															endforeach; ?>
                                                       </select>
                                                    </div>
                                                </div>


												<div class="control-group">
                                                    <label class="span4"> Ticket Payment Status </label>
                                                    <div class="span6"><input disabled class="span12" type="text" name="ps" id="ps" value=" " /></div>
                                                </div>
												
                                                <div class="control-group">
                                                    <label class="control-label"> Description</label>
                                                    <div class="controls"><input class="span12" type="text" name="description" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Part URL </label>
                                                    <div class="controls"><input class="span12" type="text" name="parts_url" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Quantity</label>
                                                    <div class="span4"><input class="span12" type="number" name="quantity" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Our Cost </label>
                                                    <div class="span4"><input class="span12" type="number" id="cost" name="cost" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="span5">* Retail For Your Customer</label>
                                                    <div class="span4">
                                                        <input type="number" id="retail_cost" name="retail_customer" class="span12" /></span>
                                                    </div>
                                                </div>
                                                
                                                <!--<div class="control-group">
                                                    <label class="span5">* Destination Store</label>
                                                    <div class="span4">
                                                        <select name="store" data-placeholder="Please Select..." class="select-search" tabindex="1" style="width:100%;">
                                                            <option value=""></option> 
                                                            <?php														
                                                             /*$data=$obj->SelectAllByID("store",array("status"=>2));
                                                             foreach($data as $rows):*/
                                                            ?>
                                <option value="<?php //echo $rows->store_id; ?>"><?php //echo $rows->name; ?></option> 
                                                            <?php //endforeach; ?>
                                                       </select>
                                                    </div>
                                                </div>-->
                                                
                                                
                                                
                                                
                                                
                                            </div>
                                            <!-- /selects, dropdowns -->



                                            <!-- Selects, dropdowns -->
                                            <div class="span6" style="padding:0px; margin:0px; float:right;">
                                                
                                                <div class="control-group">
                                                    <label class="control-label"></label>
                                                    <div class="controls">
                                                    <label class="checkbox"><div id="uniform-undefined" class="checker">
                                                            <span class="checked"><input style="opacity: 0;" value="1" name="taxable" class="style" checked="" type="checkbox"></span>
                                                        </div>* Taxable
                                                    </label>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Shipping </label>
                                                    <div class="controls"><input class="span12" type="text" name="shipping" /></div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label"> Tracking No. </label>
                                                    <div class="controls">
                                                        <input class="span12" type="text" name="trackingnum" />
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label"> Notes </label>
                                                    <div class="controls"><input class="span12" type="text" name="notes" /></div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">* Ordered </label>
                                                    <div class="controls"><input class="span4" type="text" name="ordered" /></div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">* Received </label>
                                                    <div class="controls"><input class="span4" type="text" name="received" /></div>
                                                </div>

                                                
                                                <div class="control-group">
                                                    <label class="control-label">&nbsp;</label>
                                                    <div class="controls"><button type="submit" name="create" class="btn btn-success"><i class="icon-plus-sign"></i> Add Line Item </button></div>
                                                </div>
                                            </div>
                                            <!-- /selects, dropdowns -->

                                           

                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>
                                <?php } ?>




                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');  ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
