<?php
include('class/auth.php');
include('class/pos_class.php');
$obj_pos = new pos();
$table = "product";
$table2 = "sales";
$table3 = "invoice";
$table4 = "invoice_payment";
$cashier_id=$obj_pos->cashier_id(@$_SESSION['SESS_CASHIER_ID']);
$cashiers_id=$obj_pos->cashier_actual_id(@$_SESSION['SESS_CASHIER_ID']);
$cart = $obj->cart(@$_SESSION['SESS_CART']);
if(@$_GET['lfwspcs']){ include('include/pos_lfwspcs.php'); }
if(isset($_GET['caslogoutfrompage'])){ $obj->Error("Cashier Can Logout Using This Page .. ", $obj->filename()); }
if(isset($_GET['caslogout'])){ $obj_pos->cashier_logout_without_return(@$_SESSION['SESS_CASHIER_ID']); header("location:logout.php"); }
if(isset($_GET['logout'])){ $obj_pos->cashier_logout(@$_SESSION['SESS_CASHIER_ID']); }
if(isset($_POST['cashier_login'])){ include('include/pos_cashier_login.php'); }
if(isset($_POST['savecus'])){ include('include/pos_savecus.php'); }
if(isset($_POST['store_open'])){ include('include/pos_store_open.php'); }
if(isset($_POST['storecloseing'])){ include('include/pos_storecloseing.php'); }
if(isset($_GET['storecloseingmm'])){ include('include/pos_storecloseingmm.php'); }
if(isset($_POST['store_payout'])){ include('include/pos_store_payout.php'); }
if(isset($_GET['action'])){ include('include/pos_invoice_pdf.php'); }
if(isset($_POST['paidnprint'])){ include('include/pos_pay_paidprint.php'); }
if(isset($_POST['onlypaid'])){ include('include/pos_pay_onlypaid.php'); }
if(isset($_GET['newsales'])){ include('include/pos_newsales.php'); }
if(isset($_GET['newsales_two'])){ include('include/pos_newsales_two.php'); }
if(isset($_GET['clearsales'])){ include('include/pos_clearsales.php'); }
include('include/pos_taxninvoice_check.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<?php echo $obj->bodyhead(); ?>
		<script type="text/javascript" src="js/functions/custom.js"></script>
        <script src="ajax/ajax.js"></script>
        <script src="ajax/pos_ajax.js"></script>
        <script>
		
		$.ajaxSetup({ cache: false });
            function cusid(cid, cart)
            {
                if (cid == "")
                {
                    document.getElementById("mss").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else { // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $("#mss").fadeOut();
                        $("#mss").fadeIn();
                        document.getElementById("mss").innerHTML = xmlhttp.responseText;
                    }
                }
                st = 3;
                xmlhttp.open("GET", "ajax/setversion.php?st=" + st + "&cid=" + cid + "&cart=" + cart, true);
                xmlhttp.send();
            }
			
			function reload_pos_page() {
				location.reload();
			}
			
        </script>
        
        <script>
            function paid_method(method,paid,total_amount)
            {
				//alert('Payment Method');
				if(method==6)
				{
					/*var cash=$('#pam').val();
					var credit=$('#pamc').val();
					
					$('#ddue').val()=total_amount-(cash+credit);*/	
					cash=document.getElementById('pam').value;
					credit=document.getElementById('pamc').value;
					document.getElementById('ddue').value=total_amount-((cash-0)+(credit-0)+(paid-0));
				}
				else
				{
					cash=document.getElementById('pam').value;					
					document.getElementById('ddue').value=total_amount-((cash-0)+(paid-0));
				}
            }
        </script>
        
        <script>
		function loadblankpage(invoice)
		{
			<?php 
			$chk=$obj->exists_multiple("invoice_payment",array("invoice_id"=>$cart));
			if($chk!=0)
			{
			?>
			setTimeout(window.open("pos.php?newsales=1"),5000);	
			<?php
			}
			else
			{
			?>
			setTimeout(window.open("pos.php?refresh"),5000);	
			<?php
			}
			 ?>
		}
		
		
		
		</script>
        <?php
		if(isset($_GET['refresh']))
		{
			?>
            <meta http-equiv="refresh" content="5;url=<?php echo $obj->baseUrl($obj->filename()); ?>">
            <?php
		}
		?>
        <script language="javascript" type="text/javascript">
        function printDiv(divID,amount,total_collection_cash_credit_card,cash_collected_plus,credit_card_collected_plus,opening_cash_plus,opening_credit_card_plus,payout_plus_min,buyback_min,tax_min,current_cash,current_credit_card) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;
			
            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";
			//setTimeout(window.open("pos.php?storecloseingmm="+amount),5000);	
			window.open("pos.php?storecloseingmm="+amount+"&total_collection_cash_credit_card="+total_collection_cash_credit_card+"&cash_collected_plus="+cash_collected_plus+"&credit_card_collected_plus="+credit_card_collected_plus+"&opening_cash_plus="+opening_cash_plus+"&opening_credit_card_plus="+opening_credit_card_plus+"&payout_plus_min="+payout_plus_min+"&buyback_min="+buyback_min+"&tax_min="+tax_min+"&current_cash="+current_cash+"&current_credit_card="+current_credit_card);
            //Print Page
            window.print();
            //Restore orignal HTML
			 //window.location="pos.php?storecloseingmm="+amount;
        }
    </script>
    <script>
		function store_close_report()
		{
			
			//alert("Cart ID : ");
			var dfs="<img src='images/loader-big.gif' />";   
			$('#store_close_report').html(dfs);
			
			param1 = {'fetch':1}; $.post('store_close.php', param1,  function(res1) { $('#store_close_report').html(res1); });
		}
	</script>
    <script type="text/javascript">
	function salesRowLiveEdit(sales_string_id)
	{
		 var sales_id=$('#'+sales_string_id).closest('td').attr('id');
		 var sales_invoice_id=$('#'+sales_string_id).closest('td').attr('class');
		 var sales_amount=$('#'+sales_string_id).attr('title');
		 //alert(sales_id);
		 
		 $('#'+sales_id).html('<input size="5" onkeydown="alertn()" class='+sales_invoice_id+' type="text" name="'+sales_id+'" value="'+sales_amount+'" id="sales_edit_data" >');
		 

	}
	
	function alertn()
	{
		  document.getElementById('sales_edit_data').onkeypress = function(e){
			if (!e) e = window.event;
			if (e.keyCode == '13'){
			  var sales_pid=$(this).attr('name');
			  var sales_invoice_id=$(this).attr('class');
			  var new_sales_amount=$(this).val();
			  $('#'+sales_pid).html('<button type="button" title="'+new_sales_amount+'" class="btn" id="sales_row">$'+new_sales_amount+'</button>');
			  
			  parama = {'sales_id':sales_invoice_id,'sales_pid':sales_pid,'new_sales_amount':new_sales_amount}; 
			  $.post('ajax/load_sales_list_post.php', parama,  function(data) { 
			  		  if(data==0)
					  {
						  location.refresh();
					  }
					  else
					  {
					  $("#msg").fadeOut();
					  $("#msg").fadeIn();
					  document.getElementById("msg").innerHTML=data;
					  }
			   });
			  
			  param1 = {'sales_id':sales_invoice_id}; 
			  $.get('ajax/load_sales_list.php', param1,  function(data) { 
			  		  $("#sales_list").fadeOut();
					  $("#sales_list").fadeIn();
					  document.getElementById("sales_list").innerHTML=data;
			   });
			   
			   param2 = {'sales_id':sales_invoice_id}; 
			  $.get('ajax/load_sales_list_cal.php', param2,  function(data) { 
			  		  $("#subtotal_list").fadeOut();
					  $("#subtotal_list").fadeIn();
					  document.getElementById("subtotal_list").innerHTML=data;
			   });
			  
			  alert('Price Changed Successfully');
			  
			  return false;
			}
		  }
	}
	
	
	</script>
    </head>

    <bod
<?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
<?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
<?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-money"></i>POS  (Sales - <?php echo $cart; ?> ) : Cashier Id - <?php echo $cashiers_id;  ?> <span id="msg" style="float:right; margin-left:50px;"></span></h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Middle navigation standard -->
<?php //include('include/quicklink.php');   ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">




                                <!-- Content Start from here customized -->
                                <div class="block span6">

                                    <!--<a href="#" class="btn btn-success"><i class="icon-ok-sign"></i>Save Invoice</a>
                                    <a href="#" class="btn btn-danger"><i class="icon-trash"></i> Delete Invoice & All Record</a>
                                    <a href="#" class="btn btn-primary"><i class="icon-edit"></i> Edit Invoice</a>
                                    <a href="#" class="btn btn-warning"><i class="icon-print"></i> Print Invoice</a>
                                    <a href="#" class="btn btn-info"><i class="icon-bell"></i >Clone</a>
                                    <a href="#" class="btn btn-success"><i class="icon-screenshot"></i> Quick Payment</a>
                                    <a href="#" class="btn btn-success"><i class="icon-screenshot"></i> Payment</a>

                                    -->                                    
<a href="<?php echo $obj->filename(); ?>?newsales_two" class="btn btn-danger"><i class="icon-ok-sign"></i> Make New Sales</a>
<a href="<?php echo $obj->filename(); ?>?newsales_two" class="btn btn-success"><i class="icon-check"></i> Clear POS</a>
<?php 
if($cashiers_id!=0){
$chkopenstore=$obj->exists_multiple("store_open",array("sid"=>$input_by,"status"=>1)); 
if($chkopenstore==1){ ?>
<span id="stccash">
	<a data-toggle="modal" href="#logout_store_close" class="btn btn-danger">
    	<i class="icon-off"></i> Close Store 
    </a>
</span>
<!-- Dialog content -->
<!-- href="#logout_store_close"  myModal3-->
<div id="logout_store_close" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Cashier Login to confirm | Close Store </h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                          
                    <div class="control-group">
                        <label class="control-label"> Username  </label>
                        <div class="controls">
                            <input type="text" id="strurs" placeholder="Username" class="span6" name="username">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label"> Password  </label>
                        <div class="controls">
                            <input type="password" id="strpass" placeholder="Password" class="span6" name="password">
                        </div>
                    </div>
                    <div class="control-group" id="mss"></div>
            </div>
			
        </div>
        <div class="modal-footer">
            <button type="reset" style="float:left;" class="btn btn-warning"  name="reset">Clear </button>
<button type="button" style="float:left;" onClick="store_close_confirm(<?php echo $cashiers_id;  ?>)" class="btn btn-info"  name="cashier_login">Login </button>
        </div>
</div>
<!-- /dialog content --> 
 <!-- Dialog content -->
<?php 
include('include/store_close.php');
?>



<!-- /dialog content -->
<a  data-toggle="modal" href="#myModal46" class="btn btn-info"><i class="icon-tags"></i> Hour Worked </a>



 <!-- Dialog content -->
<div id="myModal46" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" method="post" action="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i>  Hour Worked  <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                
                    <div class="table-overflow">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>S/L</th>
                            <th>Date</th>
							<th>Working Hour</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql_product=$obj->SelectAllByID_Multiple_limit("store_punch_time",array("cashier_id"=>$cashiers_id,"sid"=>$input_by),"10");
						$i=1;
						$caltimearray=array();
						if(!empty($sql_product))
						foreach($sql_product as $product):
						?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $product->date; ?></td>
							<td>
							<?php
							if($product->outdate!='')
							{
					   echo $obj->punchtimetotal($product->indate." ".$product->intime,$product->outdate." ".$product->outtime);	
					   		$caltimearray[]=$obj->punchtimetotal($product->indate." ".$product->intime,$product->outdate." ".$product->outtime);	
							}
							else
							{
								echo "Still Working...";
							}
							
							
							?>
							</td>
						</tr>
						<?php 
						$i++;
						endforeach; ?>
					</tbody>
				</table>
			</div>
				<h4> Total Working Hour : 
                <?php 
				$hour = 0;    
				$min = 0;
				$sec = 0;    
				foreach($caltimearray as $shift) {
					$hourar=explode(':',$shift);
					$hour+=$hourar[0];
					$min+=$hourar[1];
					$sec+=$hourar[2];
				}
				
				if($min!=0)
				{
					if($min>59)
					{
						$getnewhour=intval($min/60);
						$hours=$hour+$getnewhour;
						$minmin=$getnewhour*60;
						$actualmin=$min-$minmin;
						if(strlen($actualmin)==1)
						{
							echo $hours." : 0".$actualmin." : 00 Second";	
						}
						else
						{
							echo $hours." : ".$actualmin." : 00 Second";
						}
					}
					else
					{
						if(strlen($min)==1)
						{
							echo $hour." : 0".$min." : 00 Second";
						}
						else
						{
							echo $hour." : ".$min." : 00 Second";
						}
					}
				}
				else
				{
					echo $hour." : ".$min." : 00 Second";	
				}
				?>
                 </h4>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary"  name="store_payout">Save</button>
        </div>
        </form>
</div>
<!-- /dialog content -->
<a  data-toggle="modal" href="#myModal44" class="btn btn-info"><i class="icon-tags"></i> Payout </a>
 <!-- Dialog content -->
<div id="myModal44" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" method="post" action="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Payout/Drop Deatil <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                
                    <div class="control-group">
                        <label class="control-label"> Amount </label>
                        <div class="controls">
                            <input class="span6" type="text" id="cash" name="cash" placeholder="Amount" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"> Reason </label>
                        <div class="controls">
                            <textarea class="span6" type="text" id="reason" name="reason" placeholder="Reason"></textarea></div>
                    </div>
                    <div class="control-group">
                    Enter a negative amount if removing cash from the drawer, enter a positive amount if adding cash to the drawer.
                    </div>

            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary"  name="store_payout">Save</button>
        </div>
        </form>
</div>
<!-- /dialog content -->
 <?php }
 else
 { ?>
 <!--<a  data-toggle="modal" href="#myModal3" class="btn btn-success"><i class="icon-inbox"></i> Open Store </a>-->
<span id="oopencash">
	<a data-toggle="modal" href="#login_store_open" class="btn btn-success">
    	<i class="icon-inbox"></i> Open Store 
    </a>
</span>
 
<!-- href="#logout_store_close"  myModal3-->
<div id="login_store_open" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Cashier Login to confirm | Store Open </h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                          
                    <div class="control-group">
                        <label class="control-label"> Username  </label>
                        <div class="controls">
                            <input type="text" id="stturs" placeholder="Username" class="span6" name="username">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label"> Password  </label>
                        <div class="controls">
                            <input type="password" id="sttpass" placeholder="Password" class="span6" name="password">
                        </div>
                    </div>
                    <div class="control-group" id="tss"></div>
            </div>
			
        </div>
        <div class="modal-footer">
            <button type="reset" style="float:left;" class="btn btn-warning"  name="reset">Clear </button>
<button type="button" style="float:left;" onClick="store_open_confirm(<?php echo $cashiers_id;  ?>)" class="btn btn-info"  name="cashier_login">Login </button>
        </div>
</div>
<!-- /dialog content -->  
 
 <!-- Dialog content -->
<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" method="post" action="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Open Store <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
            
                    <span> <strong>Store Opening Amount</strong> </span>
                
                    <div class="control-group">
                        <label class="control-label"> Cash </label>
                        <div class="controls">
                        	
                            <input class="span6" type="text" id="cash" name="cash" placeholder="Cash Amount" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"> Credit Card </label>
                        <div class="controls">
                            <input class="span6" type="text" id="square" name="square" placeholder="Credit Card Amount" /></div>
                    </div>
                    


            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name="store_open"> Open Store </button>
        </div></form>
</div>
<!-- /dialog content -->
<?php }
}
 ?>
<a  data-toggle="modal" href="#myModal4" class="btn btn-info"><i class="icon-time"></i> <?php if($cashiers_id!=0){ ?>Time Clock <?php }else{ ?> Cashier Login Here <?php } ?></a> 
<?php if($cashier_id==1){ ?>
<!--<a href="<?php //echo $obj->filename(); ?>?logout=1" class="btn btn-danger"><i class="icon-off"></i> Logout </a>-->
<a  data-toggle="modal" href="#logout" class="btn btn-danger"><i class="icon-off"></i> Logout </a>
<?php } ?>

 <!-- Dialog content -->
<div id="myModal4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" method="post" action="">
    <?php if($obj_pos->cashier_login(@$_SESSION['SESS_CASHIER_ID'])==1){ ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Time Clock <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                          
                    <div class="control-group">
                        <label class="control-label"> Date Time </label>
                        <div class="controls">
                            <input size="10" readonly id="indate"  value="<?php echo date('Y-m-d'); ?>" class="datepicker" type="text">
                        </div>
                    </div>
            </div>
            <div class="row-fluid" id="punchtime">
            	<?php
				$chkpunch=$obj->exists_multiple("store_punch_time",array("sid"=>$input_by,"date"=>date('Y-m-d')));
				if($chkpunch!=0)
				{
				?>
                 <div class="table-overflow">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Date IN</th>
                                <th>Time In</th>
                                <th>Date Out</th>
                                <th>Time Out</th>
                                <th>Elapsed Time (HH:MM)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql_product=$obj->SelectAllByID_Multiple("store_punch_time",array("sid"=>$input_by,"date"=>date('Y-m-d'),"cashier_id"=>$cashier_id));
                            $i=1;
                            if(!empty($sql_product))
                            foreach($sql_product as $product):
                            ?>
                            <tr>
                                <td><?php echo $product->indate; ?></td>
                                <td><?php echo $product->intime; ?></td>
                                <td><?php echo $product->outdate; ?></td>
                                <td><?php echo $product->outtime; ?></td>
                                <td>
                                <?php
								if($product->outdate!='')
								{
									echo $obj->durations($product->indate." ".$product->intime,$product->outdate." ".$product->outtime);	
								}
								?>
                                </td>
                            </tr>
                            <?php 
                            $i++;
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>         
            </div>

        </div>
        <div class="modal-footer">
        	<?php 
			if($chkpunch==1){ $sssave="Punch  In | Out"; }else{ $sssave="Punch In | Out"; }
			?>
            <button type="button" class="btn btn-primary" onClick="punchin()"  name="store_open"><?php echo $sssave; ?></button>
        </div>
        <?php }else{ ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Cashier Login <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                          
                    <div class="control-group">
                        <label class="control-label"> Username  </label>
                        <div class="controls">
                            <input type="text" placeholder="Username" class="span6" name="username">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label"> Password  </label>
                        <div class="controls">
                            <input type="password" placeholder="Password" class="span6" name="password">
                        </div>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="reset" style="float:left;" class="btn btn-warning"  name="reset">Clear </button>
            <button type="Submit" style="float:left;" class="btn btn-info"  name="cashier_login">Login </button>
        </div>
        <?php } ?>
        </form>
</div>
<!-- /dialog content -->


<!-- Dialog content -->

<!-- /dialog content -->

 <!-- Dialog content -->
<div id="logout" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form class="form-horizontal" method="post" action="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5 id="myModalLabel"> <i class="icon-inbox"></i> Please Login To Logout From Cash Counter  <span id="mss"></span></h5>
        </div>
        <div class="modal-body">

            <div class="row-fluid">
                          
                    <div class="control-group">
                        <label class="control-label"> Username  </label>
                        <div class="controls">
                            <input type="text" placeholder="Username" class="span6" name="username">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label"> Password  </label>
                        <div class="controls">
                            <input type="password" placeholder="Password" class="span6" name="password">
                            <input type="hidden" name="logval" value="2">
                        </div>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="reset" style="float:left;" class="btn btn-warning"  name="reset">Clear </button>
            <button type="Submit" style="float:left;" class="btn btn-info"  name="cashier_login">Login </button>
        </div>
        </form>
</div>
<!-- /dialog content -->
                                </div>
                                <fieldset>
								<div id="store_close_message"></div>

                                    <div class="row-fluid block">
                                        <div class="well row-fluid span7">
                                            <div class="tabbable">
                                                <!--start ul tabs -->
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab1" data-toggle="tab">Main ( 0 - 49 )</a></li>
                                                    <li><a href="#tab2" data-toggle="tab">Page 2 ( 50 - 300 )</a></li>
                                                    <li><a href="#tab201" data-toggle="tab">Phone Inventory</a></li>
                                                    <li><a href="#tab3" data-toggle="tab">Barcode</a></li>
                                                    <li><a href="#tab4" data-toggle="tab"> Inventory </a></li>
                                                    <li><a href="#tab5" data-toggle="tab"> Manualy </a></li>
                                                </ul>
                                                <!--end ul tabs -->  
                                                <!--start data tabs --> 
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab1">
                                                        <!--tab 1 content start from here-->
														<!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:0px;">
                                                            <div class="control-group">
                                                                <label class="control-label">Item :</label>
                                                                <div class="controls">
                                                                    <select name="fst_pids" id="fst_pids"  style="width:80%;" data-placeholder="Choose a Item..." class="select-search select2-offscreen" tabindex="-1">
                                                                        <option value=""></option> 
																			<?php
                                                                            if($input_status==1){
																			$sqlproduct_pos = $obj_pos->SelectAllOnlyLimit("product_other_inventory","0","50");
																			}else{
																			$sqlproduct_pos =$obj_pos->SelectAllOnlyOneCondLimit("product_other_inventory","input_by",$input_by,"0","50");	
																			}
                                                                            if (!empty($sqlproduct_pos))
                                                                                foreach ($sqlproduct_pos as $row):
                                                                                    ?>
                                                                                <option value="<?php echo $row->id; ?>">
        																			<?php echo $row->name; ?>
                                                                                </option> 
    																			<?php endforeach; ?> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label">Quantity:</label>
                                                                <div class="controls"><input class="span2" value="1" type="number" name="regular" id="fst_quan" /></div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div class="controls"><button onClick="fst_inventory_sales('<?php echo $cart; ?>')" type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Add To Invoice </button></div>
                                                            </div>
                                                        </div>
                                                        <!-- /selects, dropdowns -->

                                                        <!--tab 1 content start from here-->
                                                    </div>
                                                    <div class="tab-pane" id="tab2">

														<!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:0px;">
                                                            <div class="control-group">
                                                                <label class="control-label">Item :</label>
                                                                <div class="controls">
                                                                    <select name="snd_pids" id="snd_pids"  style="width:80%;" data-placeholder="Choose a Item..." class="select-search select2-offscreen" tabindex="-1">
                                                                        <option value=""></option> 
																			<?php
                                                                            if($input_status==1){
																			$sqlproduct_pos_nd = $obj_pos->SelectAllOnlyLimit("product_other_inventory","50","300");
																			}else{
																			$sqlproduct_pos_nd =$obj_pos->SelectAllOnlyOneCondLimit("product_other_inventory","input_by",$input_by,"50","300");	
																			}
                                                                            if (!empty($sqlproduct_pos_nd))
                                                                                foreach ($sqlproduct_pos_nd as $row):
                                                                                    ?>
                                                                                <option value="<?php echo $row->id; ?>">
        																			<?php echo $row->name; ?>
                                                                                </option> 
    																			<?php endforeach; ?> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label">Quantity:</label>
                                                                <div class="controls"><input class="span2" value="1" type="number" name="regular" id="snd_quan" /></div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div class="controls"><button onClick="snd_inventory_sales('<?php echo $cart; ?>')" type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Add To Invoice </button></div>
                                                            </div>
                                                        </div>
                                                        <!-- /selects, dropdowns -->
                                                        

                                                        

                                                        <!--tab 2 content start from here-->

                                                    </div>
                                                    <div class="tab-pane" id="tab201">

                                                        <!--tab 2 content start from here-->
                                                        <!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:0px;">
                                                            <div class="control-group">
                                                                <label class="control-label">Item :</label>
                                                                <div class="controls">
                                                                    <select name="phone_pids" id="phone_pids"  style="width:80%;" data-placeholder="Choose a Item..." class="select-search select2-offscreen" tabindex="-1">
                                                                        <option value=""></option> 
																			<?php
                                                                            if($input_status==1){
																			$sqlproduct_phone= $obj_pos->SelectAllOnlyLimit("product_phone_inventory","0","300");
																			}else{
																			$sqlproduct_phone=$obj_pos->SelectAllOnlyOneCondLimit("product_phone_inventory","input_by",$input_by,"0","300");	
																			}
                                                                            if (!empty($sqlproduct_phone))
                                                                                foreach ($sqlproduct_phone as $row):
                                                                                    ?>
                                                                                <option value="<?php echo $row->id; ?>">
        																			<?php echo $row->name; ?>
                                                                                </option> 
    																			<?php endforeach; ?> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label">Quantity:</label>
                                                                <div class="controls"><input class="span2" value="1" type="number" name="regular" id="phone_quan" /></div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div class="controls"><button onClick="phone_inventory_sales('<?php echo $cart; ?>')" type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Add To Invoice </button></div>
                                                            </div>
                                                        </div>
                                                        <!-- /selects, dropdowns -->
                                                        

                                                        <!--tab 2 content start from here-->

                                                    </div>
                                                    <div class="tab-pane" id="tab3">
                                                        <!--barcode tab content start from here-->
                                                        <!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:15px 0px 0px 0px;">
                                                            <div class="navbar">
                                                                <div class="navbar-inner">
                                                                    <h5><i class="icon-barcode"></i> Add From Barcode</h5>
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">UPC Code :</label>
                                                                <div class="controls"><input class="span4" id="barcode_reader_place" type="text" name="regular"  onKeydown="Javascript: if (event.keyCode == 13)
                                                                            barcode_sales(this.value, '<?php echo $cart; ?>');"  /> Type &amp; Press Enter / Use Your Barcode Reader</div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label">Quantity:</label>
                                                                <div class="controls"><input class="span4" type="number" value="1" /></div>
                                                            </div>
                                                            <!--<div class="control-group">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div class="controls"><button type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Create Line Item </button></div>
                                                            </div>-->
                                                        </div>
                                                        <!-- /selects, dropdowns -->
                                                        <!--barcode tab Start from here-->
                                                    </div>
                                                    <div class="tab-pane" id="tab4">
                                                        <!--form tab content start here-->
                                                        <!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:0px;">
                                                            <div class="navbar">
                                                                <div class="navbar-inner">
                                                                    <h5><i class="icon-tag"></i> Add From Inventory</h5>
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">Item :</label>
                                                                <div class="controls">
                                                                    <select name="pids" id="pids"  style="width:80%;" data-placeholder="Choose a Item..." class="select-search select2-offscreen" tabindex="-1">
                                                                        <option value=""></option> 
<?php
if($input_status==1){
$sqlpdata = $obj->SelectAll($table);
}else{
$sqlpdata = $obj->SelectAllByID($table,array("input_by"=>$input_by));	
}
if (!empty($sqlpdata))
    foreach ($sqlpdata as $row):
        ?>
                                                                                <option value="<?php echo $row->id; ?>">
        <?php echo $row->name; ?>
                                                                                </option> 
    <?php endforeach; ?> 
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label">Quantity:</label>
                                                                <div class="controls"><input class="span2" value="1" type="number" name="regular" id="quan" /></div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div class="controls"><button onClick="inventory_sales('<?php echo $cart; ?>')" type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Create Line Item </button></div>
                                                            </div>
                                                        </div>
                                                        <!-- /selects, dropdowns -->
                                                        <!--form tab content end here-->
                                                    </div>
                                                    <div class="tab-pane" id="tab5">
                                                        <!--form tab content start here-->
                                                        <!-- Selects, dropdowns -->
                                                        <div class="span12" style="padding:0px; margin:0px;">
                                                            <div class="navbar">
                                                                <div class="navbar-inner">
                                                                    <h5><i class="icon-cog"></i> Add Manual Item</h5>
                                                                </div>
                                                            </div>
                                                            <form method="get" action="" name="manual">
                                                                <fieldset>
                                                                    <div class="control-group">
                                                                        <label class="control-label">Item:</label>
                                                                        <div class="controls">
                                                                            <select name="pid" id="pid" style="width:80%;" data-placeholder="Choose a Item..." class="select-search select2-offscreen" tabindex="-1">
                                                                                <option value=""></option> 
<?php
if($input_status==1){
$sqlpdata = $obj->SelectAll($table);
}else{
$sqlpdata = $obj->SelectAllByID($table,array("input_by"=>$input_by));	
}
if (!empty($sqlpdata))
    foreach ($sqlpdata as $row):
        ?>
                                                                                        <option value="<?php echo $row->id; ?>">
                                                                                <?php echo $row->name; ?>
                                                                                        </option> 
                                                                                <?php endforeach; ?> 
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label">Description :</label>
                                                                        <div class="controls"><input class="span12" type="text" name="des" /></div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label">Price:</label>
                                                                        <div class="controls"><input  class="span12" type="text" name="price" id="price" /></div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label">Cost:</label>
                                                                        <div class="controls"><input  class="span12" type="text" name="cost" id="cost" /></div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label">Quantity:</label>
                                                                        <div class="controls"><input class="span12" type="text" name="quantity" id="quantity" /></div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label">Taxable:</label>
                                                                        <div class="controls"><label class="checkbox inline"><div id="uniform-undefined" class="checker"><span class="checked"><input style="opacity: 0;" name="taxable" class="style" value="1" id="tax" type="checkbox"></span></div>Checked</label></div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <div class="controls"><button onClick="manual_sales('<?php echo $cart; ?>')" type="button" class="btn btn-success"><i class="icon-plus-sign"></i> Add Line Item </button></div>
                                                                    </div>
                                                                </fieldset>
                                                            </form>
                                                        </div>
                                                        <!-- /selects, dropdowns -->
                                                        <!--form tab content end here-->
                                                    </div>
                                                </div>
                                                <!--End data tabs -->   
                                            </div>
                                        </div>
                                        <!-- General form elements -->


                                        <!-- General form elements -->
                                        <div class="well row-fluid span5">
                                            <div class="navbar">
                                                <div class="navbar-inner">
                                                    <h5><i class="font-money"></i> Transaction 
                                                    	<span class="controls" style="margin-left:20px;">
                                                                    <select name="custo" style="width:170px;" onChange="new_customer(this.value, '<?php echo $cart; ?>')" id="customername"  data-placeholder="Choose a Customer" class="select-search select2-offscreen" tabindex="-2">
                                                                        <option value=""></option> 
																		<?php
																		$invoice_cid=$obj->SelectAllByVal($table3,"invoice_id",$_SESSION['SESS_CART'],"cid");
																		if($input_status==1)
																		{
																			$sqlpdata=$obj->SelectAll("coustomer");
																		}
																		else
																		{
																			$sqlpdata=$obj->SelectAllByID("coustomer",array("input_by"=>$input_by));
																		}
                                                                        //$sqlpdata = $obj->SelectAll("coustomer");
                                                                        if (!empty($sqlpdata))
                                                                        foreach ($sqlpdata as $row):
                                                                        ?>
                                                                        <option <?php if($invoice_cid==$row->id){ ?> selected <?php } ?> onclick="cusid(this.value, '<?php echo $cart; ?>')" value="<?php echo $row->id; ?>">
                                                                        <?php echo $row->firstname . " " . $row->lastname; ?>
                                                                        </option> 
                                                                        <?php endforeach; ?> 
                                                                        <option value="0">Add New Customer</option> 
                                                                    </select>

                                                                </span>
                                                        <!--<a style="margin-left:60px;" data-toggle="modal" href="#myModal"> <i class="icon-user"></i>  Customer Info </a>-->
                                                        <a data-toggle="modal" href="#myModal1"> <i class="icon-tasks"></i> Tax </a>
                                                    </h5>
                                                </div>
                                            </div>


                                            <!-- Dialog content -->
                                            <div id="myModal" class="modal hide fade" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <form class="form-horizontal" method="post" action="">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h5 id="myModalLabel">Customer Detail <span id="mss"></span></h5>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="row-fluid">

                                                            <div class="control-group" id="newcus_block">
                                                                <label class="control-label" style="display:none;" id="newcus_label">Choose Customer:</label>
                                                                <div class="controls" id="newcus" style="display:none;">
                                                                    <select name="custo" onChange="new_customer(this.value, '<?php echo $cart; ?>')" id="customername"  data-placeholder="Choose a Customer" class="select-search select2-offscreen" tabindex="-2">
                                                                        <option value=""></option> 
                                                                        <option value="<?php echo $def_cus; ?>"><?php echo $obj->SelectAllByVal("customer_list","id",$def_cus,"fullname"); ?></option> 
																		<?php
																		if($input_status==1)
																		{
																			$sqlpdata=$obj->SelectAll("coustomer");
																		}
																		else
																		{
																			$sqlpdata=$obj->SelectAllByID("coustomer",array("input_by"=>$input_by));
																		}
                                                                        //$sqlpdata = $obj->SelectAll("coustomer");
                                                                        if (!empty($sqlpdata))
                                                                        foreach ($sqlpdata as $row):
																		if($row->id!=$def_cus)
														 				{
                                                                        ?>
                                                                        <option <?php if($invoice_cid==$row->id){ ?> selected <?php } ?> onclick="cusid(this.value, '<?php echo $cart; ?>')" value="<?php echo $row->id; ?>">
                                                                        <?php echo $row->firstname . " " . $row->lastname; ?>
                                                                        </option> 
                                                                        <?php 
																		}
																		endforeach; ?> 
                                                                        <option value="0">Add New Customer</option> 
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <span id="cus_sel">
                                                                <div class="control-group" id="new_business">
                                                                    <label class="control-label">Business Name :</label>
                                                                    <div class="controls">
                                                                        <input class="span6" type="text" id="businessname" placeholder="Customer Business here..." /></div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Phone Number </label>
                                                                    <div class="controls">
                                                                        <input class="span6" type="text" id="phonenumber" placeholder="Customer Phone here..." /></div>
                                                                </div>
                                                            </span>


                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal">Close</button>
                                                        <button class="btn btn-primary" name="savecus"  type="submit">Save changes</button>
                                                    </div></form>
                                            </div>
                                            <!-- /dialog content -->


                                            <!-- Dialog content -->
                                            <div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h5 id="myModalLabel"> <?php
                                                                        $taxst = $obj->SelectAllByVal("pos_tax","invoice_id",$cart,"status");
                                                                        echo $obj_pos->tax_status($taxst);
                                                                        ?> </h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal">
                                                        <div class="row-fluid">

                                                            <div class="control-group">
                                                                <label class="control-label">You Can Change </label>
                                                                <div class="controls">
                                                                    <label class="radio inline"><input type="radio" name="radio3" value="2" <?php if ($taxst == 2) { ?> checked="checked" <?php } ?> onClick="pos_tax('<?php echo $cart; ?>', '2')" class="style">Part Tax</label>
                                                                    <label class="radio inline"><input type="radio" name="radio3" value="1" <?php if ($taxst == 1) { ?> checked="checked" <?php } ?> onClick="pos_tax('<?php echo $cart; ?>', '1')" class="style">Full Tax</label>
                                                                    <label class="radio inline"><input type="radio" name="radio3" value="0" <?php if ($taxst == 0) { ?> checked="checked" <?php } ?>  onClick="pos_tax('<?php echo $cart; ?>', '0')" class="style" >No Tax</label> 

                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <div class="controls" id="pos_tax"> 

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn" data-dismiss="modal">Close</button>
                                                    <button class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /dialog content -->




                                            

										<?php include('include/pos_paid.php'); ?>

                                                    
								     
                                        <!-- /general form elements -->
                                        
                                        
                                        <!-- Dialog paid -->
                                        <div id="paid" class="modal hide fade" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                <h5 id="myModalLabel"> Payment Detail</h5>
                                            </div>
                                            <form class="form-horizontal" action="" method="post">
                                                <div class="modal-body">

                                                    <div class="row-fluid">

                                                        <div class="control-group">
                                                            <label class="span4">Payment Method </label>
                                                            <div class="span8" id="newcus">
                                                                <select name="customername" id="customername" data-placeholder="Choose a Payment..." onChange="paytotal('<?php echo $cart; ?>', this.value)" class="select-search select2-offscreen" tabindex="-1">
                                                                    <option value=""></option> 
																	<?php
                                                                    $sqlpdata = $obj->SelectAll("payment_method");
                                                                    if (!empty($sqlpdata))
                                                                    foreach ($sqlpdata as $row):
                                                                    ?>
                                                                    <option value="<?php echo $row->id; ?>">
                                                                    <?php echo $row->meth_name; ?>
                                                                    </option> 
                                                                    <?php 
																	endforeach; 
																	?>  
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <span id="ss">

                                                        </span>


                                                    </div>

                                                </div>
                                                <div class="modal-footer" id="buttonshow"></div>
                                            </form>
                                        </div>
                                        
                                        <!-- /dialog paid -->
                                        
                                        
                                        
                                        
                                        
                                        <!-- Dialog content -->
                                            <div id="tradein" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <form class="form-horizontal" method="post" action="create_buyback.php">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h5 id="myModalLabel"><i class="icon-random"></i> Create Buyback For Trade-in <span id="mss"></span></h5>
                                                    </div>
                                                    <div class="modal-body">
                                                    
                                                    <?php 
													if(!isset($_SESSION['SESS_CART_BUYBACK'])) 
													{
														$obj->newcart_buyback(@$_SESSION['SESS_CART_BUYBACK']);
														$cart_trade_in = $obj->cart_buyback(@$_SESSION['SESS_CART_BUYBACK']);
													}
													else
													{
														$cart_trade_in = $obj->cart_buyback(@$_SESSION['SESS_CART_BUYBACK']);
													}
													?>
														<?php
														$getcid=$obj->SelectAllByVal("invoice","invoice_id",$_SESSION['SESS_CART'],"cid");
														if($getcid!='')
														{
														?>
                                                        <div class="row-fluid">
															
                                                            <div class="span6" style="margin: 0;">
                                                               <div class="control-group">
                                                                    <input type="text" name="model" class="span12" placeholder="Model " />
                                                                    <input type="hidden" name="buyback_id" value="<?php echo $cart_trade_in; ?>">
                                                                    <input type="hidden" name="cid" value="<?php echo $getcid; ?>">
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <input type="text" name="carrier" class="span12" placeholder="Type Carrier Name" />
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <input type="text" name="imei" class="span12" placeholder="Put Device IMEI Number" />
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <input type="text" name="type_color" class="span12" placeholder="Please Type Color" />
                                                                </div>
                                                                
                                                                
                                                                
                                                            </div>
                                                            
                                                            <div class="span6">
                                                            	
                                                                <div class="control-group">
                                                                    <input type="text" name="gig" class="span12" placeholder="Please Type Gig" />
                                                                </div>
                                                  
                                                                <div class="control-group">
                                                                    <input type="text" name="condition" class="span12" placeholder="Please Type Your Device Condition" />
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <input type="text" name="price" class="span12" placeholder="Please Type Price" />
                                                                </div>
                                                                
                                                                <div class="control-group">
                                                                    <?php 
                                                                    $sqlpm=$obj->SelectAll("payment_method");
                                                                    $i=1;
                                                                    if(!empty($sqlpm))
                                                                    foreach($sqlpm as $pm):
                                                                    if($i==1)
                                                                    {
                                                                    ?>
                                                                    <label class="radio inline"><input type="radio" checked name="payment_method" value="<?php echo $pm->id; ?>" class="style" id="pm_<?php echo $pm->id; ?>"><strong><?php echo $pm->meth_name; ?> </strong></label>
                                                                    <?php
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                    <label class="radio inline"><input type="radio" name="payment_method" value="<?php echo $pm->id; ?>" class="style" id="pm_<?php echo $pm->id; ?>"><strong><?php echo $pm->meth_name; ?> </strong></label>
                                                                    <?php	
                                                                    }
                                                                    $i++;
                                                                    endforeach; ?>
                                                                </div>
                                                                
                                                            
                                                            
                                                            
                                                        </div>
														
                                                        
                                                    </div>
													<?php }else{ ?>
                                                    <div class="row-fluid">
                                                    <label class="label label-warning">Please Select A Customer First</label>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="modal-footer">
                                                    	<input type="hidden" name="pos_id" value="<?php echo $_SESSION['SESS_CART']; ?>">
                                                        <button class="btn" data-dismiss="modal">Close</button>
                                                        <?php if($getcid!=''){ ?>
                                                        <button type="submit" name="create_tradein" class="btn btn-success"><i class="icon-ok"></i> Create BuyBack </button>
                                                    	<?php } ?>
                                                    </div>
                                                    </form>
                                            </div>
                                            <!-- /dialog content -->
                                            
                                        
                                        

                                    </div>



                                    <!-- General form elements -->

                                    <!-- /general form elements -->






                                    <div class="clearfix"></div>

                                    <!-- Default datatable -->

                                    <!-- /default datatable -->


                                </fieldset>                     



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
<?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
