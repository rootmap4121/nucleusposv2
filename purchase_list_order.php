<?php
include('class/auth.php');
$table="purchase";
if(isset($_GET['del']))
{
	$obj->deletesing("id",$_GET['del'],$table);	
}
function purchase_status($st)
{
	if($st==1)
	{
		return "Pending";	
	}
	elseif($st==2)
	{
		return "Partial";	
	}
	elseif($st==3)
	{
		return "Complete";	
	}
	
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>

    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="icon-tasks"></i> Purchase Order List </h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">

                            <!-- Content container -->
                            <div class="container">


                               
                            <a href="purchase.php" class="btn btn-success"> <i class="icon-plus-sign"></i> New Purchase Order  </a>
                            <a href="purchase_list_order.php" class="btn btn-success"> <i class="icon-tasks"></i> List Purchase Order  </a>
                            <br>
 <br> 
                                <!-- Content Start from here customized -->


                                <form class="form-horizontal" method="get" name="invoice" action="create_invoice.php">
                                    <fieldset>
                                        <!-- General form elements -->
                                        <div class="row-fluid  span12 well">     
                                            <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Shipping Notes</th>
                                                            <th>General Notes</th>
                                                            <th>Status</th>
                                                            <th>Vendor</th>
                                                            <th>Expected</th>
                                                            <th>Total</th>
                                                            <th width="60">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
														if($input_status==1)
														{ 
															$sqlpurchase_list=$obj->SelectAll($table);
														}
														elseif($input_status==5)
														{
															$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
															if(!empty($sqlchain_store_ids))
															{
																$array_ch = array();
																foreach($sqlchain_store_ids as $ch):
																	array_push($array_ch,$ch->store_id);
																endforeach;
																
																include('class/report_chain_admin.php');	
																$obj_report_chain = new chain_report();
																
																$sqlpurchase_list=$obj_report_chain->SelectAllByID_Multiple_Or($table,$array_ch,"store_id","1");
																
																
															}
															else
															{
																//echo "Not Work";
																$sqlpurchase_list="";
																$record=0;
																$record_label="Total Record ( ".$record." )"; 
															}
														}
														else
														{
															$sqlpurchase_list=$obj->SelectAllByID($table,array("store_id"=>$input_by));	
														}
														$i=1;
														if(!empty($sqlpurchase_list))
														foreach($sqlpurchase_list as $list):
														?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $list->ship_notes; ?></td>
                                                                <td><?php echo $list->gene_notes; ?></td>
                                                                <td><?php echo purchase_status($list->status); ?></td>
                                                                <td><?php echo $list->vendor; ?></td>
                                                                <td><?php echo $list->expec_date; ?></td>
                                                                <td>$<?php echo @number_format($list->total,2); ?></td>
                                                                <td>
                                                                <a href="purchase.php?edit=<?php echo $list->id; ?>" class="hovertip"   onclick="javascript:return confirm('Are you absolutely sure to Edit This?')" title="Edit Detail"><i class="icon-edit"></i></a>
                                                                <a href="<?php echo $obj->filename(); ?>?del=<?php echo $list->id; ?>" class="hovertip"   onclick="javascript:return confirm('Are you absolutely sure to Delete This?')" title="Delete Detail"><i class="icon-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        <?php 
														$i++;
														endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /general form elements -->     


                                        <div class="clearfix"></div>

                                        <!-- Default datatable -->

                                        <!-- /default datatable -->


                                    </fieldset>                     

                                </form>


                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php');  ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
