<?php
include('class/auth.php');
include('class/report_customer.php');
$report=new report(); 
if (isset($_GET['del'])) {
    $obj->deletesing("id", $_GET['del'], "checkin_request");
}

function checkin_status($st) {
    if ($st == 1) {
        return "Completed";
    } else {
        return "Not Completed";
    }
}

function checkin_paid($st) {
    if ($st == 0) {
        return "<label class='label label-danger'>Not Paid</label>";
    } else {
        return "<label class='label label-success'>Paid</label>";
    }
}

if(@$_GET['export']=="excel") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlticket = $report->SelectAllDate("checkin_list",$from,$to,"1");
			$record = $report->SelectAllDate("checkin_list",$from,$to,"2");
			$record_label="| Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlticket = $obj->SelectAll("checkin_list");
			$record = $obj->totalrows("checkin_list");
			$record_label=""; 
		}
	}
	elseif($input_status==5)
	{
		
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		{
			$array_ch = array();
			foreach($sqlchain_store_ids as $ch):
				array_push($array_ch,$ch->store_id);
			endforeach;
			
			    include('class/report_chain_admin.php');	
				$obj_report_chain = new chain_report();
				$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("checkin_list",$array_ch,"input_by","1");
				$record =$obj_report_chain->SelectAllByID_Multiple_Or("checkin_list",$array_ch,"input_by","2");;
				$record_label="";
			//echo "Work";
		}
		else
		{
			//echo "Not Work";
			$sqlticket="";
			$record =0;
			$record_label="";
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlticket = $report->SelectAllDate_Store("checkin_list",$from,$to,"1","input_by",$input_by);
			$record = $report->SelectAllDate_Store("checkin_list",$from,$to,"2","input_by",$input_by);
			$record_label="| Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlticket = $obj->SelectAllByID("checkin_list",array("input_by"=>$input_by));
			$record = $obj->exists_multiple("checkin_list",array("input_by"=>$input_by));
			$record_label=""; 
		}
	}

header('Content-type: application/excel');
$filename ="Checkin_LCD_Report_list_".date('Y_m_d').'.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Checkin LCD Report List : Wireless Geeks Inc.</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
//$data .="<h1>Wireless Geeks Inc.</h1>";
$data .="<h3>".$record_label."</h3>";
$data .="<h5>Checkin LCD Report List Generate Date : ".date('d-m-Y H:i:s')."</h5>";

$data .="<table>
    <thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Checkin ID</th>
			<th>Check IN Detail</th>
			<th>Our Cost</th>
			<th>Retail Cost</th>
			<th>Profit</th>
			<th>Date</th>
		</tr>
</thead>        
<tbody>";

			$i = 1;
			$a=0; $b=0; $c=0; $d=0;
			if (!empty($sqlticket))
				foreach ($sqlticket as $ticket):
				
				$chkcheckin=$obj->exists_multiple("pos_checkin",array("checkin_id"=>$ticket->checkin_id));
						$getsales_id=$obj->SelectAllByVal("pos_checkin","checkin_id",$ticket->checkin_id,"invoice_id");
						$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
						if($curcheck!=0)
						{
							$a+=1;	
							
							
							
					$chkx = $obj->exists_multiple("check_user_price", array("ckeckin_id" => $ticket->checkin_id));
					if ($chkx == 0) {
						$estp = $obj->SelectAllByVal("product", "name", $ticket->device . "-" . $ticket->problem, "price_cost");
						if ($estp == '') {
							$devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
							$modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
							$probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
							$pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid,"name");
						} else {
							$pp = $estp;
						}
					} else {

						$estp = $obj->SelectAllByVal("check_user_price", "ckeckin_id", $ticket->checkin_id, "price");
						if ($estp == '') {
							$devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
							$modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
							$probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
							$pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid, "name");
						} else {
							$pp = $estp;
						}
					}
					$pid = $obj->SelectAllByVal("product", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "id");
					$cid=$obj->SelectAllByVal2("coustomer","firstname",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"first_name"),"phone",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"phone"),"id");
					$ourcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "ourcost");
					$b+=$ourcost;
					
					if($pp=='')
					{
						$retailcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "retailcost");
					}
					else
					{
						$retailcost=$pp;
					}
					$c+=$retailcost;
					$profit=$retailcost-$ourcost;
					$d+=$profit;
							
									
			$data.="<tr>
				<td>".$i."</td>
				<td>".$ticket->checkin_id."</td>
				<td>".$ticket->device." ".$ticket->model." ".$ticket->color." ".$ticket->network." ".$ticket->problem."</td>
				<td>".$ourcost."</td>
				<td>".$retailcost."</td>
				<td>".$profit."</td>
				<td>".$ticket->date."</td>
			</tr>";
			
			$i++; } endforeach;
			
$data .="</tbody><tfoot><tr>
			<th>#</th>
			<th>Checkin ID</th>
			<th>Check IN Detail</th>
			<th>Our Cost</th>
			<th>Retail Cost</th>
			<th>Profit</th>
			<th>Date</th>
		</tr></tfoot></table>";
		
		
		
		
		$data.="<table border='0' width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Check IN Quantity </td>
							<td>".$a."</td>
						</tr>
						<tr>
							<td>2. Our Total Cost  </td>
							<td>$".$b."</td>
						</tr>
						<tr>
							<td>3. Retail Total Cost  </td>
							<td>$".$c."</td>
						</tr>
						<tr>
							<td>4. Profit  </td>
							<td>$".$d."</td>
						</tr>
					</tbody>
				</table>";
		
$data .='</body></html>';

echo $data;
}

if(@$_GET['export']=="pdf") 
{
	if($input_status==1)
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlticket = $report->SelectAllDate("checkin_list",$from,$to,"1");
			$record = $report->SelectAllDate("checkin_list",$from,$to,"2");
			$record_label="| Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlticket = $obj->SelectAll("checkin_list");
			$record = $obj->totalrows("checkin_list");
			$record_label=""; 
		}
	}
	elseif($input_status==5)
	{
		
		$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
		if(!empty($sqlchain_store_ids))
		{
			$array_ch = array();
			foreach($sqlchain_store_ids as $ch):
				array_push($array_ch,$ch->store_id);
			endforeach;
			
			    include('class/report_chain_admin.php');	
				$obj_report_chain = new chain_report();
				$sqlticket=$obj_report_chain->SelectAllByID_Multiple_Or("checkin_list",$array_ch,"input_by","1");
				$record =$obj_report_chain->SelectAllByID_Multiple_Or("checkin_list",$array_ch,"input_by","2");;
				$record_label="";
			//echo "Work";
		}
		else
		{
			//echo "Not Work";
			$sqlticket="";
			$record =0;
			$record_label="";
		}
	}
	else
	{
		if(isset($_GET['from']))
		{
			$from=$_GET['from'];
			$to=$_GET['to'];
			$sqlticket = $report->SelectAllDate_Store("checkin_list",$from,$to,"1","input_by",$input_by);
			$record = $report->SelectAllDate_Store("checkin_list",$from,$to,"2","input_by",$input_by);
			$record_label="| Report Generate Between ".$from." - ".$to;
		}
		else
		{
			$sqlticket = $obj->SelectAllByID("checkin_list",array("input_by"=>$input_by));
			$record = $obj->exists_multiple("checkin_list",array("input_by"=>$input_by));
			$record_label=""; 
		}
	}
	    
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='0'><tbody>";
    $html .="<tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>
						Checkin LCD Report List Report
						</td>
					</tr>
				</table>
			
				
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Checkin LCD Report List Generate Date : ".date('d-m-Y H:i:s')."</td>
					</tr>
				</table>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
				$html.="<thead>
        <tr style='background:#09f; color:#fff;'>
			<th>#</th>
			<th>Checkin ID</th>
			<th>Check IN Detail</th>
			<th>Our Cost</th>
			<th>Retail Cost</th>
			<th>Profit</th>
			<th>Date</th>
		</tr>
</thead>        
<tbody>";
	
	
			$i = 1;
			$a=0; $b=0; $c=0; $d=0;
			if (!empty($sqlticket))
				foreach ($sqlticket as $ticket):
				
				$chkcheckin=$obj->exists_multiple("pos_checkin",array("checkin_id"=>$ticket->checkin_id));
						$getsales_id=$obj->SelectAllByVal("pos_checkin","checkin_id",$ticket->checkin_id,"invoice_id");
						$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
						if($curcheck!=0)
						{
							$a+=1;	
							
							
							
					$chkx = $obj->exists_multiple("check_user_price", array("ckeckin_id" => $ticket->checkin_id));
					if ($chkx == 0) {
						$estp = $obj->SelectAllByVal("product", "name", $ticket->device . "-" . $ticket->problem, "price_cost");
						if ($estp == '') {
							$devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
							$modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
							$probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
							$pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid,"name");
						} else {
							$pp = $estp;
						}
					} else {

						$estp = $obj->SelectAllByVal("check_user_price", "ckeckin_id", $ticket->checkin_id, "price");
						if ($estp == '') {
							$devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
							$modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
							$probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
							$pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid, "name");
						} else {
							$pp = $estp;
						}
					}
					$pid = $obj->SelectAllByVal("product", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "id");
					$cid=$obj->SelectAllByVal2("coustomer","firstname",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"first_name"),"phone",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"phone"),"id");
					$ourcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "ourcost");
					$b+=$ourcost;
					
					if($pp=='')
					{
						$retailcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "retailcost");
					}
					else
					{
						$retailcost=$pp;
					}
					$c+=$retailcost;
					$profit=$retailcost-$ourcost;
					$d+=$profit;
							
									
			$html.="<tr>
				<td>".$i."</td>
				<td>".$ticket->checkin_id."</td>
				<td>".$ticket->device." ".$ticket->model." ".$ticket->color." ".$ticket->network." ".$ticket->problem."</td>
				<td>".$ourcost."</td>
				<td>".$retailcost."</td>
				<td>".$profit."</td>
				<td>".$ticket->date."</td>
			</tr>";
			
			$i++; } endforeach;
			
	$html.="</tbody><tfoot><tr>
		<th>#</th>
		<th>Checkin ID</th>
		<th>Check IN Detail</th>
		<th>Our Cost</th>
		<th>Retail Cost</th>
		<th>Profit</th>
		<th>Date</th>
		</tr></tfoot></table>";
		
		$html.="<table border='0'  width='250' style='width:200px;'>
					<tbody>
						<tr>
							<td>1. Total Check IN Quantity </td>
							<td>".$a."</td>
						</tr>
						<tr>
							<td>2. Our Total Cost  </td>
							<td>$".$b."</td>
						</tr>
						<tr>
							<td>3. Retail Total Cost  </td>
							<td>$".$c."</td>
						</tr>
						<tr>
							<td>4. Profit  </td>
							<td>$".$d."</td>
						</tr>
					</tbody>
				</table>";		
			
    $html.="</td></tr>";
    $html.="</tbody></table>";
    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0; 
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); 
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">

            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->


            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
   										<?php 
										echo $obj->ShowMsg();
										
										if($input_status==1)
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sqlticket = $report->SelectAllDate("checkin_list",$from,$to,"1");
												$record = $report->SelectAllDate("checkin_list",$from,$to,"2");
												$record_label="| Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlticket = $obj->SelectAllByID("checkin_list",array("date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("checkin_list",array("date"=>date('Y-m-d')));
												$record_label=""; 
											}
										}
										elseif($input_status==5)
										{
											
											$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
											if(!empty($sqlchain_store_ids))
											{
												$array_ch = array();
												foreach($sqlchain_store_ids as $ch):
													array_push($array_ch,$ch->store_id);
												endforeach;
												
												if(isset($_GET['from']))
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sqlticket=$obj_report_chain->ReportQuery_Datewise_Or("checkin_list",$array_ch,"input_by",$_GET['from'],$_GET['to'],"1");
													$record =$obj_report_chain->ReportQuery_Datewise_Or("checkin_list",$array_ch,"input_by",$_GET['from'],$_GET['to'],"2");
													$record_label="";
												}
												else
												{
													include('class/report_chain_admin.php');	
													$obj_report_chain = new chain_report();
													$sqlticket=$obj_report_chain->SelectAllByID_Multiple2_Or("checkin_list",array("date"=>date('Y-m-d')),$array_ch,"input_by","1");
													$record =$obj_report_chain->SelectAllByID_Multiple2_Or("checkin_list",array("date"=>date('Y-m-d')),$array_ch,"input_by","2");;
													$record_label="";
												}
												//echo "Work";
											}
											else
											{
												//echo "Not Work";
												$sqlticket="";
												$record =0;
												$record_label="";
											}
										}
										else
										{
											if(isset($_GET['from']))
											{
												$from=$_GET['from'];
												$to=$_GET['to'];
												$sqlticket = $report->SelectAllDate_Store("checkin_list",$from,$to,"1","input_by",$input_by);
												$record = $report->SelectAllDate_Store("checkin_list",$from,$to,"2","input_by",$input_by);
												$record_label="| Report Generate Between ".$from." - ".$to;
											}
											else
											{
												$sqlticket = $obj->SelectAllByID_Multiple("checkin_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
												$record = $obj->exists_multiple("checkin_list",array("input_by"=>$input_by,"date"=>date('Y-m-d')));
												$record_label=""; 
											}
										}
										?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="icon-ok-circle"></i>Check In List Info <?php echo $record_label; ?> | <a  data-toggle="modal" href="#myModal"> Search Datewise </a></h5>
                            <ul class="icons">
                                <li><a href="<?php echo $obj->filename(); ?>" class="hovertip" title="Reload"><i class="font-refresh"></i></a></li>
                            </ul>
                        </div><!-- /page header -->

                        <div class="body">
<!-- Dialog content -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="" method="get">
            <div class="modal-header" style="height:25px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 id="myModalLabel"><i class="icon-calendar"></i> Search Datewise</h5>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label">Date range:</label>
                        <div class="controls">
                            <ul class="dates-range">
                                <li><input type="text" id="fromDate" readonly value="<?php echo date('Y-m-d'); ?>" name="from" placeholder="From" /></li>
                                <li class="sep">-</li>
                                <li><input type="text" id="toDate" readonly value="<?php echo date('Y-m-d'); ?>"  name="to" placeholder="To" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  type="submit" name="search_date"><i class="icon-screenshot"></i> Search</button>
            </div>
        </form>
</div>
<!-- /dialog content -->
										
                            <!-- Middle navigation standard -->

                            <?php //include('include/quicklink.php');  ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">



                                <!-- Content Start from here customized -->


                                <!-- Default datatable -->
                                <div class="table-overflow">
                                    <table class="table table-striped" id="data-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Checkin ID</th>
                                                <th>Check IN Detail</th>
                                                <th>Our Cost</th>
                                                <th>Retail Cost</th>
                                                <th>Profit</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            $i = 1;
											$a=0; $b=0; $c=0; $d=0;
                                            if (!empty($sqlticket))
                                                foreach ($sqlticket as $ticket):
												
												$chkcheckin=$obj->exists_multiple("pos_checkin",array("checkin_id"=>$ticket->checkin_id));
														$getsales_id=$obj->SelectAllByVal("pos_checkin","checkin_id",$ticket->checkin_id,"invoice_id");
														$curcheck=$obj->exists_multiple("sales",array("sales_id"=>$getsales_id));
														if($curcheck!=0)
														{
															$a+=1;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><a href="view_checkin.php?ticket_id=<?php echo $ticket->checkin_id; ?>"><?php echo $ticket->checkin_id; ?></a></td>

                                                        <td><?php echo $ticket->device." ".$ticket->model." ".$ticket->color." ".$ticket->network." ".$ticket->problem; ?></td>
                                                        <td>$<?php
                                                            
                                                            $chkx = $obj->exists_multiple("check_user_price", array("ckeckin_id" => $ticket->checkin_id));
                                                            if ($chkx == 0) {
                                                                $estp = $obj->SelectAllByVal("product", "name", $ticket->device . "-" . $ticket->problem, "price_cost");
                                                                if ($estp == '') {
                                                                    $devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
                                                                    $modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
                                                                    $probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
                                                                    $pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid,"name");
                                                                } else {
                                                                    $pp = $estp;
                                                                }
                                                            } else {

                                                                $estp = $obj->SelectAllByVal("check_user_price", "ckeckin_id", $ticket->checkin_id, "price");
                                                                if ($estp == '') {
                                                                    $devid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "device_id");
                                                                    $modid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "model_id");
                                                                    $probid = $obj->SelectAllByVal("checkin_request", "checkin_id", $ticket->checkin_id, "problem_id");
                                                                    $pp = $obj->SelectAllByVal3("checkin_price", "checkin_id", $devid, "checkin_version_id", $modid, "checkin_problem_id", $probid, "name");
                                                                } else {
                                                                    $pp = $estp;
                                                                }
                                                            }
                                                            $pid = $obj->SelectAllByVal("product", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "id");
                                                            $cid=$obj->SelectAllByVal2("coustomer","firstname",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"first_name"),"phone",$obj->SelectAllByVal("checkin_request","checkin_id",$ticket->checkin_id,"phone"),"id");
															$ourcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "ourcost");
															$b+=$ourcost;
															echo $ourcost;
                                                            ?></td>

                                                        <td>$<?php
                                                            if($pp=='')
															{
																$retailcost=$obj->SelectAllByVal("product_report", "name", $ticket->device . ", " . $ticket->model . " - " . $ticket->problem, "retailcost");
															}
															else
															{
																$retailcost=$pp;
															}
															
															echo $retailcost;
															$c+=$retailcost;
															$profit=$retailcost-$ourcost;
															$d+=$profit;
                                                            ?></a></td>
                                                        <td>$<?php echo $profit; ?></td>
                                                        <td><?php echo $ticket->date; ?></td>
                                                        
                                                        
                                                    </tr>
                                                            <?php
															 
														$i++;
														}
                                                        endforeach;
                                                    ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /default datatable -->
                                <!-- Default datatable -->
                                <div style="margin-top:10px;" class="table-overflow">
                                	
                                    <table class="table table-striped" style="width:250px;">
                                        <tbody>
                                                <tr>
                                                    <td>1. Total Check IN Quantity </td>
                                                    <td><?php echo $a; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>2. Our Total Cost  </td>
                                                    <td>$<?php echo $b; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>3. Retail Total Cost  </td>
                                                    <td>$<?php echo $c; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>4. Profit  </td>
                                                    <td>$<?php echo $d; ?></td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /default datatable -->


                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 

							<?php 
							if(isset($_GET['from'])){
								$from=$_GET['from'];
								$to=$_GET['to'];
							?>
   <a href="<?php echo $obj->filename(); ?>?export=excel&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_excel.png">
   </a>
   <a href="<?php echo $obj->filename(); ?>?export=pdf&amp;from=<?php echo $from; ?>&amp;to=<?php echo $to; ?>">
   		<img src="pos_image/file_pdf.png">
   </a> 
                            <?php
							}
							else
							{
							?>
                                <a href="<?php echo $obj->filename(); ?>?export=excel">
                                	<img src="pos_image/file_excel.png">
                                </a>
                                <a href="<?php echo $obj->filename(); ?>?export=pdf">
                                	<img src="pos_image/file_pdf.png">
                                </a> 
                            <?php 
							}
							?>


                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
<?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php');   ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
