<?php 
include('class/auth.php');
if($input_status==3 || $input_status==4)
{
	$obj->Error("Invalid Page Request.","index.php");
}
include('class/report_customer.php');
$report=new report();  
$table="coustomer";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ajax/customer_ajax.js"></script>
    </head>
	<body>
        <?php include('include/header.php'); ?>
        <!-- Main wrapper -->
        <div class="wrapper three-columns">
            <!-- Left sidebar -->
            <?php include('include/sidebar_left.php'); ?>
            <!-- /left sidebar -->
            <!-- Main content -->
            <div class="content">

                <!-- Info notice -->
                <?php echo $obj->ShowMsg(); ?>
                <!-- /info notice -->

                <div class="outer">
                    <div class="inner">
                        <div class="page-header"><!-- Page header -->
                            <h5><i class="font-home"></i> Inventory Report </h5>
                        </div><!-- /page header -->
						
                        <div class="body">

                            <!-- Middle navigation standard -->
                            <?php //include('include/quicklink.php'); ?>
                            <!-- /middle navigation standard -->

                            <!-- Content container -->
                            <div class="container">

                                <!-- Content Start from here customized -->




                                        <!-- General form elements -->
                                        <div class="row-fluid block">
                                   			  <div class="table-overflow">
                                                <table class="table table-striped" id="data-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>UPC / Barcode</th>
                                                            <th>Name</th>
                                                            <th>Our Cost</th>
                                                            <th>Retail Cost</th>
                                                            <th>Stock</th>
                                                            <th>Sold Quantity</th>
                                                            <th>Re-Order</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
														
														if($input_status==1)
														{
															$sql_product=$obj->Product_report("product","status","1","status","3","1");
														}
														elseif($input_status==5)
														{
															
															$sqlchain_store_ids=$obj->SelectAllByID("store_chain_admin",array("sid"=>$input_by));
															if(!empty($sqlchain_store_ids))
															{
																$array_ch = array();
																foreach($sqlchain_store_ids as $ch):
																	array_push($array_ch,$ch->store_id);
																endforeach;
																include('class/report_chain_admin.php');	
																$obj_report_chain = new chain_report();
																$sql_product=$obj_report_chain->SelectAllByID_Multiple_Or("product_checkin_inventory",$array_ch,"input_by","1");
															
																//echo "Work";
															}
															else
															{
																//echo "Not Work";
																$sql_product="";
															}
														}
														else{
								$sql_product=$obj->Product_report_Store("product","status","1","status","3","1","input_by",$input_by);	
															//$sql_product=$obj->Product_report("product","status","1","status","3","1");
														}
														$i=1;
                                                        if(!empty($sql_product))
                                                        foreach($sql_product as $product):
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><label class="btn"> <?php echo $product->barcode; ?> </label></td>
                                                            <td><label class="label label-success"> <?php echo $product->name; ?> </label></td>
                                                            <td><label class="label"> <?php echo $product->price_cost; ?> </label></td>
                                                            <td><label class="label"> <?php echo $product->price_retail; ?> </label></td>
                                                            <td><label class="btn"> 
															<?php 
															$stock=$obj->escape_empty($obj->SelectAllByVal("product_report","id",$product->id,"quantity"));
															$sold=$obj->escape_empty($obj->SelectAllByVal("product_report","id",$product->id,"soldquantity"));
															$instock=$stock-$sold;
															echo $instock; ?> </label></td>
                                                            <td><label class="btn"> <?php echo $obj->escape_empty($obj->SelectAllByVal("product_report","id",$product->id,"soldquantity")); ?> </label></td>
                                                            <td><label class="label label-primary"> <?php echo $obj->escape_empty($product->reorder); ?> </label></td>
                                                        </tr>
                                                        <?php 
                                                        $i++;
                                                        endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>
                                        <!-- /general form elements -->



                                <!-- Content End from here customized -->




                                <div class="separator-doubled"></div> 



                            </div>
                            <!-- /content container -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /main content -->
            <?php include('include/footer.php'); ?>
            <!-- Right sidebar -->
            <?php //include('include/sidebar_right.php'); ?>
            <!-- /right sidebar -->

        </div>
        <!-- /main wrapper -->

    </body>
</html>
