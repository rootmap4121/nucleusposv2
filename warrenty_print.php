<?php
include('class/auth.php');
$table="warrenty";
if (isset($_GET['print_invoice'])) {

    $cart = $_GET['warrenty_id'];
    $creator = $obj->SelectAllByVal("warrenty", "warrenty_id", $cart, "uid");
    $pt = "Cash";
    $ckid =$cart;
    $tax_statuss =0;
    if($tax_statuss==0){ $taxs=0; }else{ $taxs=0; }
	
    include("pdf/MPDF57/mpdf.php");
	extract($_GET);
    $html.="<table id='sample-table-2' class='table table-hover' border='1'><tbody>";

	$report_cpmpany_name=$obj->SelectAllByVal("setting_report","store_id",$input_by,"name");
	$report_cpmpany_address=$obj->SelectAllByVal("setting_report","store_id",$input_by,"address");
	$report_cpmpany_phone=$obj->SelectAllByVal("setting_report","store_id",$input_by,"phone");
	$report_cpmpany_email=$obj->SelectAllByVal("setting_report","store_id",$input_by,"email");
	$report_cpmpany_fotter=$obj->SelectAllByVal("setting_report","store_id",$input_by,"fotter");
	function limit_words($string, $word_limit){
		$words = explode(" ",$string);
		return implode(" ",array_splice($words,0,$word_limit));
	}
	
	$addressfline=limit_words($report_cpmpany_address,3);
	$lengthaddress=strlen($addressfline);
	$lastaddress=substr($report_cpmpany_address,$lengthaddress,30000);


    $html .="<tr>
			<td style='height:40px; background:rgba(0,51,153,1);'>
				<table style='width:100%; height:40px; border:0px;'>
					<tr>
						<td width='87%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:25px;'>".$report_cpmpany_name." : Warranty Exchange</td><td width='13%' style='background:rgba(0,51,153,1);  color:#FFF; font-size:30px;'><span style='float:left; text-align:left;'>Invoice</span></td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:960px; height:40px; font-size:12px; border:0px;'>
					<tr>
						<td width='69%'>
						".$addressfline."<br>
						".$lastaddress."
						</td>
						<td width='31%'>
						DIRECT ALL INQUIRIES TO:<br />
						".$report_cpmpany_name."<br />
						".$report_cpmpany_phone."<br />
						".$report_cpmpany_email."<br />
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:30px;' valign='top'>
				<table style='width:100%; height:40px; border:0px; font-size:18px;'>
					<tr>
						<td> Warranty From: </td>
					</tr>
				</table>
			</td>
		  </tr>
		  <tr>
			<td style='height:40px;' valign='top'>";
			$type=$obj->SelectAllByVal("warrenty","warrenty_id",$warrenty_id,"type");
			if($type=="ticket")
			{
$cid=$obj->SelectAllByVal("ticket_list","ticket_id",$warrenty_id,"cid");
$html .="<table style='width:960px; height:40px; border:0px;'>
	<tr>
		<td width='69%'>
		Name : " . $obj->SelectAllByVal("coustomer", "id", $cid, "firstname") . "<br />
		Address : " . $obj->SelectAllByVal("coustomer", "id", $cid, "address") . "<br />
		City, State, Zip : " . $obj->SelectAllByVal("coustomer", "id", $cid, "address") . "<br />
		Phone : " . $obj->SelectAllByVal("coustomer", "id", $cid, "phone") . "<br />
		</td>
	</tr>
</table>";
			}
			elseif($type=="checkin")
			{
$html .="<table style='width:960px; height:40px; border:0px;'>
	<tr>
		<td width='69%'>
		Name : " . $obj->SelectAllByVal("checkin_list", "checkin_id", $warrenty_id, "firstname") . "<br />
		Address : " . $obj->SelectAllByVal("checkin_list", "checkin_id", $warrenty_id, "address") . "<br />
		City, State, Zip : " . $obj->SelectAllByVal("checkin_list", "checkin_id", $warrenty_id, "address") . "<br />
		Phone : " . $obj->SelectAllByVal("checkin_list", "checkin_id", $warrenty_id, "phone") . "<br />
		</td>
	</tr>
</table>";
			}
			elseif($type=="unlock")
			{
$cid=$obj->SelectAllByVal("unlock_list","unlock_id",$warrenty_id,"cid");				
$html .="<table style='width:960px; height:40px; border:0px;'>
	<tr>
		<td width='69%'>
		Name : " . $obj->SelectAllByVal("coustomer", "id", $cid, "fullname") . "<br />
		Email : " . $obj->SelectAllByVal("coustomer", "id", $cid, "email") . "<br />
		Phone : " . $obj->SelectAllByVal("coustomer", "id", $cid, "phone") . "<br />
		</td>
	</tr>
</table>";
			}
			$html .="</td>
		  </tr>
		  
		  <tr>
			<td style='height:40px;' valign='top'>
				<table style='width:960px; height:40px; border:0px;'>
					<tr>
						<td width='69%'>
						Phone Repair Center <br />
						We Repair | We Buy | We Sell <br />
						</td>
						<td width='31%'>
						INVOICE DATE  : " . $obj->SelectAllByVal("warrenty","warrenty_id",$cart,"date") . "<br />
						ORDER NO. : " . $cart . "<br />
						SALES REP : " . $obj->SelectAllByVal("store","id",$obj->SelectAllByVal("warrenty","warrenty_id",$cart,"access_id"),"name") . "<br />
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  
		  <tr>
			<td valign='top' style='margin:0; padding:0; width:100%;'>
				<table style='width:960px;border:1px; font-size:12px; background:#ccc;'>";
    $html.="<thead><tr>
						<td>S/L</td>
						<td>Quantity</td>
						<td>Description</td>
						<td>Reason</td>
						<td>Warrenty</td>
					</tr></thead>";
            $html.="<thead><tr>
						<td>1.</td>
						<td>1</td>
						<td>";
						if($type=="ticket")
						{
							$html.=$obj->SelectAllByVal("ticket_list","ticket_id",$cart,"title");
							$html.=", Problem - ".$obj->SelectAllByVal("ticket_list","ticket_id",$cart,"problem");	
						}
						elseif($type=="checkin")
						{
							$html.=$obj->SelectAllByVal("checkin_list","checkin_id",$cart,"device");
							$html.=",".$obj->SelectAllByVal("checkin_list","checkin_id",$cart,"model");
							$html.=",".$obj->SelectAllByVal("checkin_list","checkin_id",$cart,"color");
							$html.=",".$obj->SelectAllByVal("checkin_list","checkin_id",$cart,"network");
							$html.=", Problem - ".$obj->SelectAllByVal("checkin_list","checkin_id",$cart,"problem");	
						}
						 $html.="</td>
						 <td>" . $obj->SelectAllByVal("warrenty","warrenty_id",$cart,"note") . "</td>
						<td>" . $obj->SelectAllByVal("warrenty","warrenty_id",$cart,"warrenty") . " Days</td>
					</tr></thead>";

        $pp ="Cash";


    $due =0;
    $html.="</table></td></tr>";

        $html.="<tr><td><table style='width:960px;'>
					<thead>
						<tr>
							<td width='350' valign='top'>";
		  if($type=="ticket")
			{
		$html.="<table style='width:300px;border:1px; margin-left:-4px; font-size:12px; background:#ccc;'>
					<thead>
						<tr>
							<th>IMEI : </th>
							<th>" . $obj->SelectAllByVal("ticket_list","ticket_id", $warrenty_id, "imei") . "</th>
						</tr>
						<tr>
							<th>Carrier :  </th>
							<th>" . $obj->SelectAllByVal("ticket_list","ticket_id", $warrenty_id, "carrier") . "</th>
						</tr>
						<tr>
							<th>Problem:  </th>
							<th>" . $obj->SelectAllByVal("ticket_list","ticket_id", $warrenty_id, "problem") . "</th>
						</tr>
					</thead>
				</table>";
			}
			elseif($type=="checkin")
			{
		$html.="<table style='width:300px;border:1px; margin-left:-4px; font-size:12px; background:#ccc;'>
					<thead>
						<tr>
							<th>IMEI : </th>
							<th>" . $obj->SelectAllByVal("checkin_list","checkin_id", $warrenty_id, "imei") . "</th>
						</tr>
						<tr>
							<th>Color :  </th>
							<th>" . $obj->SelectAllByVal("checkin_list","checkin_id", $warrenty_id, "color") . "</th>
						</tr>
						<tr>
							<th>Device:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list","checkin_id", $warrenty_id, "device") . "</th>
						</tr>
						<tr>
							<th>Model:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list","checkin_id", $warrenty_id, "model") . "</th>
						</tr>
						<tr>
							<th>Problem:  </th>
							<th>" . $obj->SelectAllByVal("checkin_list","checkin_id", $warrenty_id, "problem") . "</th>
						</tr>
					</thead>
				</table>";
			}					
		  
				$html.="</td>
				<td>
					
				</td>
				</tr>
				</thead>
				</table>
		  </td>
		  </tr>
		  <tr>
		  <td>
				
		  </td>
		  </tr>";
    $html.="<tr>
			<td align='center' style='font-size:8px;'>".$report_cpmpany_fotter."</td>
		  </tr>
		  <tr>
			<td align='center'>Thank You For Your Business</td>
		  </tr>";
    $html.="</tbody></table>";

    $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    // LOAD a stylesheet
    $stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

    $mpdf->WriteHTML($html, 2);

    $mpdf->Output('mpdf.pdf', 'I');
}
?>